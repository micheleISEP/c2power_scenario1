
#ifndef ns_c2power_structures_h
#define ns_c2power_structures_h

#ifndef ns_ip_h
typedef int32_t nsaddr_t;
#endif

#include <map>

class bs_relay_source {
 public:
  ~bs_relay_source() {
    if (name_bs!=NULL) delete[] name_bs;
    if (name_relay!=NULL) delete[] name_relay;
    if (name_source!=NULL) delete[] name_source;
  }

  bs_relay_source(nsaddr_t b, nsaddr_t r, nsaddr_t s, nsaddr_t rL=0, nsaddr_t sL=0, char* nameb=NULL, char* namer=NULL, char* names=NULL) {
    id_bs = b;
    id_relay = r;
    id_source = s;
    id_relayLong = rL;
    id_sourceLong = sL;
    if (nameb!=NULL) {name_bs = new char[strlen(nameb)+1];strcpy(name_bs, nameb);} else {name_bs = NULL;}
    if (namer!=NULL) {name_relay = new char[strlen(namer)+1];strcpy(name_relay, namer);} else {name_relay = NULL;}
    if (names!=NULL) {name_source = new char[strlen(names)+1];strcpy(name_source, names);} else {name_source = NULL;}
  }

  int id_bs;
  int id_relay;
  int id_source;
  int id_relayLong;
  int id_sourceLong;
  char* name_bs;
  char* name_relay;
  char* name_source;
};



class statistics_node_selection {
 public:
  statistics_node_selection(nsaddr_t f, nsaddr_t t, double c, int l) {
    id = f; addr = t; cost = c; longrange = l;
  }
  nsaddr_t id;
  nsaddr_t addr;
  double cost;
  char longrange;
};

/**
class buffer_overheard {
// buffer for overheard frames
// OPEN ISSUE: are we implementing this mechanism?
};
*/

/**
class routing_entry {
// TO BE DEFINED

}
*/


/**
 * Riccardo: Class used in order to containd stack infos inside the c2power layer.
 * Initialized at the beginning via "discovery" tcl command and Discovery() function.
 * NB: for this I added the following functions to sap.cc:
 *			- Module* SAP::getModuleUp()
 * 			- Module* SAP::getModuleDown()
 *     and "ClMsgC2pDisc" cross layer message to the ns-miracle folder
 */
class stack_entry
{
public:

	stack_entry() : IPIFaddr(-1), IPIFid(-1), MACaddr(-1), MACid(-1), PHYid(-1) {};

	void setIPIFid(int i)   { IPIFid   = i; };
	void setIPIFaddr(int i) { IPIFaddr = i; };
	void setMACid(int i)    { MACid    = i; };
	void setMACaddr(int i)  { MACaddr  = i; };
	void setPHYid(int i)    { PHYid    = i; };

	int getIPIFid()   { return IPIFid;   };
	int getIPIFaddr() { return IPIFaddr; };
	int getMACid()    { return MACid;    };
	int getMACaddr()  { return MACaddr;  };
	int getPHYid()    { return PHYid;    };

private:
	int IPIFaddr;
	int IPIFid;
	int MACaddr;
	int MACid;
	int PHYid;
};

class stack_struct
{
public:
	stack_struct() {};
	~stack_struct() { for (int i=0; i<getSize(); i++) {remEntry(i);};};

	void addEntry(int idx, stack_entry e) { 
	  printf("addEntry idx %d \n", idx);
entries[idx] = e; };
	void remEntry(int idx) { entries.erase(idx); };

	stack_entry getEntry(int idx) { return entries[idx]; };
	int getSize() { return entries.size(); };

private:
	std::map<int, stack_entry> entries;
};

#endif // ns_c2power_structures_h
