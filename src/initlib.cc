#include <tclcl.h>
#include <node-core.h>
#include <module.h>
#include "c2power.h"

#include "c2power_clmsg.h"

extern EmbeddedTcl C2PowerInitTclCode;

extern packet_t PT_C2P_ROUTING;

//int hdr_c2p_rout::offset_;

//extern "C" int C2Power_Init() why doesn't work, if I use a "2" into the name???
extern "C" int C_Init()
{
	// Reserve space for C2Power Routing headers
//	C2PRoutingHeaderClass* c2p_r_h = new C2PRoutingHeaderClass;
//	c2p_r_h->bind();

	// CL message for the energy model contained in the plug-in "C2PEnergyModel"
	CLMSG_C2P_GETC2PENERGYMODEL = ClMessage::addClMessage();

	// Register pkts for the routing part (append to the static conts in packet.h:186 )
	PT_C2POWER = p_info::addPacket("PT_C2POWER");
	PT_C2P_ROUTING = p_info::addPacket("PT_C2P_ROUTING");

  C2PowerInitTclCode.load();
  return 0;
}


static class C2PowerHeaderClass : public PacketHeaderClass {
public:
  C2PowerHeaderClass() : PacketHeaderClass("PacketHeader/C2Power", sizeof(hdr_c2power)){
    bind_offset(&hdr_c2power::offset_);
    bind();
  }
} class_c2powerhdr;

static class C2PowerModuleClass : public TclClass {
public:
  C2PowerModuleClass() : TclClass("Module/C2Power") {}
  TclObject* create(int a, const char* const* b) {
    return new C2PowerLayer();
  }
} class_c2powerlayer;


