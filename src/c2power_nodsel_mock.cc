#include "c2power_mock.h"
#include "glpk_stub.h"


MockNodeSelectionMT::MockNodeSelectionMT(C2PowerLayer* ag) {
  ag_ = ag;
  job = NODSEL_JOB_INIT;
}

int MockNodeSelectionMT::SendStatisticsUp(statistics_node_selection* s, statistics_node_selection* l) {
  return SendStatisticsLongRange(s,l);
}

Packet* MockNodeSelectionMT::stats_to_packet(statistics_node_selection* sns, statistics_node_selection* snsL) {
  Packet* pkt = Packet::alloc(2*(sizeof(nsaddr_t)+sizeof(nsaddr_t)+sizeof(double)+sizeof(char))); // ALBANO: +16 not to have a warning with valgrind
  unsigned char* d = pkt->accessdata();
  memcpy(d, &sns->id, sizeof(nsaddr_t));
  memcpy(d+sizeof(nsaddr_t), &sns->addr, sizeof(nsaddr_t));
  memcpy(d+2*sizeof(nsaddr_t), &sns->cost, sizeof(double));
  memcpy(d+2*sizeof(nsaddr_t)+sizeof(double), &sns->longrange, sizeof(char));
  memcpy(d+2*sizeof(nsaddr_t)+sizeof(double)+sizeof(char), &snsL->id, sizeof(nsaddr_t));
  memcpy(d+3*sizeof(nsaddr_t)+sizeof(double)+sizeof(char), &snsL->addr, sizeof(nsaddr_t));
  memcpy(d+4*sizeof(nsaddr_t)+sizeof(double)+sizeof(char), &snsL->cost, sizeof(double));
  memcpy(d+4*sizeof(nsaddr_t)+2*sizeof(double)+sizeof(char), &snsL->longrange, sizeof(char));

  //    for (int i=0;i<2*sizeof(nsaddr_t) + sizeof(double);i++) printf("NODSEL: datadump %d\n", d[i]);
  return pkt;
}

int MockNodeSelectionMT::SendStatisticsLongRange(statistics_node_selection* snsShort, statistics_node_selection* snsLong) {
  Packet* pkt = stats_to_packet(snsShort, snsLong);
  hdr_c2power* hdr = hdr_c2power::access(pkt);
  hdr->tipo = C2POWER_NODSEL_STATS;
  hdr->mitt_id = ag_->addr_;
  hdr->mitt_x = 0;
  hdr->mitt_y = 0;

  hdr_cmn* hdrc = hdr_cmn::access(pkt);
  hdrc->ptype() = PT_C2POWER;
  hdrc->next_hop() = IP_BROADCAST;
  hdrc->addr_type() = NS_AF_INET;
  hdrc->direction() = hdr_cmn::DOWN;
  
  hdr_ip *iph = hdr_ip::access(pkt);
  iph->daddr() = IP_BROADCAST;
  iph->saddr() = ag_->LongAddr;

  //  if (ag_->Debug(C2POWER_DEBUG_NODSEL)>0) printf("NODSEL MT: node %d: %d -> %d q %f: sending statistics long range\n", ag_->addr_, sns->from, sns->to, sns->quality);
  
  
  // quick hack, to ensure i use the long range:
  int using_what = ag_->using_long_range;
  ag_->using_long_range = 1;
  ag_->recv(pkt);
  ag_->using_long_range = using_what;
  return TCL_OK;
}

int MockNodeSelectionMT::SendStatisticsShortRange(statistics_node_selection* sns) {
  printf("NODSEL MT: SendStatisticsShortRange NOT implemented\n");
  return TCL_ERROR;
}

int MockNodeSelectionMT::ClearRoutes() {
  brs_.clear();
  job = NODSEL_JOB_INIT;
  ag_->using_long_range = -1;
  ag_->TurnOnOffInterface(1,0);
  ag_->TurnOnOffInterface(1,1);
}

bs_relay_source* MockNodeSelectionMT::packet_to_route(Packet* pkt)
{
  unsigned char* d = pkt->accessdata();
  bs_relay_source* route =
    new bs_relay_source(
			*(nsaddr_t*)d,
			*(nsaddr_t*)(d+sizeof(nsaddr_t)),
			*(nsaddr_t*)(d+2*sizeof(nsaddr_t)),
			*(nsaddr_t*)(d+3*sizeof(nsaddr_t)),
			*(nsaddr_t*)(d+4*sizeof(nsaddr_t))
			);
  return route;
}



int MockNodeSelectionMT::ReceiveRoute(Packet* pkt) {
  // I should copy it, then delete the original:
  bs_relay_source* b = packet_to_route(pkt);
  if (ag_->Debug(C2POWER_DEBUG_NODSEL)) printf("node %d received route: %d/%d -> %d/%d -> %d\n", ag_->addr_, b->id_source, b->id_sourceLong, b->id_relay, b->id_relayLong, b->id_bs);
  brs_.push_back(b);
}

int MockNodeSelectionMT::SetupRoute(int switchoffunused) {
  assert (NODSEL_JOB_INIT == job);
  job = NODSEL_JOB_NONCOOP;
  list<bs_relay_source*>::iterator current = brs_.begin();
  while (current != brs_.end()) {
    bs_relay_source* item = *current;
    //    printf("node %d considering route: %d -> %d -> %d\n", ag_->addr_, item->id_source, item->id_relay, item->id_bs);
    if (item->id_relay == ag_->ShortAddr) {
    printf("node %d: ", ag_->addr_);
      printf("I am relay\n");
      my_partnerShort = item->id_source;
      my_partnerLong = item->id_sourceLong;
      job = NODSEL_JOB_RELAY;
    }
    if (item->id_source == ag_->ShortAddr) {
      printf("node %d: ", ag_->addr_);
      printf("I am source\n");
      my_partnerShort = item->id_relay;
      my_partnerLong = item->id_relayLong;
      job = NODSEL_JOB_SOURCE;
    }
    if (item->id_bs == ag_->addr_) {
    printf("node %d: ", ag_->addr_);
      printf("I am BS??????????\n");
      job = NODSEL_JOB_BS;
    }
    current++;
  }
  if (NODSEL_JOB_RELAY == job) ag_->using_long_range = -1;
  else if (NODSEL_JOB_SOURCE == job) {
    ag_->using_long_range = 0;
    if (switchoffunused)
      ag_->TurnOnOffInterface(0,1); // OFF, longrange ALBANO
  } else {// NODSEL_JOB_NONCOOP
    ag_->using_long_range = 1; // if you are not cooperating, you use only long-range
    if (switchoffunused)
      ag_->TurnOnOffInterface(0,0); // OFF, shortrange ALBANO
  }

  if (ag_->Debug(C2POWER_DEBUG_NODSEL)>0)
    return Dump();

  return TCL_OK;
}

int MockNodeSelectionMT::Dump()
{
  printf("NODSEL MT: I am:");
  switch (job) {
  case NODSEL_JOB_NONCOOP:
    printf("\ta non-cooperating unit\n");
    break;
  case NODSEL_JOB_INIT:
    printf("\tjust initialized, still not using node selection\n");
    break;
  case NODSEL_JOB_BS:
    printf("\t\ta BASE STATION?????????????????\n\n");
    break;
  case NODSEL_JOB_SOURCE:
    printf("\ta source. my relay is %d/%d\n", my_partnerLong, my_partnerShort);
    break;
  case NODSEL_JOB_RELAY:
    printf("\ta relay. my source is %d/%d\n", my_partnerLong, my_partnerShort);
    break;
  default:
    printf("\ta strange, undefined thing\n\n");
    return TCL_OK;
    //      return TCL_ERROR;
    break;
  }
  return TCL_OK;
}

int MockNodeSelectionMT::RelayPacket(Packet* pkt) {
  hdr_ip* iph = hdr_ip::access(pkt);
  hdr_cmn* hdrc = hdr_cmn::access(pkt);
  if (iph->daddr() == IP_BROADCAST) return 0;
  switch (job) {
  case NODSEL_JOB_INIT:
    return 0;
    break;
  case NODSEL_JOB_NONCOOP:
    if(hdrc->direction() == hdr_cmn::DOWN) {
      iph->saddr() = ag_->LongAddr;
      ag_->sendDown(ag_->LongRangeDeviceID, pkt, 0.0);
      return 1;
    } else { // direction() == UP
      return 0;
    }
    break;
  case NODSEL_JOB_SOURCE:
    if(hdrc->direction() == hdr_cmn::DOWN) {
      hdr_ip *iph = hdr_ip::access(pkt);
      iph->saddr() = ag_->ShortAddr;//set my ip address of short range
      //printf("SOURCE %d laddr %d saddr %d src %d dst %d lrdi %d srdi %d\n", ag_->addr_, ag_->LongAddr, ag_->ShortAddr, iph->saddr(), iph->daddr(), ag_->LongRangeDeviceID, ag_->C2PowerDeviceID);
      hdrc->next_hop() = my_partnerShort;
      ag_->sendDown(ag_->C2PowerDeviceID,pkt,0.0);
      return 1;
    } else { // direction() == UP
      hdr_ip *iph = hdr_ip::access(pkt);
      if (iph->daddr() == ag_->ShortAddr) {
	iph->daddr() == ag_->LongAddr;
	ag_->sendUp(pkt);
	return 1;
      }
    }
    break;
  case NODSEL_JOB_RELAY:
    if(hdrc->direction() == hdr_cmn::DOWN) {
      iph->saddr() = ag_->LongAddr;
      ag_->sendDown(ag_->LongRangeDeviceID, pkt, 0.0);
      return 1;
    } else { // direction() == UP
      // I do masquerading
      if (iph->saddr() == my_partnerShort) {
	iph->saddr() == my_partnerLong;
	//printf("RELAY %d packet from %d modified to %d dst %d\n", ag_->addr_, my_partnerShort, my_partnerLong, iph->daddr());
	hdrc->next_hop() = iph->daddr();
	hdrc->direction() == hdr_cmn::DOWN;
	ag_->sendDown(ag_->LongRangeDeviceID,pkt,0.0);
	return 1;
      }
      if (iph->daddr() == my_partnerLong) {
	hdrc->next_hop() = my_partnerShort;
	iph->daddr() == my_partnerShort;
	hdrc->direction() == hdr_cmn::DOWN;
	ag_->sendDown(ag_->C2PowerDeviceID,pkt,0.0);
	return 1;
      }
      return 0;
    }
    break;
  default:
    printf("error in recv function for ns_mt_!\n\n");
    exit(-1);
  }
  return 1;
}


MockNodeSelectionMT::~MockNodeSelectionMT() {
  // delete all the elements in brs_
  list<bs_relay_source*>::iterator current = brs_.begin();
  bs_relay_source* item = *current++;

  while (current != brs_.end()) {
    delete(item);
    item = NULL;
    current++;
  }
}


MockNodeSelectionBS::MockNodeSelectionBS(C2PowerLayer* ag) {
  ag_ = ag;
  ag_->using_long_range = 1; // ALBANO: gonna be "-1" when the big simulator is ready
}

int MockNodeSelectionBS::ReceiveStatistics(Packet* pkt) {
  sns_.push_back(packet_to_stats(pkt,0));
  sns_.push_back(packet_to_stats(pkt,2*sizeof(nsaddr_t)+sizeof(double)+sizeof(char)));
  return TCL_OK;
}

int MockNodeSelectionBS::find(vector<nsaddr_t> nodeid, nsaddr_t id) {
  int found = -1;
  for (int i=0;i<nodeid.size();i++)
    if (nodeid[i] == id)
      found = i;
  return found;
}

#define MAGNIFY 1000000

static bool compare_items(statistics_node_selection* first, statistics_node_selection* second) {
  if (first->cost < second->cost) return true;
  return false;
}

void MockNodeSelectionBS::Greedy(vector<nsaddr_t> nodeid) {

  list<statistics_node_selection*>::iterator rit = sns_.begin();
  list<statistics_node_selection*>::reverse_iterator sit = sns_.rbegin();

  statistics_node_selection* ritem = *rit;
  statistics_node_selection* sitem = *sit;

  int sources = nodeid.size()/2; // more relays than sources, if odd number of nodes
  int relays = nodeid.size()-sources;



  for (int i=0;i<nodeid.size();i++) {
    printf("nodeid[%d] %d\n", i, nodeid[i]);
  }
  for (int i=0;i<sources;i++) {
    ritem = *rit;
    sitem = *sit;

    int rfound = find(nodeid, ritem->id);
    int sfound = find(nodeid, sitem->id);

    assert(rfound >= 0);
    assert(rfound < relays+sources);
    assert(sfound >= 0);
    assert(sfound < relays+sources);

    printf("NODSEL RELAY\t\tposition %d id %d addr %d longrange %d cost %g\n", rfound, ritem->id, ritem->addr, ritem->longrange, ritem->cost);
    printf("NODSEL SOURCE\t\tposition %d id %d addr %d longrange %d cost %g\n", sfound, sitem->id, sitem->addr, sitem->longrange, sitem->cost);

    /*
    if (1 == item->longrange) {
      if (found < relays)
       	clp->SetLongRelay(found, item->cost*MAGNIFY);
      else
	clp->SetLongSource(found-relays, item->cost*MAGNIFY);
    } else {
      for (int i=0;i<relays;i++)
	clp->SetShortRS(i,found,item->cost*MAGNIFY);
	}*/
    ++rit;
    ++sit;
  }
  //  for (; current != sns_.end() ; current++) {

  //  bs_relay_source**vals = clp->Solve(0);

  /*
  for (int i=0;vals[i]!=NULL;i++) {
    vals[i]->id_relay = nodeid[vals[i]->id_relay];
    vals[i]->id_source = nodeid[vals[i]->id_source + relays];

    printf("bs %d relay %d source %d\n", vals[i]->id_bs, vals[i]->id_relay, vals[i]->id_source);
    brs_.push_back(vals[i]);
  }

  //  for (int i=0;vals[i]!=NULL;i++) delete vals[i];
  delete[] vals;
  printf("saving %g over %g\n", clp->saving, clp->without_cooperation);
  //  delete clp;
  */
}

void MockNodeSelectionBS::NPComplete(vector<nsaddr_t> nodeid) {

  list<statistics_node_selection*>::iterator current = sns_.begin();
  statistics_node_selection* item = *current;

  C2PowerGlpk* clp = new C2PowerGlpk();

  int sources = nodeid.size()/2; // more relays than sources, if odd number of nodes
  int relays = nodeid.size()-sources;

  clp->SetMN(relays,sources);
  current = sns_.begin();
  for (; current != sns_.end() ; current++) {
    item = *current;
    int found = find(nodeid, item->id);
    //    printf("NODSEL BS\t\tposition %d link %d -> %d quality %g longrange %d\n", found, item->from, item->to, item->cost, item->longrange);
    assert(found >= 0);
    assert(found < relays+sources);
    if (1 == item->longrange) {
      if (found < relays)
	clp->SetLongRelay(found, item->cost*MAGNIFY);
      else
	clp->SetLongSource(found-relays, item->cost*MAGNIFY);
    } else {
      //      SetShortRS(i,j,RandCostUwb()); // j -> i
      for (int i=0;i<relays;i++)
	clp->SetShortRS(i,found,item->cost*MAGNIFY);
    }
  }
  if (clp->CheckMatrix()<0) {
    printf("error in the matrix: I lack come measurements!\n");
    exit(-1);
  }
  bs_relay_source**vals = clp->Solve(0);

  printf("\tsolution:\n");
  brs_.clear();
  for (int i=0;vals[i]!=NULL;i++) {
    vals[i]->id_relay = nodeid[vals[i]->id_relay];
    vals[i]->id_source = nodeid[vals[i]->id_source + relays];

    printf("bs %d relay %d source %d\n", vals[i]->id_bs, vals[i]->id_relay, vals[i]->id_source);
    brs_.push_back(vals[i]);
  }

  //  for (int i=0;vals[i]!=NULL;i++) delete vals[i];
  delete[] vals;
  printf("saving %g over %g\n", clp->saving, clp->without_cooperation);
  delete clp;


}

int MockNodeSelectionBS::ComputeRoutes(int green = 0) {
  brs_.clear();

  vector<nsaddr_t> nodeid;
  list<statistics_node_selection*>::iterator current = sns_.begin();
  statistics_node_selection* item = *current;

  for (int i=0;i<nodeid.size();i++) printf("element %d\n", nodeid[i]);
  green = 1;
  if (green) {
    for (current = sns_.begin(); current != sns_.end() ; current++) {
      item = *current;
      if (item->longrange) printf("item %d cost %g\n", item->id, item->cost);
    }
    // sort nodeid using item->cost: nodeid piccolo se item->cost basso
    sns_.sort(compare_items);
    for (current = sns_.begin(); current != sns_.end() ; current++) {
      item = *current;
      if (item->longrange) printf("sorted item %d cost %g\n", item->id, item->cost);
    }
  }

  for (current = sns_.begin(); current != sns_.end() ; current++) {
    item = *current;
    int found = find(nodeid, item->id);
    if (-1==found)
      nodeid.push_back(item->id);
  }
#define GreedyAlgo 0
  if (GreedyAlgo) {
    if (!green) {
      printf("\n\ngreedy must be green!\n\n");
      exit(-1);
    }
    Greedy(nodeid);
  } else {
    NPComplete(nodeid);
  }

  // then I remove the stuff in the list?
  //  sns_.clear();
  return TCL_OK;
}

Packet* MockNodeSelectionBS::route_to_packet(bs_relay_source* brs) {
  Packet* pkt = Packet::alloc(5*sizeof(nsaddr_t) +16);// ALBANO: +16 not to have a warning with valgrind
  unsigned char* d = pkt->accessdata();
  memcpy(d, &brs->id_bs, sizeof(nsaddr_t));
  memcpy(d+sizeof(nsaddr_t), &brs->id_relay, sizeof(nsaddr_t));
  memcpy(d+2*sizeof(nsaddr_t), &brs->id_source, sizeof(nsaddr_t));
  memcpy(d+3*sizeof(nsaddr_t), &brs->id_relayLong, sizeof(nsaddr_t));
  memcpy(d+4*sizeof(nsaddr_t), &brs->id_sourceLong, sizeof(nsaddr_t));
  return pkt;
}


int MockNodeSelectionBS::SendRoutes() {
  list<bs_relay_source*>::iterator current = brs_.begin();
  bs_relay_source* item = *current;
  while (current != brs_.end()) {
    // here i will send to both source and relay
    if (ag_->Debug(C2POWER_DEBUG_NODSEL)>0)
      printf("NODSEL BS: %d -> %d -> %d\n", item->id_source, item->id_relay, item->id_bs);

    // translate id_relay and id_source longIF addr
    list<statistics_node_selection*>::iterator currentstat = sns_.begin();
    for (; currentstat != sns_.end() ; currentstat++) {
      statistics_node_selection* itemstat = *currentstat;
      if (itemstat->id == item->id_relay && 1==itemstat->longrange)
	item->id_relayLong = itemstat->addr;
      if (itemstat->id == item->id_source && 1==itemstat->longrange)
	item->id_sourceLong = itemstat->addr;
    }

    // overwrite id_relay and id_source with shortIF addr
    currentstat = sns_.begin();
    for (; currentstat != sns_.end() ; currentstat++) {
      statistics_node_selection* itemstat = *currentstat;
      if (itemstat->id == item->id_source && 0==itemstat->longrange)
	item->id_source = itemstat->addr;
      if (itemstat->id == item->id_relay && 0==itemstat->longrange)
	item->id_relay = itemstat->addr;
    }

    Packet* pkt = route_to_packet(item);
    hdr_c2power* hdr = hdr_c2power::access(pkt);
    hdr->tipo = C2POWER_NODSEL_ROUTE;
    hdr->mitt_id = ag_->addr_;
    hdr->mitt_x = 0;
    hdr->mitt_y = 0;

    hdr_cmn* hdrc = hdr_cmn::access(pkt);
    hdrc->ptype() = PT_C2POWER;
    hdrc->next_hop() = IP_BROADCAST;
    hdrc->addr_type() = NS_AF_INET;
    hdrc->direction() = hdr_cmn::DOWN;

    hdr_ip *iph = hdr_ip::access(pkt);
    iph->daddr() = IP_BROADCAST;
    iph->saddr() = ag_->addr_;

    // quick hack, to ensure i use the long range:
    // ACTUALLY, BS should ALWAYS use long range
    int using_what = ag_->using_long_range;
    ag_->using_long_range = 1;
    ag_->recv(pkt);
    ag_->using_long_range = using_what;

    current++;
    item = *current;
  }
}

  MockNodeSelectionBS::~MockNodeSelectionBS() {
    brs_.clear();
    sns_.clear();
  }

statistics_node_selection* MockNodeSelectionBS::packet_to_stats(Packet* pkt, int offset) {
    unsigned char* d = pkt->accessdata();
    //    for (int i=0;i<2*sizeof(nsaddr_t) + sizeof(double);i++) printf("NODSEL BS side: datadump %d\n", d[i]);
    statistics_node_selection* sns =
      new statistics_node_selection(
				    *(nsaddr_t*)(d+offset),
				    *(nsaddr_t*)(d+offset+sizeof(nsaddr_t)),
				    *(double*)(d+offset+2*sizeof(nsaddr_t)),
				    *(char*)(d+offset+2*sizeof(nsaddr_t)+sizeof(double))
				    );
    return sns;
  }

int MockNodeSelectionBS::Dump() {
  printf("NODSEL BS: base station %d, data:\n", ag_->addr_);
  {
  list<bs_relay_source*>::iterator current = brs_.begin();
  bs_relay_source* item = *current;
  while (current != brs_.end()) {
    printf("NODSEL BS\t\troute bs %d -> relay %d -> source %d\n",
	   item->id_bs, item->id_relay,item->id_source);
    bs_relay_source* item = *current++;
  }}
  {
  list<statistics_node_selection*>::iterator current = sns_.begin();
  statistics_node_selection* item = *current;
  while (current != sns_.end()) {
    printf("NODSEL BS\t\tlink %d -> %d cost %g longrange %d\n", item->id, item->addr, item->cost, item->longrange);
    current++;
    item = *current;
  }}
  return TCL_OK;
}

int MockNodeSelectionBS::RedirectPacket(Packet* pkt) {
  hdr_ip* iph = hdr_ip::access(pkt);
  hdr_cmn* hdrc = hdr_cmn::access(pkt);

  list<bs_relay_source*>::iterator current = brs_.begin();
  while (current != brs_.end()) {
    bs_relay_source* item = *current;
    if (item->id_sourceLong == iph->daddr()) {
      hdrc->next_hop() = item->id_relayLong;
      hdrc->direction() == hdr_cmn::DOWN;
      ag_->sendDown(ag_->LongRangeDeviceID,pkt,0.0);
      printf("packet for %d rediretto to %d\n", iph->daddr(), hdrc->next_hop());
      return 1;
    }
    current++;
  }
  return 0;
}

