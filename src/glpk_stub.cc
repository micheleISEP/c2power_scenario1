#include <sys/time.h>
#include <stdio.h>
#include <string.h>

#include "glpk_stub.h"

static int get_adesso() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return((t.tv_sec%1000000)*1000+t.tv_usec/1000);
}

void C2PowerGlpk::SetRandomProblem(int m, int n) {
  SetRandomProblem(m,n,0);
}

void C2PowerGlpk::SetRandomProblem(int m, int n, char g) {
  SetRandomProblem(m,n,g,get_adesso());
}

void C2PowerGlpk::Reset() {
  if (cost_Lr) {delete[] cost_Lr; cost_Lr = NULL;}
  if (cost_Ls) {delete[] cost_Ls; cost_Ls = NULL;}
  if (cost_Srs) {delete[] cost_Srs; cost_Srs = NULL;}
  if (NULL!=names_r) {
    for (int i=0;i<relays;i++) if (NULL != names_r[i]) delete[] names_r[i];
    delete[] names_r; names_r = NULL;}
  if (NULL!=names_s) {
    for (int i=0;i<sources;i++) if (NULL != names_s[i]) delete[] names_s[i];
    delete[] names_s; names_s = NULL;}
  relays = sources = 0;
}

void C2PowerGlpk::SetMN(int m, int n) {
  Reset();
  relays = m;
  sources = n;
  cost_Lr = new double[relays]; for (int i=0;i<relays;i++) cost_Lr[i] = -1;
  cost_Ls = new double[sources]; for (int i=0;i<sources;i++) cost_Ls[i] = -1;
  cost_Srs = new double[relays*sources]; for (int i=0;i<relays*sources;i++) cost_Srs[i] = -1;
  names_r = new char* [relays]; for (int i=0;i<relays;i++) names_r[i] = NULL;
  names_s = new char* [sources]; for (int i=0;i<relays;i++) names_r[i] = NULL;
}

void C2PowerGlpk::SetLongRelay(int i, double c) {
  char name[100];
  sprintf(name, "relay%d", i);
  SetLongRelay(i,c,name);
}

void C2PowerGlpk::SetLongRelay(int i, double c, char* name) {
  if (i<0 || i>=relays) return;
  cost_Lr[i] = c;
  names_r[i] = new char[strlen(name+1)]; strcpy(names_r[i],name);
}

void C2PowerGlpk::SetLongSource(int i, double c) {
  char name[100];
  sprintf(name, "relay%d", i);
  SetLongSource(i,c,name);
}

void C2PowerGlpk::SetLongSource(int i, double c, char* name) {
  if (i<0 || i>=sources) return;
  cost_Ls[i] = c;
  names_s[i] = new char[strlen(name+1)]; strcpy(names_s[i],name);
}

void C2PowerGlpk::SetShortRS(int i, int j, double c) {
  if ((i >= 0) && (i < relays) && (j >= 0) && (j < sources)) cost_Srs[i*sources+j] = c;
}

static int cmpdouble(const void *p1, const void *p2)
{
  double v1 = *(double*)p1;
  double v2 = *(double*)p2;
  if (v1 < v2) return -1;
  return v1>v2;
}

static int static_seed=0;
void C2PowerGlpk::SetRandomProblem(int m, int n, char g, int s) {
  green = g;
  seed = s;
  if (static_seed != seed) {
    srand(seed);
    static_seed = seed;
  }

  SetMN(m,n);
  if (g) {
    double vals[m+n];
    for (int i=0;i<m+n;i++) vals[i] = RandCostWifi();
    qsort(vals, m+n, sizeof(double), cmpdouble);

    for (int i=0;i<relays;i++) SetLongRelay(i, vals[i]);
    for (int i=0;i<sources;i++) SetLongSource(i, vals[i+relays]);

    //    for (int i=0;i<relays;i++) printf("relay[%d]: %f\n", i, vals[i]);for (int i=0;i<sources;i++) printf("source[%d]: %f\n", i, vals[i+relays]);
  } else {
    for (int i=0;i<relays;i++) SetLongRelay(i, RandCostWifi());
    for (int i=0;i<sources;i++) SetLongSource(i, RandCostWifi());
  }

  for (int i=0;i<relays;i++)
    for (int j=0;j<sources;j++)
      SetShortRS(i,j,RandCostUwb()); // j -> i

}

#define ENERGY_ALSO_ROUTERS 1
//#define DEBUG_NAMES

bs_relay_source** C2PowerGlpk::Solve(int printdata=0)
{
  if (printdata) {
      SetShortRS(1,1,-1); // j -> i
      SetShortRS(1,2,-1); // j -> i
  }
#ifdef DEBUG_NAMES
  printf("names:\n");
  for (int i=0;i<relays;i++) printf("\trelay %d:\t%s\n", i, names_r[i]);
  for (int i=0;i<sources;i++) printf("\tsource %d:\t%s\n", i, names_s[i]);
#endif

  without_cooperation = 0;
#ifdef ENERGY_ALSO_ROUTERS
  for (int i=0;i<relays;i++)
    without_cooperation += cost_Lr[i];
#endif
  for (int i=0;i<sources;i++)
    without_cooperation += cost_Ls[i];

  glp_prob* lp=NULL;
  lp = glp_create_prob();
  glp_set_prob_name(lp, "c2power_nodsel");
  glp_set_obj_name(lp, "saving");
  glp_set_obj_dir(lp, GLP_MAX);

  int ia[1+2*(sources*relays)];
  int ja[1+2*(sources*relays)];
  double ar[1+2*(sources*relays)];
  for (int i=0;i<=2*(sources*relays);i++) {
    ia[i] = ja[i] = 0; ar[i] = 1.0; // I already know I will have the constant coefficients '1'
  }

  glp_add_rows(lp, sources+relays);

  int constraint_number = 1;
  for (int i=0;i<relays;i++) { // from 0 to relays-1
    char rowname[100];
    sprintf(rowname, "relay%d", i+1);
    glp_set_row_name(lp, i+1, rowname);
    glp_set_row_bnds(lp, i+1, GLP_DB, 0.0, 1.0);

    for (int j=0;j<sources;j++) {
      ia[constraint_number] = i+1;
      ja[constraint_number] = i*sources+j+1;
      constraint_number++;
    }
  }
  for (int i=0;i<sources;i++) {
    char rowname[100];
    sprintf(rowname, "source%d", i+1);
    glp_set_row_name(lp, i+relays+1, rowname);
    glp_set_row_bnds(lp, i+relays+1, GLP_DB, 0.0, 1.0);

    for (int j=0;j<relays;j++) {
      ia[constraint_number] = i+relays+1;
      ja[constraint_number] = j*sources+i+1;
      constraint_number++;
    }
  }
  glp_add_cols(lp, sources*relays);
  glp_load_matrix(lp, constraint_number-1, ia, ja, ar);

  for (int i=0;i<relays;i++) for (int j=0;j<sources;j++) {
      double coeff = 0;
      char columnname[100];
      sprintf(columnname, "x[%d,%d]", i+1, j+1);
      glp_set_col_name(lp, i*sources+j+1, columnname);
      glp_set_col_bnds(lp, i*sources+j+1, GLP_DB, 0.0, 1.0);
      if (cost_Srs[i*sources+j] < 0)
	coeff = - without_cooperation;
      else
	coeff = cost_Ls[j] - cost_Lr[i] - cost_Srs[i*sources+j];
      //      printf("coeff[%d,%d] = %f\n", i, j, coeff);
      glp_set_obj_coef(lp, i*sources+j+1, coeff);
      glp_set_col_kind(lp, i*sources+j+1, GLP_BV);
    }

  //#define BAH
#ifdef BAH
  glp_iocp parm;
  glp_init_iocp(&parm);
  parm.presolve = GLP_ON;
  glp_intopt(lp, &parm);
#else // BAH
  glp_smcp parm1;
  glp_init_smcp(&parm1);
  parm1.msg_lev=GLP_MSG_ERR;
  glp_simplex(lp, &parm1);
  glp_iocp parm2;
  glp_init_iocp(&parm2);
  parm2.msg_lev=GLP_MSG_ERR;
  glp_intopt(lp, &parm2);
#endif // BAH

  saving = glp_get_obj_val(lp);
  if(printdata) PrintData(lp);

  // count the results:
  int count = 0;
  for (int i=0;i<relays;i++)
    for (int j=0;j<sources;j++)
      if (glp_get_col_prim(lp,i*sources+j+1) > 0)
	count++;

  // the result will be a NULL-termined array
  bs_relay_source** ret = new bs_relay_source* [count+1];
  ret[count] = NULL;

  // report the results:
  count = 0;
  for (int i=0;i<relays;i++)
    for (int j=0;j<sources;j++)
      if (glp_get_col_prim(lp,i*sources+j+1) > 0) {
	// setup the single answer
	ret[count] = new bs_relay_source(0,i,j,0,0,NULL,names_r[i], names_s[j]);
	count++;
      }


  glp_delete_prob(lp);
  return ret;
}

int C2PowerGlpk::CheckMatrix() {
  int ret = 1;
  for (int i=0;i<relays;i++) if (cost_Lr[i]<0) ret = -1;
  for (int i=0;i<sources;i++) if (cost_Ls[i]<0) ret = -1;
  for (int i=0;i<relays*sources;i++) if (cost_Srs[i] < 0) ret = -1;
  if (ret<0) {
    for (int i=0;i<relays;i++) printf("Lr[%d] = %g\n", i, cost_Lr[i]);
    for (int i=0;i<sources;i++) printf("Ls[%d] = %g\n", i, cost_Ls[i]);
    for (int i=0;i<relays*sources;i++) printf("Srs[%d,%d] = %g\n", i/sources, i%sources, cost_Srs[i]);
  }
  return ret;
}

void C2PowerGlpk::Solve_c2power()
{
#ifdef DEBUG_NAMES
  printf("names:\n");
  for (int i=0;i<relays;i++) printf("\trelay %d:\t%s\n", i, names_r[i]);
  for (int i=0;i<sources;i++) printf("\tsource %d:\t%s\n", i, names_s[i]);
#endif

  // ALBANO: actually, we should have direct connection for everybody, so all the cost_Lr and cost_Ls > 0. Should I add a sanity check?

  without_cooperation = 0;
#ifdef ENERGY_ALSO_ROUTERS
  for (int i=0;i<relays;i++)
    if (cost_Lr[i] > 0)
      without_cooperation += cost_Lr[i];
#endif
  for (int i=0;i<sources;i++)
    if (cost_Lr[i] > 0)
      without_cooperation += cost_Ls[i];

  glp_prob* lp=NULL;
  lp = glp_create_prob();
  glp_set_prob_name(lp, "c2power_nodsel");
  glp_set_obj_name(lp, "saving");
  glp_set_obj_dir(lp, GLP_MAX);

  // I count the number of variables = number of possible cooperations
  int vars=0;
  for (int i=0;i<relays*sources;i++) if (cost_Srs[i] > 0) vars++;

  // I create the variables, with the relative coefficient
  char varname[vars][100];
  double varcoef[vars];
  int k=0;
  for (int i=0;i<relays;i++) for (int j=0;j<sources;j++) if (cost_Srs[i] > 0) {
	varcoef[k] = cost_Ls[j] - cost_Lr[i] - cost_Srs[i*sources+j];
	sprintf(varname[k], "x[%d,%d]", i+1, j+1);
	k++;
      }

  int ia[1+2*(sources*relays)];
  int ja[1+2*(sources*relays)];
  double ar[1+2*(sources*relays)];
  for (int i=0;i<=2*(sources*relays);i++) {
    ia[i] = ja[i] = 0; ar[i] = 1.0; // I already know I will have the constant coefficients '1'
  }

  glp_add_rows(lp, sources+relays);

  int constraint_number = 1;
  for (int i=0;i<relays;i++) { // from 0 to relays-1
    char rowname[100];
    sprintf(rowname, "relay%d", i+1);
    glp_set_row_name(lp, i+1, rowname);
    glp_set_row_bnds(lp, i+1, GLP_DB, 0.0, 1.0);

    for (int j=0;j<sources;j++) {
      ia[constraint_number] = i+1;
      ja[constraint_number] = i*sources+j+1;
      constraint_number++;
    }
  }
  for (int i=0;i<sources;i++) {
    char rowname[100];
    sprintf(rowname, "source%d", i+1);
    glp_set_row_name(lp, i+relays+1, rowname);
    glp_set_row_bnds(lp, i+relays+1, GLP_DB, 0.0, 1.0);

    for (int j=0;j<relays;j++) {
      ia[constraint_number] = i+relays+1;
      ja[constraint_number] = j*sources+i+1;
      constraint_number++;
    }
  }
  glp_add_cols(lp, sources*relays);
  glp_load_matrix(lp, constraint_number-1, ia, ja, ar);

  for (int i=0;i<relays;i++) for (int j=0;j<sources;j++) {
      double coeff = 0;
      char columnname[100];
      sprintf(columnname, "x[%d,%d]", i+1, j+1);
      glp_set_col_name(lp, i*sources+j+1, columnname);
      glp_set_col_bnds(lp, i*sources+j+1, GLP_DB, 0.0, 1.0);
      coeff = cost_Ls[j] - cost_Lr[i] - cost_Srs[i*sources+j];
      //      printf("coeff[%d,%d] = %f\n", i, j, coeff);
      glp_set_obj_coef(lp, i*sources+j+1, coeff);
      glp_set_col_kind(lp, i*sources+j+1, GLP_BV);
    }

  //#define BAH
#ifdef BAH
  glp_iocp parm;
  glp_init_iocp(&parm);
  parm.presolve = GLP_ON;
  glp_intopt(lp, &parm);
#else // BAH
  glp_smcp parm1;
  glp_init_smcp(&parm1);
  parm1.msg_lev=GLP_MSG_ERR;
  glp_simplex(lp, &parm1);
  glp_iocp parm2;
  glp_init_iocp(&parm2);
  parm2.msg_lev=GLP_MSG_ERR;
  glp_intopt(lp, &parm2);
#endif // BAH

  saving = glp_get_obj_val(lp);
  //  PrintData(lp);
  glp_delete_prob(lp);
}

static void schianta(const char* msg) {
  printf("\n\nERROR:\n\t%s\n\n", msg);
  exit(-1);
}

static void primal_dual_checks(glp_prob* lp) {
  if (GLP_FEAS != glp_get_dual_stat(lp)) {
    switch(glp_get_dual_stat(lp)) {
    case GLP_UNDEF:
      schianta("dual problem NOT defined");
      break;
    case GLP_INFEAS:
      schianta("dual solution is NOT feasible");
      break;
    case GLP_NOFEAS:
      schianta("NO solution to the dual problem");
      break;
    default:
      schianta("something WRONG with the core solver - dual problem");
      break;
    }
  }
  if (GLP_FEAS != glp_get_prim_stat(lp)) {
    switch(glp_get_prim_stat(lp)) {
    case GLP_UNDEF:
      schianta("primal problem NOT defined");
      break;
    case GLP_INFEAS:
      schianta("primal solution is NOT feasible");
      break;
    case GLP_NOFEAS:
      schianta("NO solution to the primal problem");
      break;
    default:
      schianta("something WRONG with the core solver - primal problem");
      break;
    }
  }
}

void C2PowerGlpk::PrintData(glp_prob* lp) {

  for (int i=0;i<relays;i++)
    for (int j=0;j<sources;j++)
      printf("x[%d,%d] = %f\n", i+1, j+1, glp_get_col_prim(lp,i*sources+j+1));

  int cols = glp_get_num_cols(lp);
  printf("columns (variables) = %d\n", cols);
  //  printf("relays = %d\n", relays);
  for (int i=0;i<cols;i++) {
    printf("\t%s\tcoeff\t%f\n", glp_get_col_name(lp, i+1), glp_get_obj_coef(lp, i+1));
    if (GLP_DB != glp_get_col_type(lp, i+1) || 0.0 != glp_get_col_lb(lp, i+1) || 1.0 != glp_get_col_ub(lp, i+1)) schianta("a variable has WRONG bounds");
    if (GLP_BV != glp_get_col_kind(lp, i+1)) schianta("a variable is NOT binary");
  }

  int rows = glp_get_num_rows(lp);
  int ncons = glp_get_num_nz(lp);
  printf("rows (constraints) = %d with %d non-zero coefficients\n", rows, ncons);
  for (int i=0;i<rows;i++) {
    printf("\t%s\n", glp_get_row_name(lp, i+1));
    if (GLP_DB != glp_get_row_type(lp, i+1) || 0.0 != glp_get_row_lb(lp, i+1) || 1.0 != glp_get_row_ub(lp, i+1)) schianta("a constraint is of WRONG type");

    int pos[cols+1];
    double val[cols+1];
    int len = glp_get_mat_row(lp, i+1, pos, val);
    //    printf("%d/%d elements:\n\t", len, cols);
    for (int j=1;j<=len;j++) {
      if (1.0 != val[j]) schianta("WRONG value for a constraint coefficient");
      //      printf("%f %s + ", val[j], glp_get_col_name(lp, pos[j]));
      printf("%s + ", glp_get_col_name(lp, pos[j]));
    }
    printf("\n");
  }

  if (GLP_FEAS != glp_get_dual_stat(lp)) {
    switch(glp_mip_status(lp)) {
    case GLP_UNDEF:
      schianta("MIP problem NOT defined");
      break;
    case GLP_INFEAS:
      schianta("MIP solution is NOT feasible");
      break;
    case GLP_NOFEAS:
      schianta("NO solution to the MIP problem");
      break;
    default:
      schianta("something WRONG with the core solver - MIP problem");
      break;
    }
  }

  //  primal_dual_checks(lp);
  printf("\nRESULT: %f\n", glp_get_obj_val(lp));
}



int main(int argc, char** argv) {
  int m,n;

  if (argc!=3 && argc!=1) {
    printf("\ngive me m(relays) and n(sources)\n\n");
    return -1;
  }
  if (argc == 3) {
    m = atoi(argv[1]);
    n = atoi(argv[2]);
  } else {
    m = -1;
    n = -1;
  }

  int tempo = get_adesso();
  double mean = 0;
#define N_ROUNDS 100
  if (m < 0) {
    for (m=1;m<=50;m++) for (n=1;n<=50;n++) {
	for (int i=0;i<N_ROUNDS;i++) {
	  C2PowerGlpk* clp = new C2PowerGlpk();
	  clp->SetRandomProblem(m,n,1);
	  bs_relay_source** vals = clp->Solve();
	  for (int i=0;vals[i]!=NULL;i++)
	    delete vals[i];
	  delete[] vals;
	  //    printf("%f = %f/%f\n", clp->saving/clp->without_cooperation, clp->saving, clp->without_cooperation);
	  mean+=clp->saving/clp->without_cooperation;
	  delete clp;
	}
	mean /= N_ROUNDS;
	printf("m %d n %d \t a mean of %f on %d rounds, time %d\n", m, n, mean, N_ROUNDS, get_adesso()-tempo);
	tempo = get_adesso();
      }
  } else {
    for (int i=0;i<N_ROUNDS;i++) {
      C2PowerGlpk* clp = new C2PowerGlpk();
      clp->SetRandomProblem(m,n,1);
      bs_relay_source**vals = clp->Solve(1);

      printf("\tsolution:\n");
      for (int i=0;vals[i]!=NULL;i++)
	printf("bs %d relay %d source %d\n", vals[i]->id_bs, vals[i]->id_relay, vals[i]->id_source);


      for (int i=0;vals[i]!=NULL;i++)
	delete vals[i];
      delete[] vals;
      //    printf("%f = %f/%f\n", clp->saving/clp->without_cooperation, clp->saving, clp->without_cooperation);
      mean+=clp->saving/clp->without_cooperation;
      delete clp;
    }
    mean /= N_ROUNDS;
    printf("m %d n %d \t a mean of %f on %d rounds, time %d\n", m, n, mean, N_ROUNDS, get_adesso()-tempo);
  }

  return 0;
}
