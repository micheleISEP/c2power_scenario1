/*****************************************************************
 * Riccardo:
 * 	TODO: delete routing packet and use "accessdata" method
 *        in order to be in line with other blocks.
 * 		  Put used structures in c2power_structures.h
 *****************************************************************/


#ifndef __ns_c2p_routing_pkts_h__
#define __ns_c2p_routing_pkts_h__

#include <packet.h>
#include "c2power_routing_RouteTable.h"
#include "c2power.h"

/*****************************************************************
 * Declare packets formats
 *****************************************************************/
#define C2POWER_ROUT_DATA 1
#define C2POWER_ROUT_RREQ 2
#define C2POWER_ROUT_RREP 3
#define C2POWER_ROUT_RERR 4

/*****************************************************************
 * Header macros for C2Power routing
 *****************************************************************/
#define HDR_C2P_ROUTING(p)	(hdr_c2p_rout::access(p))
#define HDR_C2P_REQUEST(p)	(hdr_c2p_rreq::access(p))
#define HDR_C2P_REPLY(p)	(hdr_c2p_rrep::access(p))
#define HDR_C2P_ERROR(p)	(hdr_c2p_rerr::access(p))
#define HDR_C2P_DATA(p)		(hdr_c2p_data::access(p))

extern packet_t PT_C2P_ROUTING;

// Maximum length of a route
// XXX needed for packet size... can it be dynamic?
#define MAX_RT_LEN 8

/*****************************************************************
 * Declare Structures for packets
 *****************************************************************/

/** Common header for routing part */
class hdr_c2p_rout {
public:
	char h_type;

	// Used as both IP or c2power destination address;
	//  - When we send a RREQ (for a data pkt) we know the IP dest addr but not c2p dest addr.
	//  - When we send a RREP or RERR we know the c2p dest addr.
	int dst;

	int routelen;

	c2p_route_entry addrs[MAX_RT_LEN];

	static int offset_;
	inline static int& offset() {return offset_;}
	inline static hdr_c2p_rout* access(const Packet* p) {
		return (hdr_c2p_rout*) p->access(offset_);
	}
	inline struct c2p_route_entry* getAddrs() { return addrs; }
	inline int getSrc() { return addrs[0].c2p_addr; }
	inline int getDst() { return dst; }
//	inline void fillRoute(Route* r) {
//		for (int i=0; i<r->getLength()-1; i++) {
//			c2p_route_entry e = r->addrs.at(i);
//			addrs[i] = e;
////			addrs[i].c2p_addr     = r->addrs[i].c2p_addr;
////			addrs[i].ipif_addr_in  = r->addrs[i].ipif_addr_in;
////			addrs[i].ipif_addr_out = r->addrs[i].ipif_addr_out;
////			addrs[i].energyCost   = r->addrs[i].energyCost;
////			addrs[i].lifetime     = r->addrs[i].lifetime;
//		}
//		routelen = r->getLength();
//	}
//	inline Route* header2Route() {
//		Route* r = new Route();
//		for ( int i=0; i<routelen-1; i++ ) {
//			r->addEntry(addrs[i]);
//		}
//		return r;
//	}
//	inline int getEgressIPIF(int c2p_addr) {
//		for (int i=0; i<routelen-1; i++) {
//			if ( addrs[i].c2p_addr == c2p_addr ) {
//				return addrs[i].ipif_addr_out;
//				break;
//			}
//		}
//	}
//	inline int getNextNode(int c2p_addr) {
//		for (int i=0; i<routelen-1; i++) {
//			if ( addrs[i].c2p_addr == c2p_addr ) {
//				return addrs[i+1].c2p_addr;
//				break;
//			}
//		}
//	}
	inline void printRoute() {
		printf("\tRoute in header:\n");
		for (int i=0; i<routelen; i++) {
//			if ( addrs[i] == 0 || addrs[i].c2p_addr == -1 )
//				break;
			printf("\t[n%d] %d | ",i,	addrs[i].c2p_addr);

			printf("%d.%d.%d.%d",	(addrs[i].ipif_addr_in & 0xff000000)>>24,
									(addrs[i].ipif_addr_in & 0x00ff0000)>>16,
									(addrs[i].ipif_addr_in & 0x0000ff00)>>8,
									(addrs[i].ipif_addr_in & 0x000000ff) );
			printf(" ");
			printf("%d.%d.%d.%d",	(addrs[i].ipif_addr_out & 0xff000000)>>24,
									(addrs[i].ipif_addr_out & 0x00ff0000)>>16,
									(addrs[i].ipif_addr_out & 0x0000ff00)>>8,
									(addrs[i].ipif_addr_out & 0x000000ff) );
			printf(" | ");
			printf("%g %f\n", addrs[i].energyCost, addrs[i].lifetime);
		}
		printf("\troute length=%d\n",routelen);
	}
	inline std::string strRoute() {
		std::stringstream out;

		out << "{R: ";
		for (int i=0; i<routelen; i++) {
			out << addrs[i].c2p_addr << "|";
		}
		out << "}";
		return out.str();
	}
};

/** header for RREQ packets */
struct hdr_c2p_rreq {
	int rreqId; // the ID of the route request process

//	inline int size() {
//		return (sizeof(int));
//	}
	static int offset_;
	inline static int& offset() {return offset_;}
	inline static hdr_c2p_rreq* access(const Packet* p) {
		return (hdr_c2p_rreq*) p->access(offset_);
	}
};

/** header for RREP packets */
class hdr_c2p_rrep {
public:
	int rreqId; // the ID of the route request process to which we are responding
	int next_hop;
	double cost;
//	inline int size() {
//		return (sizeof(int));
//	}
	static int offset_;
	inline static int& offset() {return offset_;}
	inline static hdr_c2p_rrep* access(const Packet* p) {
		return (hdr_c2p_rrep*) p->access(offset_);
	}
};

/** header for RERR packets */
class hdr_c2p_rerr {
public:
	int next_unreachable_node;
	int next_hop;
//	inline int size() {
//		return (sizeof(int));
//	}
	static int offset_;
	inline static int& offset() {return offset_;}
	inline static hdr_c2p_rerr* access(const Packet* p) {
		return (hdr_c2p_rerr*) p->access(offset_);
	}
};

/** header for data packets routed through c2power*/
class hdr_c2p_data {
public:
	int next_node;

//	inline int size() {
//		return (sizeof(int));
//	}
	static int offset_;
	inline static int& offset() {return offset_;}
	inline static hdr_c2p_data* access(const Packet* p) {
		return (hdr_c2p_data*) p->access(offset_);
	}
};

/*****************************************************************
 * Declare Packet Header classes
 * - Normally it falls into initlib.cc ... put here for keep
 *   implementation of the parts as much independent as possible
 *****************************************************************/

static class C2PRoutingHeaderClass : public PacketHeaderClass {
public:
	C2PRoutingHeaderClass() : PacketHeaderClass("PacketHeader/C2PowerRouting", sizeof(hdr_c2p_rout)){
		bind_offset(&hdr_c2p_rout::offset_);
		bind();
	}
} class_c2power_rout;

static class C2PRoutRREQHeaderClass : public PacketHeaderClass {
public:
	C2PRoutRREQHeaderClass() : PacketHeaderClass("PacketHeader/C2PowerRoutRREQ", sizeof(hdr_c2p_rreq)){
		bind_offset(&hdr_c2p_rreq::offset_);
		bind();
	}
} class_c2power_rreq;

static class C2PRoutRREPHeaderClass : public PacketHeaderClass {
public:
	C2PRoutRREPHeaderClass() : PacketHeaderClass("PacketHeader/C2PowerRoutRREP", sizeof(hdr_c2p_rrep)){
		bind_offset(&hdr_c2p_rrep::offset_);
		bind();
	}
} class_c2power_rrep;

static class C2PRoutRERRHeaderClass : public PacketHeaderClass {
public:
	C2PRoutRERRHeaderClass() : PacketHeaderClass("PacketHeader/C2PowerRoutRERR", sizeof(hdr_c2p_rerr)){
		bind_offset(&hdr_c2p_rerr::offset_);
		bind();
	}
} class_c2power_rerr;

static class C2PRoutDATAHeaderClass : public PacketHeaderClass {
public:
	C2PRoutDATAHeaderClass() : PacketHeaderClass("PacketHeader/C2PowerRoutDATA", sizeof(hdr_c2p_data)){
		bind_offset(&hdr_c2p_data::offset_);
		bind();
	}
} class_c2power_data;

#endif /* __ns_c2p_routing_pkts_h__ */
