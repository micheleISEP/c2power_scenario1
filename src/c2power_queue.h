#ifndef _c2power_queue_h
#define _c2power_queue_h

extern packet_t PT_C2POWER;

class C2PowerQueue;
typedef int (*PacketFilter)(Packet *, void *);

LIST_HEAD(C2PowerQueue_List, C2PowerQueue);

/** C2Power management packets are added ad the beginning of the queue */
class C2PowerQueue : public DropTail {
public:
	C2PowerQueue();

        int     command(int argc, const char*const* argv);
        void    recv(Packet *p, Handler *h);

        void    recvHighPriority(Packet *, Handler *);

        void    recvC2PowerPriority(Packet *, Handler *);


	void	Terminate(void);

	int calcC2PPacketNum(void);

private:


public:
	LIST_ENTRY(C2PowerQueue) link;
	static struct C2PowerQueue_List prhead;
};

#endif /* !_c2p_queue_h */
