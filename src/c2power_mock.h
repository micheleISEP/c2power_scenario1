#ifndef ns_c2power_mock_h
#define ns_c2power_mock_h

//#include <list>
#include "c2power.h"
//#include "c2power_beacon.h"
#include "c2power-timers.h"
#include "c2power_neighbour_list.h"
#include "c2power_routing_RREQTable.h"

#include <iostream>

void parse32Addr(nsaddr_t);

//#include <node-core.h>
//#include <packet.h>
//#include <ip.h>


class MockClusterManager : public ClusterManager {

  C2Power_NeighbourList neigbour_list_;

  //C2PowerBeaconTimer* beaconing;
  C2PowerLayer* ag_;

  /*Timers*/
    double beaconPeriod;
    C2PowerTimer beaconTimer_;

    double neighbourListPeriod; //refresh neighbour list and remove out-of-date entries
    C2PowerTimer neighbourListUpdateTimer_;

    double neighbourListRemoveTreshold;

    bool beaconingON;

 public:
  MockClusterManager(C2PowerLayer* ag);
  int AddNode(C2Power_NeighbourListEntry);
  //int RemoveNode(C2Power_NeighbourListEntry);
  int RecvBeacon(Packet* pkt);
  //int Beaconing(bool start);
  int Beaconing();
  int BeaconingON();
  int BeaconingOFF();

  int RefreshNeighbourList();
  int SendBeaconAndReschedule();

  C2Power_NeighbourList& GetNeighbourList();

  ~MockClusterManager();
};


/** MultiradioRelayingManager */
class MultiradioRelayingManager : public RelayingManager {


public:
 MultiradioRelayingManager(C2PowerLayer*,ClusterManager*);

 //param: shortrangeDev ID, longrangrDevID, latestLongDev DataMode ID,  ShortDataRateVal
 int SelectInterface(int, int,int,PhyMode);

private:
  //C2Power_NeighbourList* neigbour_list_;
  ClusterManager* cm_;

  C2PowerLayer* ag_;


  bool IsCooperationPossible(int LatestDataMode,PhyMode ShortDataRateVal);

};


#define NODSEL_JOB_INIT 0
#define NODSEL_JOB_BS 1 // do I need this one?
#define NODSEL_JOB_RELAY 2
#define NODSEL_JOB_SOURCE 3
#define NODSEL_JOB_NONCOOP 4


class MockNodeSelectionMT : public NodeSelectionMT {
 private:
  C2PowerLayer* ag_;
  list<bs_relay_source*> brs_;
  int job;
  nsaddr_t my_partnerLong;
  nsaddr_t my_partnerShort;

 public:
  int SendStatisticsUp(statistics_node_selection* snsS, statistics_node_selection* snsL);
  MockNodeSelectionMT(C2PowerLayer* ag);

  Packet* stats_to_packet(statistics_node_selection* snsShort, statistics_node_selection* snsLong);

  int SendStatisticsLongRange(statistics_node_selection* snsS, statistics_node_selection* snsL);
  int SendStatisticsShortRange(statistics_node_selection* sns);

  int ClearRoutes();
  int ReceiveRoute(Packet* pkt);
  bs_relay_source* packet_to_route(Packet* pkt);
  int SetupRoute(int switchoffunused);

  int RelayPacket(Packet* pkt);

  int Dump();
  virtual ~MockNodeSelectionMT();

};

class MockNodeSelectionBS : public NodeSelectionBS {
 private:
  C2PowerLayer* ag_;
  list<statistics_node_selection*>sns_;
  list<bs_relay_source*>brs_;
  int find(vector<nsaddr_t> nodeid, nsaddr_t id);
 public:
  MockNodeSelectionBS(C2PowerLayer* ag);
  int RedirectPacket(Packet* pkt);

  int ReceiveStatistics(Packet* pkt);
  statistics_node_selection* packet_to_stats(Packet* pkt, int offset);


  int ComputeRoutes(int green);
  void NPComplete(vector<nsaddr_t> nodeid);
  void Greedy(vector<nsaddr_t> nodeid);
  Packet* route_to_packet(bs_relay_source* brs);
  int SendRoutes();

  int Dump();
  virtual ~MockNodeSelectionBS();
};




class MockRoutingManager : public RoutingManager {

public:
	MockRoutingManager(C2PowerLayer* ag);
	virtual ~MockRoutingManager();

	/* implemented functions */
	int RemoveNode(int ne); // Called from c2p core on node removal
	int RelayDataFrame(Packet* p);

	int Recv(Packet* pkt,int idSrc);

	int selectRoute( int src, int rreqId );

	/* Triggered when a RREQ process fails (RREP not arrived) */
	int failedRREQ( int dstIP, int rreqId );

	void refreshRouteTable();
	void refreshRREQTable();
	void periodicLongScanning(bool firstCall);

public:

	/* Parameters */
	double	alpha_;
	double	rReqTimeOut;
	int		hoplimit;
	int		reBroadcastLimit;
	int		use_broadcast;
	double  routeValidity; // (default=-1) If routeValidity is not set the route refresh is not considered
	int	    turnOnOff;     // define if I can use the PHY turn-on/off capability.
	double  turnONperiod;
	double  turnOFFperiod;

	bool    isLongON;  // value that keeps trace of the status of the long range

private:
	C2PowerLayer* ag_;

	/* Counter for RREQ ID */
	int		rReqIdCount;

	/*Timers*/
	RREQFailed* rreqFail_;
	RREQTimeoutTimer* rreqTimeOut_;
	RouteTableTimer*  routeValidityPeriod_;
	RREQTableTimer*   rreqValidityPeriod_;
	LongONOFFTimer*   timerLongRange_;

	/* Internal structures */
	vector<Packet*> storedPkts;

	RREQTable* rreqTable;
	RouteTable* routeTable;
	RouteTable* handledRoutes;

	/* functions */
	int recvRREQ(Packet* pkt,int idSrc);
	int recvRREP(Packet* pkt,int idSrc);
	int recvRERR(Packet* pkt,int idSrc);

	int sendRREQ(int dst);
	int sendRREP(Route* rt,int rreqid);
	int sendRERR(Route* route , int next_node_unreach );

	int Debug(int what) {return ag_->Debug(what);};

	/* Send a packet using "stack_struct" structure (irrespectively to the value of using_long_range )*/
	void sendPkt( Packet* pkt, int egress_ipif );
	void sendPkt( Packet* pkt);

	void fillRoute(hdr_c2p_rout* hr, Route* r);
	void header2Route(hdr_c2p_rout* hr, Route* r);
	int getEgressIPIF(hdr_c2p_rout* hr, int thisnodeaddr);
	int getNextNode(hdr_c2p_rout* hr, int thisnodeaddr);
	int getNextIPIF(hdr_c2p_rout* hr, int thisnodeaddr);
	int getDestIPIF(hdr_c2p_rout* hr);

	bool checkTurnOffLongRange();

};


#endif // ns_c2power_mock_h

