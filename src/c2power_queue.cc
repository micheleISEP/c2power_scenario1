#include <drop-tail.h>
#include <cmu-trace.h>

#include "c2power_queue.h"

C2PowerQueue_List C2PowerQueue::prhead = { 0 };

static class C2PowerQueueClass : public TclClass {
public:
	C2PowerQueueClass() : TclClass("Queue/DropTail/C2PowerQueue") {}
  TclObject* create(int, const char*const*) {
    return (new C2PowerQueue);
  }
} class_C2PowerQueue;


C2PowerQueue::C2PowerQueue() : DropTail()
{
        //bind("Prefer_Routing_Protocols", &Prefer_Routing_Protocols);
        LIST_INSERT_HEAD(&prhead, this, link);
}

int
C2PowerQueue::command(int argc, const char*const* argv)
{
  if (argc == 2 && strcasecmp(argv[1], "reset") == 0)
   {
      Terminate();
      //FALL-THROUGH to give parents a chance to reset
   }
  return DropTail::command(argc, argv);
}

void
C2PowerQueue::recv(Packet *p, Handler *h)
{
        struct hdr_cmn *ch = HDR_CMN(p);

        if (ch->ptype() == PT_C2POWER){
        	recvC2PowerPriority(p, h);
        }

        //else if(CONDITION)
        //{
        //	recvHighPriority(Packet *p, Handler *)
        //}

        else {
                Queue::recv(p, h);
        }
}


void
C2PowerQueue::recvHighPriority(Packet *p, Handler *)
  // insert packet after C2POWER packets
{
	int C2P_num = C2PowerQueue::calcC2PPacketNum();


	if(C2P_num <= 0) //no C2Power Packets in queue, just enqueHead()
	{
		q_->enqueHead(p);
	}
	else //some C2Power Packets in queue, add after all C2P but before all data packets
	{
		//int k = 0;
		PacketQueue tmp;

		for(int i=0;i<C2P_num;i++)
		{
			tmp.enqueHead(q_->deque()); //rewrite all C2Power packets from the queue to temporary queue
		}

		//HERE
		//temporarily dequeue other priority packets

		q_->enqueHead(p); //add current packet

		//HERE
		//enqueue other priority packets again

		for(int i=0;i<C2P_num;i++)
		{
			q_->enqueHead(tmp.deque());
		}

	}

	if (q_->length() >= qlim_)
    {
      Packet *to_drop = q_->lookup(q_->length()-1);
      q_->remove(to_drop);
      drop(to_drop);
    }

  if (!blocked_) {
    /*
     * We're not blocked.  Get a packet and send it on.
     * We perform an extra check because the queue
     * might drop the packet even if it was
     * previously empty!  (e.g., RED can do this.)
     */
    p = deque();
    if (p != 0) {
      blocked_ = 1;
      target_->recv(p, &qh_);
    }
  }
}

void
C2PowerQueue::recvC2PowerPriority(Packet *p, Handler *)
  // insert packet at front of the queue, but after other PT_C2POWER
{
	//printf("C2PowerQueue::recvC2PowerPriority\n");
	int C2P_num = C2PowerQueue::calcC2PPacketNum();

	if(C2P_num <= 0) //no C2Power Packets in queue, just enqueHead()
	{
		q_->enqueHead(p);
	}
	else //some C2Power Packets in queue, add after C2P but before  data packets
	{
		PacketQueue tmp;

		for(int i=0;i<C2P_num;i++)
		{
			tmp.enqueHead(q_->deque()); //rewrite all C2Power packets from the queue to temporary queue
		}

		q_->enqueHead(p); //add current header

		for(int i=0;i<C2P_num;i++)
		{
			q_->enqueHead(tmp.deque());
		}
	}

	if (q_->length() >= qlim_)
    {
      Packet *to_drop = q_->lookup(q_->length()-1);
      q_->remove(to_drop);
      drop(to_drop);
    }

  if (!blocked_) {
    /*
     * We're not blocked.  Get a packet and send it on.
     * We perform an extra check because the queue
     * might drop the packet even if it was
     * previously empty!  (e.g., RED can do this.)
     */
    p = deque();
    if (p != 0) {
      blocked_ = 1;
      target_->recv(p, &qh_);
    }
  }
}


void
C2PowerQueue::Terminate()
{
	Packet *p;
	while((p = deque())) {
		drop(p, DROP_END_OF_SIMULATION);
	}
}

int C2PowerQueue::calcC2PPacketNum()
{
	int _val = -1; //the queue is empty

	//struct hdr_cmn *ch = HDR_CMN(p);
	if (q_->length() > 0) //if the queue is not empty
	{
		_val = 0; //queue contains some packets

		//int k =0;
		while( q_->length() > _val && (HDR_CMN(q_->lookup(_val)))->ptype() == PT_C2POWER)
		{
			_val++;
		}

		//for(int i=0;i<q_->length();i++)
		//{
		//	Packet *p = q_->lookup(i);
		//	struct hdr_cmn *ch = HDR_CMN(p);
		//	if(ch->ptype() == PT_C2POWER)
		//	{
		//		_val++;
		//	}
		//	else
		//	{
		//		break; //there is no more C2Power packet
		//	}
		//}
	}
	return _val;
};

