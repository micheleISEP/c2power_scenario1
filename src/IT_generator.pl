#!/usr/bin/perl
$ns = "/home/albano/ns-allinone-2.34/ns-2.34-miracle/ns";
$seed = 54321;
for ($traffic=1;$traffic<=2;$traffic++) {
    for ($rate=20;$rate<=100;$rate+=20) {
	for ($num_nodes=5;$num_nodes<=20;$num_nodes+=5) {
	    for ($i=0;$i<10;$i++) {
		$seed += 1234;
		for ($nodsel = 1; $nodsel<=1;$nodsel++) {
		    open(FIN, "<IT_run.launch.tcl");
		    $outname = "run.$traffic.$rate.$num_nodes.$i.$nodsel.tcl";
		    print "$ns $outname > $outname.out\n";
		    open(FOUT, ">$outname");
		    while ($riga = <FIN>) {
			$riga =~ s/<TRAFFIC>/$traffic/;

			$period = 1.0 / $rate;
			$riga =~ s/<PERIOD>/$period/;

			$riga =~ s/<AREA>/20/;

			$riga =~ s/<NODSEL>/$nodsel/;
			$riga =~ s/<NUM_NODES>/$num_nodes/;
			$riga =~ s/<SEED>/$seed/;



			print FOUT "$riga";
		    }
		    close FIN;
		    close FOUT;
		}
	    }
	}
    }
}

#<TRAFFIC> 1 2
#<PERIOD> 0.05 0.025 0.016666667 0.0125 0.01
#<AREA> 20
#<NODSEL> 0 1
#<SEED> boh? 10 times
#<NUM_NODES> 5 10 15 20
