#ifndef _C2POWERPLUGIN_
#define _C2POWERPLUGIN_

#include "plugin.h"
#include <energy-model.h>
#include <c2power_clmsg.h>

extern ClMessage_t CLMSG_C2P_GETC2PENERGYMODEL;

#define C2PPLGIN_CLMSG_VERBOSITY 1

// Riccardo:
// This plug-in is used in order to have a unique EnergyModel for the node that is
// used by different interfaces (through mphy.cc).
// Otherwise, how to have an unique "battery" that drains its energy?
// TODO: Try to distinguish energy spent in different states by each interface within the node?
class C2PEnergyModel : public PlugIn
{
	public:
		C2PEnergyModel();
		virtual ~C2PEnergyModel();

		// TCL command interpreter
		virtual int command(int argc, const char*const* argv);

		// cross-layer command interpreter
		virtual int recvSyncClMsg(ClMessage* m);

		double getResidualEnergy();

	private:
		EnergyModel* em_;
};

/**
 * Implements a cross-layer message in order to communicate to the MPHY
 * the pointer to the energy model contained in the C2power plug in.
 */
class ClMsgC2pGetC2PEnergyModel : public ClMessage
{
	public:
		ClMsgC2pGetC2PEnergyModel();
		ClMsgC2pGetC2PEnergyModel(ClMsgC2pGetC2PEnergyModel *m);
		ClMessage* copy();	// copy the message
		C2PEnergyModel* getPointer() {return emp_;}
		void setPointer(C2PEnergyModel* emp) {emp_ = emp;}
	protected:
		C2PEnergyModel* emp_;
};


// XXX Riccardo:
//   Commented all the old file... it seems old stuff not used by other parts of the code

//#include "plugin.h"
//#include <clmessage.h>
////#include <cltracer.h>
//#include <list>
//
//#define C2PPLGIN_CLMSG_VERBOSITY 1
//
//extern ClMessage_t C2P_CLM_ENTRY_REQ;		// request for an entry
//
//class ClSAP;
//class listentry;
//
//#define DEFAULT_CLMSG_VERBOSITY 1
//
//
//class C2PowerPlugIn : public PlugIn
//{
//
//	public:
//		C2PowerPlugIn();
//		~C2PowerPlugIn();
//
//		// Map for couples [ModuleId - address]
//		list<listentry> map_;
//
//		// TCL command interpreter
//		virtual int command(int argc, const char*const* argv);
//		// cross-layer command interpreter
//		virtual int recvAsyncClMsg(ClMessage* m);
//		virtual int recvSyncClMsg(ClMessage* m);
//
//		// set ctrl message type
//		void setClMsgType(ClMessage_t type);
//		// set ctrl message type
//		ClMessage_t getClMsgType();
//
//	private:
//		ClMessage_t clMsgType_;
//
//		void printMap();
//};
//
//
//// Info for map
//class listentry
//{
//public:
//	listentry(int mid, int address) {
//		 id_=mid;
//		 address_=address;
//	}
//	int getId() {return id_;};
//	int getAddress() {return address_;};
//
//protected:
//	int id_;
//	int address_;
//};
//
//
//
//class C2PClMsgEntryReq : public ClMessage
//{
//	public:
//		C2PClMsgEntryReq();
//		C2PClMsgEntryReq(C2PClMsgEntryReq *m);
//		ClMessage* copy();	// copy the message
//		const int getModId() {return modId_;}
//		const int getAddr() {return addr_;}
//		void setModId(int mid) {modId_ = mid;}
//		void setAddr(int a) {addr_ = a;}
//	protected:
//		int modId_;
//		int addr_;
//};


#endif /* _C2POWERPLUGIN_ */
