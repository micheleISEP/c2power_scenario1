################################################################################
# Command-line parameters
################################################################################


set param(raname)  arf
set param(NoC2PowerNodeNum) 0
set param(apgrid)   20
set param(C2PowerNodeNum)     2 

if {$argc == 0} {

    # default parameters if none passed on the command line
    #set param(dataRate) Mode6Mb

} elseif {$argc == 2} {

#set param(dataRate) [lindex $argv 0]
set param(apgrid)   [lindex $argv 0]
set param(C2PowerNodeNum)     [lindex $argv 1]

} elseif {$argc == 3} {

#set param(dataRate) [lindex $argv 0]
set param(apgrid)   [lindex $argv 0]
set param(C2PowerNodeNum)     [lindex $argv 1]
set param(raname)  [lindex $argv 2]


} elseif {$argc == 4} {

#set param(dataRate) [lindex $argv 0]
set param(apgrid)   [lindex $argv 0]
set param(C2PowerNodeNum)     [lindex $argv 1]
set param(raname)  [lindex $argv 2]
set param(NoC2PowerNodeNum) [lindex $argv 3]


} else {

    puts "USAGE:  ns $argv0 scenarioWidth C2PowerNodesNum RateAdapter NoC2PowerNodesNum"
    puts "  e.g.  ns $argv0 10 2 snr 0"   
    exit 1

}

if { $param(raname) == "snr" } {
    set param(ra) "SNR"
} elseif { $param(raname) == "arf" } {
    set param(ra) "ARF"    
} elseif { $param(raname) == "rraa" } {
    set param(ra) "RRAA"    
} else {
    puts "unknown rate adaptation scheme \"$param(raname)\""
    exit
}

#global $param(ra)

puts "  Parameters set: $param(apgrid) $param(C2PowerNodeNum) $param(ra) $param(NoC2PowerNodeNum)"

################################################################################
# Results
################################################################################

set machine_name [exec uname -n]
set opt(fstr)        ${param(C2PowerNodeNum)}_${param(NoC2PowerNodeNum)}_${param(ra)}.${machine_name}
set opt(resultfname) "stats_${opt(fstr)}.log"
set opt(tracefile)   "${argv0}-[exec whoami].tr"

################################################################################
# Module Libraries
################################################################################
load libMiracle.so
load libmiraclelink.so
load libMiracleBasicMovement.so
load libMiracleWirelessCh.so
load libmiraclecbr.so
load libmphy.so
load libmphytracer.so
load libMiracleIp.so
load libMiracleIpRouting.so
load libmiracleport.so
load libarptracer.so
load libcbrtracer.so
load libalttracermac80211.so
load libdei80211mr.so
load .libs/libC2Power.so



################################################################################
# Simulator instance
################################################################################

set ns [new Simulator]
$ns use-Miracle

################################################################################
# Random Number Generators - not needed ??
################################################################################

set opt(dmax)   [expr $param(apgrid) ]
set opt(dmin)   [expr -$param(apgrid)]

global defaultRNG
set positionrng [new RNG]
set startrng [new RNG]
set stoprng [new RNG]

set rvposition [new RandomVariable/Uniform]
$rvposition set min_ $opt(dmin)
$rvposition set max_ $opt(dmax)
$rvposition use-rng $positionrng

set start [new RandomVariable/Uniform]
$start set min_ 0
$start set max_ 1
$start use-rng $startrng

set stop [new RandomVariable/Uniform]
$stop set min_ 6
$stop set max_ 6.9
$stop use-rng $stoprng

################################################################################
# Finish procedures 
################################################################################

proc finish {} { 
  global ns tf param PhyShort PhyLong sPhy apPhy C2Power
  $ns flush-trace
  close $tf
  
  puts "..::SIMULATION FINISHED::.." 


  if {$param(C2PowerNodeNum) > 0} {
  puts "Results for C2P nodes"
   for {set nodeIdx 1} {$nodeIdx <= $param(C2PowerNodeNum)} {incr nodeIdx} {
   #puts "[$C2Power($nodeIdx) getPacketsNum]"
   puts -nonewline "[$PhyShort($nodeIdx) traceEnergy]"
   #puts  "[$PhyLong($nodeIdx) traceEnergy]"
   }
puts " "

   for {set nodeIdx 1} {$nodeIdx <= $param(C2PowerNodeNum)} {incr nodeIdx} {
   #puts "[$C2Power($nodeIdx) getPacketsNum]"
   #puts -nonewline "[$PhyShort($nodeIdx) traceEnergy]"
   puts   -nonewline "[$PhyLong($nodeIdx) traceEnergy]"
   }
  }

  if {$param(NoC2PowerNodeNum) > 0} {
  puts "Results for NoC2P nodes"
   for {set nodeIdx 1} {$nodeIdx <= $param(NoC2PowerNodeNum)} {incr nodeIdx} {
   puts -nonewline "[$sPhy($nodeIdx) traceEnergy]"
   }
  }

  #puts "C2Power nodes:"  
  #for {set nodeIdx 1} {$nodeIdx <= $param(C2PowerNodeNum)} {incr nodeIdx} {
  #puts "Node $nodeIdx"
  #puts "ShortDev:"
  #puts  -nonewline "  [$PhyShort($nodeIdx) traceEnergy]"
  #puts  "LongDev:  [$PhyLong($nodeIdx) traceEnergy]"
  #}

  #puts "NoC2Power nodes:" 
  #for {set nodeIdx 1} {$nodeIdx <= $param(NoC2PowerNodeNum)} {incr nodeIdx} {
  #puts "LongDev:  [$sPhy($nodeIdx) traceEnergy]"
  #}
}

################################################################################
# Channels and spectral masks
################################################################################

set tf [open $opt(tracefile) w]
$ns trace-all $tf

set channelShort [new Module/DumbWirelessCh]
set propagationShort [new MPropagation/FullPropagation]
$propagationShort set debug_ 0
$propagationShort set xFieldWidth_ 60
$propagationShort set yFieldWidth_ 60


set channelLong [new Module/DumbWirelessCh]
set propagationLong [new MPropagation/FullPropagation]
$propagationLong set debug_ 0
$propagationLong set xFieldWidth_ 60
$propagationLong set yFieldWidth_ 60

create-god [expr (2*($param(C2PowerNodeNum)) + $param(NoC2PowerNodeNum) + 1)]

## Channels 802.11 ##
#            	   1     2     3     4     5     6     7     8     9     10    11
# Spectral masks 2.412 2.417 2.422 2.427 2.432 2.437 2.442 2.447 2.452 2.257 2.462

set maskShort [new MSpectralMask/Rect]
$maskShort setFreq 2.412e9
$maskShort setBandwidth 22e6
$maskShort setPropagationSpeed 3e8

set maskLong [new MSpectralMask/Rect]
$maskLong setFreq 2.457e9
$maskLong setBandwidth 22e6
$maskLong setPropagationSpeed 3e8


################################################################################
# Override Default Module Configuration
################################################################################

## Parameters
Module/MPhy/80211Phy set debug_ 0
Module/MPhy/80211Phy set SIFS_ 0
Module/MPhy/80211Phy set bSyncInterval_ 20e-6
Module/MPhy/80211Phy set gSyncInterval_ 10e-6

Queue/DropTail/PriQueue set Prefer_Routing_Protocols
Queue/DropTail/PriQueue set size_ 1000

#set PHYDataRate $param(dataRate)
set noisePower 7e-12 

set per [new PER]
$per set noise_ $noisePower
$per set debug_ 0
$per loadPERTable80211gTrivellato

Module/MPhy/80211Phy set CSThresh_ [expr $noisePower * 1.1]
Module/MPhy/80211Phy set CCAMode_ 0
Module/MPhy/80211Phy set VerboseCounters_ 1

Mac/802_11/MrclMultirate set VerboseCounters_ 1
set peerstats [new PeerStatsDB/Static]
$peerstats numpeers [expr (2*($param(C2PowerNodeNum)) + $param(NoC2PowerNodeNum) + 1)]

RateAdapter/RRAA set timeout_ 6
RateAdapter/$param(ra) set debug_ 0

Module/C2Power set beaconPeriod_ 1.0
Module/C2Power set neighbourListPeriod_ 5.0
Module/C2Power set neighbourListRemoveTreshold_ 10.0

Module/C2Power set debug_ 5

set cbrPacketSize 1024
set cbrPeriod 0.05

################################################################################
# Create Access Point & Router procedure
################################################################################

# +------------------------------------------------------------------------+
# |                              5. Routing                                |
# +------------------------------------------------------------------------+
# |            3. Wireless           | |           IP  3. Wired IP         |
# +-----------------+----------------+ +----------------+------------------+
# |               2. MAC             |                  |
# +-----------------+----------------+ +----------------+------------------+
# |           1. Module/Phy          | |         To The Internet           |
# +-----------------+----------------+ ------------------------------------+
#                   |           
# +----------------------------------+ 
# |          DumbWirelessChannel     |
# +----------------------------------+ 

#global $peerstats

proc createAP {adapter wifiaddr wifinetmask wireaddr wirenetmask apposx apposy} {
global ns apPhy propagationLong  PHYDataRate maskLong per channelLong 
global ap_macaddr apNode apIpIF_wire apIpr
global peerstats

  set apNode [$ns create-M_Node]
  set apIpr [new Module/IP/Routing]
  set apIpIF_wifi [new Module/IP/Interface]
  set apIpIF_wire [new Module/IP/Interface]

  $apIpIF_wifi addr   $wifiaddr
  $apIpIF_wifi subnet $wifinetmask

  $apIpIF_wire addr  $wireaddr
  $apIpIF_wire subnet $wirenetmask

  set apMAC [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail/PriQueue" [$apIpIF_wifi addr] "" 100 ]
  set apPhy [new Module/MPhy/80211Phy]
  set interfap [new MInterference/MIV]
  set apPosition [new "Position/BM"]
	
  $apPosition setX_ $apposx
  $apPosition setY_ $apposy
  $propagationLong  newNode $apPosition
  $apNode addPosition $apPosition
  $apNode addModule 4 $apIpr 0 "apIpr"
  $apNode addModule 3 $apIpIF_wifi 0 "apIpIFwif"
  $apNode addModule 3 $apIpIF_wire 0 "apIpIFwir"
  $apNode addModule 2 $apMAC 0 "apMAC"
  $apNode addModule 1 $apPhy 0 "apPhy"

  $apMAC dataMode_ Mode6Mb
  $apMAC basicMode_ Mode6Mb
  $apMAC usePhy $apPhy

  set ap_macaddr $apMAC

  set apRA [new RateAdapter/$adapter]
  $apRA attach2mac [$apMAC getMac]
  $apRA use80211g
  $apRA setmodeatindex 0

  $apMAC PeerStatsDB $peerstats

  $apPhy useMAC $apMAC
  $apPhy setPer $per
  $apPhy setSpectralMask $maskLong
  $apPhy setPropagation $propagationLong
  $apPhy setInterference $interfap

  $apNode setConnection $apIpr $apIpIF_wifi 0
  $apNode setConnection $apIpr $apIpIF_wire 0
  $apNode setConnection $apIpIF_wifi $apMAC 0
  $apNode setConnection $apMAC $apPhy 0
  $apNode addToChannel $channelLong  $apPhy 0

  $apMAC bss_id "$ap_macaddr"
	
	


puts "AP created at ([$apPosition getX_],[$apPosition getY_])"
}

################################################################################
# Create Sink Node in the Internet
################################################################################
proc createSinkNode {id ipaddr netmask destnet destnetmask gtw} {
global ns sink_node sink_ipif sink_port sink_cbr

set sink_node($id)   [$ns create-M_Node] 
set sink_cbr($id)    [new Module/CBR]
set sink_port($id)   [new Module/Port]
set sink_ipr($id)    [new Module/IP/Routing]
set sink_ipif($id)   [new Module/IP/Interface]

$sink_node($id) addModule 7  $sink_cbr($id) 0 "sink_CBR"
$sink_node($id) addModule 6  $sink_port($id) 0 "sink_PORT"
$sink_node($id) addModule 5  $sink_ipr($id)  0 "sink_IPR"
$sink_node($id) addModule 4  $sink_ipif($id) 0 "sink_IPF"

$sink_node($id) setConnection $sink_cbr($id) $sink_port($id)   1
$sink_node($id) setConnection $sink_port($id) $sink_ipr($id)   0
$sink_node($id) setConnection $sink_ipr($id)  $sink_ipif($id)  0

$sink_ipif($id)  addr   $ipaddr
$sink_ipif($id)  subnet $netmask
$sink_ipr($id)  addRoute $destnet $destnetmask $gtw

}

################################################################################
# Direct Link to the Internet :)
################################################################################

set dlink [new Module/DuplexLink]
$dlink delay      0.1
$dlink bandwidth  100000000
$dlink qsize      10
$dlink settags "dlink"


################################################################################
# Create a simple NoC2Power wireless node
################################################################################

proc createNoC2PowerNode {adapter id ipad netmask defaultGw posX posY} {

  global ns propagationLong maskLong PHYDataRate per channelLong
  global ap_macaddr sCbr sPhy peerstats
  global cbrPacketSize cbrPeriod


  set sNode($id)  [$ns create-M_Node]
  set sCbr($id) [new Module/CBR]
  set sPort($id) [new Module/Port]
  set sIpr($id) [new Module/IP/Routing]
  set sIpIF($id) [new Module/IP/Interface]
  #set C2Power($id) [new Module/C2Power]

  $sIpIF($id) addr $ipad
  $sIpIF($id) subnet $netmask
  $sIpr($id) defaultGateway $defaultGw

  set sMAC($id) [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail" [$sIpIF($id) addr] "" 20 ]
  set sPhy($id) [new Module/MPhy/80211Phy]
  set sinterf [new MInterference/MIV]  

  $sCbr($id) set packetSize_ $cbrPacketSize
  $sCbr($id) set period_ $cbrPeriod
    
  set sPosition($id) [new "Position/BM"]

  $sNode($id)  addPosition $sPosition($id)

  $sPosition($id) setX_ $posX
  $sPosition($id) setY_ $posY

  $propagationLong newNode $sPosition($id)
  $sNode($id)  addModule 6 $sCbr($id) 0 "sCBR_$id"
  $sNode($id)  addModule 5 $sPort($id) 0 "sPort_$id"
  $sNode($id)  addModule 4 $sIpr($id) 0 "sIpr_$id"
  $sNode($id)  addModule 3 $sIpIF($id) 0 "sIpIF_$id"
  #$sNode($id)  addModule 2 $sMAC($id) 0 "sMAC_$id"

  set sMACnc_id [$sNode($id)  addModule 2 $sMAC($id) 0 "sMAC_$id"]

  $sNode($id)  addModule 1 $sPhy($id) 0 "sPhy_$id"

  $sMAC($id) dataMode_ Mode6Mb
  $sMAC($id) basicMode_ Mode6Mb
  $sMAC($id) usePhy $sPhy($id)

  set sRA($id) [new RateAdapter/$adapter]

  $sRA($id) attach2mac [$sMAC($id) getMac]
  $sRA($id) use80211g
  $sRA($id) setmodeatindex 0

  $sMAC($id) PeerStatsDB $peerstats


  $sPhy($id) useMAC $sMAC($id)
  $sPhy($id) setPer $per
  $sPhy($id) setSpectralMask $maskLong
  $sPhy($id) setPropagation $propagationLong
  $sPhy($id) setInterference $sinterf

#  $sPhy($id) setEnergyModel
  $sPhy($id) setEnergy 1000
  #$Phy($id) setConsumption 1.0 2.0 3.0 0.01
  #microsecond counter
  $sPhy($id) setConsumption 1.0 1.0 1.0 1.0  

  $sNode($id)  setConnection $sCbr($id) $sPort($id) 0
  $sNode($id)  setConnection $sPort($id) $sIpr($id) 0
  $sNode($id)  setConnection $sIpr($id) $sIpIF($id) 0
  #$sNode($id)  setConnection $sPort($id) $sIpIF($id) 1

  #$sNode($id)  setConnection $sIpIF($id) $C2Power($id) 1
  #$sNode($id)  setConnection $C2Power($id) $MAC($id) 1
  $sNode($id)  setConnection $sIpIF($id) $sMAC($id) 0
  $sNode($id)  setConnection $sMAC($id) $sPhy($id) 0
  $sNode($id)  addToChannel $channelLong $sPhy($id) 0

  $sPort($id) assignPort $sCbr($id)

  $sMAC($id) bss_id "$ap_macaddr"

  puts "NoC2PowerNode created at ([$sPosition($id) getX_],[$sPosition($id) getY_])"

 return $sNode($id) 
}
################################################################################
# Create a C2POWER Node
################################################################################
# +------------------------------------------------------------------------+
# |                              4. PORT & APP                             |
# +------------------------------------------------------------------------+
# |                              4. C2POWER                                |
# +------------------------------------------------------------------------+
# |                3. IP             | |              3. IP                |
# +-----------------+----------------+ +----------------+------------------+
# |               2. MAC             | |              2. MAC               | 
# +-----------------+----------------+ +----------------+------------------+
# |           1. Module/Phy          | |          1. Module/Phy            |
# +-----------------+----------------+ ------------------------------------+
#                   |           	                 |           
# +----------------------------------+ +-----------------------------------+ 
# |          DumbWirelessChannel     | |          DumbWirelessChannel      |
# +----------------------------------+ +-----------------------------------+ 


proc createC2PowerNode {adapter id ipadSh netmaskSh ipadLong netmaskLong defaultGw  posX posY} {

  global ns  PHYDataRate per maskShort channelShort channelLong propagationShort propagationLong Position Cbr
  global C2Power PhyShort maskLong PhyLong ap_macaddr  MACLong 
  global peerstats
  global cbrPacketSize cbrPeriod

  set Node($id)  [$ns create-M_Node]
  set Cbr($id) [new Module/CBR]

  set Port($id) [new Module/Port]
  set Ipr($id) [new Module/IP/Routing]
  set C2Power($id) [new Module/C2Power]
  
  set IpIFShort($id) [new Module/IP/Interface]
  set IpIFLong($id) [new Module/IP/Interface]
 

  $IpIFShort($id) addr $ipadSh
  $IpIFShort($id) subnet $netmaskSh

  $IpIFLong($id) addr $ipadLong
  $IpIFLong($id) subnet $netmaskLong

  $Ipr($id) defaultGateway $defaultGw

  ############ C2POWER DEVICE ###########################################################
  set MACShort($id) [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail/C2PowerQueue" [$IpIFShort($id) addr] "" 25 ]
  set PhyShort($id) [new Module/MPhy/80211Phy]
  set interf [new MInterference/MIV]
  

  $Cbr($id) set packetSize_ $cbrPacketSize
  $Cbr($id) set period_ $cbrPeriod
    
  set Position($id) [new "Position/BM"]

  $Node($id)  addPosition $Position($id)
  $Position($id) setX_ $posX
  $Position($id) setY_ $posY

  $propagationShort newNode $Position($id)
  $Node($id)  addModule 7 $Cbr($id) 0 "CBR_$id"
  $Node($id)  addModule 6 $Port($id) 0 "Port_$id"
  $Node($id)  addModule 5 $Ipr($id) 0 "Ipr_$id"
  $Node($id)  addModule 3 $IpIFShort($id) 0 "IpIFSh_$id"
  $Node($id)  addModule 4 $C2Power($id) 0 "C2POWER_$id"  
  $Node($id)  addModule 2 $MACShort($id) 0 "MACSh_$id"
  $Node($id)  addModule 1 $PhyShort($id) 0 "PhySh_$id"

  $MACShort($id) dataMode_ Mode54Mb
  $MACShort($id) basicMode_ Mode6Mb
  $MACShort($id) usePhy $PhyShort($id)

  $PhyShort($id) useMAC $MACShort($id)
  $PhyShort($id) setPer $per
  $PhyShort($id) setSpectralMask $maskShort
  $PhyShort($id) setPropagation $propagationShort
  $PhyShort($id) setInterference $interf

#  $PhyShort($id) setEnergyModel
  $PhyShort($id) setEnergy 1000
  #$Phy($id) setConsumption 1.0 2.0 3.0 0.01
  #microsecond counter
  $PhyShort($id) setConsumption 1.0 1.0 1.0 1.0  



  $Node($id)  setConnection $C2Power($id) $IpIFShort($id) 0
  $Node($id)  setConnection $IpIFShort($id) $MACShort($id) 0
  $Node($id)  setConnection $MACShort($id) $PhyShort($id) 0
  $Node($id)  addToChannel  $channelShort $PhyShort($id) 0






  # LONG RANGE DEVICE #################################################################

  set MACLong($id) [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail" [$IpIFLong($id) addr] "" 100 ]
  set PhyLong($id) [new Module/MPhy/80211Phy]
  set interfLong [new MInterference/MIV]

  $propagationLong newNode $Position($id)
  
  $MACLong($id) dataMode_ Mode6Mb
  $MACLong($id) basicMode_ Mode6Mb
  $MACLong($id) usePhy $PhyLong($id)

  $MACLong($id) bss_id "$ap_macaddr"

  set C2PRateAdapter($id) [new RateAdapter/$adapter]
  $C2PRateAdapter($id) attach2mac [$MACLong($id) getMac]
  $C2PRateAdapter($id) use80211g
  $C2PRateAdapter($id) setmodeatindex 0
  $MACLong($id) PeerStatsDB $peerstats

  $PhyLong($id) useMAC $MACLong($id)
  $PhyLong($id) setPer $per
  $PhyLong($id) setSpectralMask $maskLong
  $PhyLong($id) setPropagation $propagationLong
  $PhyLong($id) setInterference $interfLong


  $Node($id)  addModule 3 $IpIFLong($id) 0 "IpIFL_$id"  
  $Node($id)  addModule 2 $MACLong($id) 0 "MACLong_$id"
  $Node($id)  addModule 1 $PhyLong($id) 0 "PhyLong_$id"

#  $PhyLong($id) setEnergyModel
  $PhyLong($id) setEnergy 1000
  #$Phy($id) setConsumption 1.0 2.0 3.0 0.01
  #microsecond counter
  $PhyLong($id) setConsumption 1.0 1.0 1.0 1.0  


  $Node($id)  setConnection $Cbr($id) $Port($id) 0
  $Node($id)  setConnection $Port($id) $Ipr($id) 0
  $Node($id)  setConnection $Ipr($id) $C2Power($id) 0
  #$Node($id)  setConnection $Port($id) $IpIFLong($id) 1
  $Port($id) assignPort $Cbr($id)

  $Node($id)  setConnection $C2Power($id) $IpIFLong($id) 0
  $Node($id)  setConnection $IpIFLong($id) $MACLong($id) 0
  $Node($id)  setConnection $MACLong($id) $PhyLong($id) 0
  $Node($id)  addToChannel $channelLong $PhyLong($id) 0



  #$C2Power($id) setC2PowerDevice [$MACShort($id) Id_]
  #$C2Power($id) setLongRangeDevice [$MACLong($id) Id_]

  $C2Power($id) setC2PowerDevice [$IpIFShort($id) Id_]
  $C2Power($id) setLongRangeDevice [$IpIFLong($id) Id_]

  $C2Power($id) setShortRangeDeviceAddr [$IpIFShort($id) addr]  
  $C2Power($id) setShortRangeDeviceSubnet [$IpIFShort($id) subnet]

  $C2Power($id) setLongRangeDeviceAddr [$IpIFLong($id) addr]
  $C2Power($id) setLongRangeDeviceSubnet [$IpIFLong($id) subnet]

  $C2Power($id) setC2PowerDatarate Mode54Mb



  puts "C2PowerNode created at ([$Position($id) getX_],[$Position($id) getY_])"
  


  return $Node($id) 
}


################################################################################
# Scenario settings
################################################################################

createAP "$param(ra)" "2.0.0.254" "255.255.255.0" "1.0.0.1" "255.255.255.0" "$param(apgrid)" "$param(apgrid)"
	$apIpr addRoute "2.0.0.0" "255.255.255.0" "2.0.0.254"
	$apIpr addRoute "1.0.0.0" "255.255.255.0" "1.0.0.1"

createSinkNode "1" "1.0.0.254" "255.255.255.0" "2.0.0.0" "255.255.255.0" "1.0.0.1"
$dlink connect $sink_node(1) $sink_ipif(1) 1 $apNode $apIpIF_wire 1


if {$param(C2PowerNodeNum) > 0} {
puts "Creating C2P nodes"

for {set id 1} {$id <= $param(C2PowerNodeNum)} {incr id} {
createC2PowerNode "$param(ra)" "$id" "3.0.0.$id" "255.255.255.0" "2.0.0.$id" "255.255.255.0" "2.0.0.254" [$rvposition value] [$rvposition value]


$Cbr($id) set destAddr_ [$sink_ipif(1) addr]
$Cbr($id) set destPort_ [$sink_port(1) assignPort $sink_cbr(1)]

}

}


if {$param(NoC2PowerNodeNum) > 0} {
puts "Creating NoC2P nodes"



for {set id 1} {$id <= $param(NoC2PowerNodeNum)} {incr id} {

createNoC2PowerNode "$param(ra)" "$id" "2.0.0.10$id" "255.255.255.0" "2.0.0.254" [$rvposition value] [$rvposition value]

$sCbr($id) set destAddr_ [$sink_ipif(1) addr]
$sCbr($id) set destPort_ [$sink_port(1) assignPort $sink_cbr(1)]
}

}

################################################################################
# SIMULATION
################################################################################

puts "---> BEGIN SIMULATION"

if {$param(C2PowerNodeNum) > 0} {


for {set id 1} {$id <= $param(C2PowerNodeNum)} {incr id} {

$ns at "0.$id" "$C2Power($id) start_beaconing"
#$ns at "1.$id" "$C2Power($id) discovery"
#$ns at "1.$id" "$C2Power($id) routing"
$ns at "5.$id" "$C2Power($id) nodsel_dumpdata"

$ns at "1.$id" "$Cbr($id) start"

}

}

if {$param(NoC2PowerNodeNum) > 0} {


for {set id 1} {$id <= $param(NoC2PowerNodeNum)} {incr id} {

$ns at "1.$id" "$sCbr($id) start"

}

}



#$ns at 0.11 "$C2Power(2) start_beaconing"

#$ns at 0.9 "$Cbr(1) start"
#$ns at 1 "$Cbr(2) start"






#$ns at 120.0 "finish; $ns halt"
$ns at 6.0 "finish; $ns halt"

$ns run

