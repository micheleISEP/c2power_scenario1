
#ifndef _c2power_routing_RREQTABLE_H_
#define _c2power_routing_RREQTABLE_H_

#include <vector>
#include <string>
#include <sstream>

#include <stdio.h>
#include <c2power_routing_RouteTable.h>

//using namespace std;

/*********************************************************************************/
/*********************************************************************************/
class Record {

public:
	Record();
//	Record(int,int,int);
	Record(int,int,int,int);
	~Record();

	int srcId;
	int destIP;
	int rreqId;
	int reBroadcast;
	int decisionTaken;

	double time;

	std::vector<Route*> routes;
};

/*********************************************************************************/
/*********************************************************************************/
class RREQTable {

public:
	RREQTable();
	~RREQTable();

public:

	/** Add a RREQ
	 * 	- if couple <srcNode-rreq> already exist increment reBroadcast number;
	 *  - if couple <srcNode-rreq> doesn't exist it adds a new record.
	 *  */
//	void addRReq(int srcNode, int rreq);
	void addRReq(int srcNode, int dstIP, int rreq, double time);

	/** increment rebroadcast number to a record */
//	void incrReBroadcst(int srcNode, int rreq);
	void incrReBroadcst(int srcNode, int dstIP, int rreq, double time);

	/** set this record as decided */
	void setDecided(int srcNode, int rreq);

	/** check if for this record a decision is already taken*/
	bool checkDecided(int srcNode, int rreq);

	/** Check if couple <srcNode-rreq> is already contained in Cache */
	int findRReq(int srcNode, int rreq);
	int chkAtSRC(int srcNode, int dstIP);

	Record& getEntry(int idx);
	Record& operator[](int idx);

	/** Remove couple <srcNode-rreq> if any. */
	bool remRReq(int srcNode, int rreq);

	/** Remove record <srcNode-dstIP-rreq> if any. */
	bool remRReq(int srcNode, int dstIP, int rreq);

	/** Get list size. */
	int getSize();

	/** Get num routes for an entry <srcNode-rreq> */
	int getNumRoutes(int srcNode, int rreq);

	/** Get the routes routes for an entry <srcNode-rreq> */
	std::vector<Route*>* getRoutes(int srcNode, int rreq);

	/** Add a route to an entry <srcNode-rreq> */
	bool addRoute(int srcNode, int rreq, Route* r);

	/** Removes old entryes */
	bool Refresh(double validity, double now);

	std::string print();

	bool clearTable();

private:
	std::vector<Record> rreqCache;

};

#endif /* _c2power_routing_RREQTABLE_H_ */
