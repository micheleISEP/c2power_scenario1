#include <ip.h>
#include <mac.h>
#include <config.h>
#include <limits>

#include "c2power.h"
#include "c2power_mock.h"
#include "c2power_routing_pkts.h"

#include <stdlib.h>
#include <iostream>

#define NOT_FOR_ME "NFM"
#define REBROADCAST_LIMIT_EXCEEDED "RLX"
#define HOP_LIMIT_EXCEEDED "HLX"
#define ROUTE_WITH_LOOPS "RWL"
#define LONG_RANGE_PROBLEM "LRP"
#define ROUTE_ALREADY_DECIDED "RAD"
#define UNKNOWN_REASON "UNR"
#define NO_HANDLED_ROUTES "NHR"

MockRoutingManager::MockRoutingManager(C2PowerLayer* ag) {
	ag_ = ag;

	rReqIdCount = 0;

	// Those parameters are expected to be initialized via TCL
	hoplimit = -1;
	reBroadcastLimit = -1;
	alpha_ = -1;
	rReqTimeOut = -1;
	use_broadcast = -1;
	routeValidity = -1;

	turnOnOff = -1;
	turnONperiod = -1;
	turnOFFperiod = -1;

	isLongON = true;	// Value that keeps trace of the status of the long range true ny default.

	// Tables istanciation
	rreqTable = new RREQTable();
	routeTable = new RouteTable();
	handledRoutes = new RouteTable();

	// Timersistanciation
	routeValidityPeriod_ = new RouteTableTimer(this);
	rreqValidityPeriod_  = new RREQTableTimer(this);
	timerLongRange_      = new LongONOFFTimer(this);

}

MockRoutingManager::~MockRoutingManager() {
	delete rreqTable;
	delete routeTable;
}

int MockRoutingManager::RemoveNode(int c2paddr ){
	// For every rotue having next node "ni" send a RERR to the SRC
	// calculating the reverse route.
	// Then remove the handled routes.

	vector<Route*> v = handledRoutes->getlist( ag_->addr_, c2paddr );
	for (int i=0; i<v.size(); i++) {
		sendRERR(v[i], c2paddr);
	}

	bool rem = handledRoutes->remRoutes1(ag_->addr_, c2paddr);

	// I need to remove from routeTable routes containing c2paddr as first node.
	routeTable->remRoutes2(c2paddr);

	if (rem) {
		if (turnOnOff) checkTurnOffLongRange();
	}
	return TCL_OK;
}

int MockRoutingManager::RelayDataFrame(Packet* pkt) {

	hdr_cmn* cmn_h = hdr_cmn::access(pkt);
	hdr_ip* ip_h = hdr_ip::access(pkt);
	hdr_c2power* c2p_h = hdr_c2power::access(pkt);
	hdr_c2p_rout* rout_h = hdr_c2p_rout::access(pkt);
	hdr_c2p_data* data_h = hdr_c2p_data::access(pkt);

	if (Debug(C2POWER_DEBUG_ROUTING))
		printf("node %d: int MockRoutingManager::RelayDataFrame(Packet* pkt)\n", ag_->addr_);

	if ( rout_h->h_type != C2POWER_ROUT_DATA && cmn_h->direction() == hdr_cmn::UP) {
		printf("\tERROR: expected C2POWER_ROUT_DATA\n" );
		exit(-1);
	}

	if ( c2p_h->tipo != C2POWER_ROUTING ) {
		// this is a packet received from higher layers to be ralayed
		if (Debug(C2POWER_DEBUG_ROUTING))
			printf("\tc2p_h->tipo != C2POWER_DATA - this is source node\n" );

		if ( routeTable->existRoute( ip_h->daddr() ) ) {
			// There is a route in the table... send the packet

			Route* r = routeTable->getRoute(ip_h->daddr());

			if (Debug(C2POWER_DEBUG_ROUTING))
				printf("    packet sent through route %s\n", r->print().c_str() );

			cmn_h->direction() = hdr_cmn::DOWN;
//			cmn_h->next_hop() = IP_BROADCAST; // to send the packet directly (without the need of arp pkts..)
			c2p_h->tipo = C2POWER_ROUTING;
			rout_h->dst = ip_h->daddr();
			rout_h->h_type = C2POWER_ROUT_DATA;

			fillRoute(rout_h,r);

			data_h->next_node = r->addrs[1].c2p_addr;

			// Broadcast or unicast forward of packet
			if ( use_broadcast <= 0) {
				nsaddr_t next_ipif = (nsaddr_t)getNextIPIF(rout_h,ag_->addr_);
				if (Debug(C2POWER_DEBUG_ROUTING)) {
					printf(" - DATA unicast\n"); parse32Addr(next_ipif); printf("\n");
				}
				cmn_h->next_hop() = next_ipif;
				ip_h->daddr() = getDestIPIF(rout_h);
				ip_h->saddr() = (nsaddr_t)getEgressIPIF(rout_h,ag_->addr_);
			}
			else {
				if (Debug(C2POWER_DEBUG_ROUTING)) printf(" - DATA broadcast\n");
				cmn_h->next_hop() = IP_BROADCAST;
//				ip_h->daddr() = IP_BROADCAST;
			}

			sendPkt( pkt, rout_h->addrs[0].ipif_addr_out );
		}
		else {
			// There is no route in the table...
			// update header info, store packet and start RREQ proc.
			if (Debug(C2POWER_DEBUG_ROUTING))
				printf("    No routes; send RREQ and store packet.\n");

			cmn_h->direction() = hdr_cmn::DOWN;
//			cmn_h->next_hop() = IP_BROADCAST; // to send the packet directly (without the need of arp pkts..)
			c2p_h->tipo = C2POWER_ROUTING;
			rout_h->dst = ip_h->daddr();
			rout_h->h_type = C2POWER_ROUT_DATA;

			storedPkts.push_back(pkt);

			sendRREQ( ip_h->daddr() );
		}

		return TCL_OK;
	}
	else {
		// this is a data packet that has to be ralayed
		// if this node is the next hop, then relay pkt otherwise remove it
		if ( rout_h->h_type != C2POWER_ROUT_DATA ) {
			printf("    ERROR expected C2POWER_ROUT_DATA pkt type\n");
			exit(-1);
		}

		// if there is no route in the packet (data packet is at the source)
		if ( rout_h->routelen <= 0 ) {

			cmn_h->direction() = hdr_cmn::DOWN;
			c2p_h->tipo = C2POWER_ROUTING;
			rout_h->dst = ip_h->daddr();
			rout_h->h_type = C2POWER_ROUT_DATA;

			Route* r = routeTable->getRoute(ip_h->daddr());

			fillRoute(rout_h,r);

			data_h->next_node = rout_h->addrs[1].c2p_addr;

			if (Debug(C2POWER_DEBUG_ROUTING))
				printf("    DATA pkt updated with route and sent to %d | R:%s\n",data_h->next_node,r->printShort().c_str() );

			// Broadcast or unicast forward of packet
			if ( use_broadcast <= 0) {
				nsaddr_t next_ipif = (nsaddr_t)getNextIPIF(rout_h,ag_->addr_);
				if (Debug(C2POWER_DEBUG_ROUTING)) {
					printf(" - DATA unicast\n"); parse32Addr(next_ipif); printf("\n");
				}
				cmn_h->next_hop() = next_ipif;
				ip_h->daddr() = getDestIPIF(rout_h);
				ip_h->saddr() = (nsaddr_t)getEgressIPIF(rout_h,ag_->addr_);
			}
			else {
				if (Debug(C2POWER_DEBUG_ROUTING)) printf(" - DATA broadcast\n");
				cmn_h->next_hop() = IP_BROADCAST;
//				ip_h->daddr() = IP_BROADCAST;
			}

			sendPkt( pkt, r->addrs[0].ipif_addr_out );

			return TCL_OK;
		}

		// if there's no handled routes delete the packet (RERR was sent while erasing the handled route)
		if ( !handledRoutes->existRoute(ip_h->daddr()) ) {

		  if (Debug(C2POWER_DEBUG_ROUTING)) printf("    No handled routes for dst=%d; pkt deleted\n",ip_h->daddr() );

		  printf("total of %d routes\n", handledRoutes->getNumRoutes());
		  if (Debug(C2POWER_DEBUG_ROUTING)) {
		    for (int i=0; i < handledRoutes->getNumRoutes(); i++) {
		      Route* r = handledRoutes->getEntry(i);
		    // Dump data on the route:
		      printf("route: %s\n", r->print().c_str());
		    }

		  }

		
		  ag_->Drop(pkt,NO_HANDLED_ROUTES);
//			else
//				Packet::free(pkt);
			return TCL_OK;
		}

		// if there is a route I have to relay the packet
		if ( ag_->addr_ == data_h->next_node ) {
			int nextnode = getNextNode(rout_h,ag_->addr_);

			handledRoutes->updateRouteUsage(rout_h->addrs,rout_h->routelen, NOW);

			cmn_h->direction_ = hdr_cmn::DOWN;
			c2p_h->tipo = C2POWER_ROUTING;
			rout_h->h_type = C2POWER_ROUT_DATA;
			data_h->next_node = nextnode;

			if (Debug(C2POWER_DEBUG_ROUTING)) printf("    DATA pkt Relayed to node %d\n", nextnode );

			// Broadcast or unicast forward of packet
			if ( use_broadcast <= 0) {
				nsaddr_t next_ipif = (nsaddr_t)getNextIPIF(rout_h,ag_->addr_);
				if (Debug(C2POWER_DEBUG_ROUTING)) {
					printf(" - DATA unicast\n"); parse32Addr(next_ipif); printf("\n");
				}
				cmn_h->next_hop() = next_ipif;
//				ip_h->daddr() = next_ipif;
//				ip_h->saddr() = (nsaddr_t)getEgressIPIF(rout_h,ag_->addr_);
			}
			else {
				if (Debug(C2POWER_DEBUG_ROUTING)) printf(" - DATA broadcast\n");
				cmn_h->next_hop() = IP_BROADCAST;
//				ip_h->daddr() = IP_BROADCAST;
			}

			sendPkt( pkt, getEgressIPIF(rout_h,ag_->addr_) );
		}
		else {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("    DATA pkt deleted (not for me)\n");
			if (Debug(C2POWER_DEBUG_ROUTING))
				ag_->Drop(pkt,NOT_FOR_ME);
			else
				Packet::free(pkt);
		}
		return TCL_OK;
	}

	printf("    c2power_routing_mock.cc: error in RelayDataFrame().\n");
	exit(-1);

}

// dst of RREQ is the _DEST_ IP address!
int MockRoutingManager::sendRREQ(int dst) {

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: Send a RREQ, to %d.%d.%d.%d time: %f\n",
			ag_->addr_,
			((dst & 0xff000000)>>24),
			((dst & 0x00ff0000)>>16),
			((dst & 0x0000ff00)>>8),
			((dst & 0x000000ff)),
			NOW );

	Packet* pkt = Packet::alloc();

	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdrc->ptype() = PT_C2P_ROUTING;
	hdrc->next_hop() = IP_BROADCAST;
	hdrc->addr_type() = NS_AF_INET;
	hdrc->direction() = hdr_cmn::DOWN;

	// Needed to be not discarded by IP interface
	hdr_ip *iph = hdr_ip::access(pkt);
	iph->daddr() = IP_BROADCAST;// << Address::instance().nodeshift();
//	iph->saddr() = ag_->addr_;

	hdr_c2power* hdr = hdr_c2power::access(pkt);
	hdr->tipo = C2POWER_ROUTING;
	hdr->mitt_id = ag_->addr_;

	hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt);
	hrcom->h_type = C2POWER_ROUT_RREQ;
	hrcom->routelen = 1;
	hrcom->dst = dst;

	if ( rreqTable->chkAtSRC(ag_->addr_,dst) != -1 ) {
//		if (Debug(C2POWER_DEBUG_ROUTING)) printf( rreqTable->print().c_str() );
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("    rreq already sent...\n");
		return TCL_OK;
	}

//	if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s", rreqTable->print().c_str() );

	// initialize the route
	c2p_route_entry* re = new c2p_route_entry;
	re->c2p_addr = ag_->addr_;


	// retrieve energy measures related to src node (short renge IF).
	ClMsgC2pPtx* m = new ClMsgC2pPtx(UNICAST, ag_->getStack()->getEntry(0).getPHYid() );
	//	printf("sending clmsg to %d... \n", ag_->getStack()->getEntry(0).getPHYid());
	ag_->sendSyncClMsg(m);
	double Ptx = m->getTXPower();
	ClMsgC2pDataRate* md = new ClMsgC2pDataRate(UNICAST, ag_->getStack()->getEntry(0).getMACid() );
	ag_->sendSyncClMsg(md);
	double Rb = md->getDataRate();
	delete m;
	delete md;

	re->energyCost = Ptx/Rb;
	re->lifetime = ag_->getEnergyModel()->getResidualEnergy();

	hrcom->getAddrs()[0] = *re;

	hdr_c2p_rreq* hrreq = HDR_C2P_REQUEST(pkt);
	hrreq->rreqId = ++rReqIdCount;

	if (Debug(C2POWER_DEBUG_ROUTING)) hrcom->printRoute();

	rreqTable->addRReq(ag_->addr_,dst,rReqIdCount,NOW);
	if (rreqValidityPeriod_->status() != TIMER_PENDING )
		rreqValidityPeriod_->resched(4);

	// Schedule a RREQFailed timer
	rreqFail_ = new RREQFailed(this, hrcom->dst, hrreq->rreqId);
	rreqFail_->sched(rReqTimeOut+0.5);

	//XXX here I suppose that RREQ starts always from short range (the first stack)
	int ipif_addr_out = ag_->getStack()->getEntry(0).getIPIFaddr();
	hrcom->getAddrs()[hrcom->routelen-1].ipif_addr_out = ipif_addr_out;
	sendPkt(pkt, ipif_addr_out);

	return TCL_OK;
}

int MockRoutingManager::sendRREP(Route* rt, int rreqid) {

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("node %d: Send a RREP, time: %f\n", ag_->addr_, NOW );

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tIn route: %s\n", rt->print().c_str() );

	Packet* pkt = Packet::alloc();

	// Common Header
	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdrc->ptype() = PT_C2P_ROUTING;
	//hdrc->next_hop() = IP_BROADCAST;
	hdrc->addr_type() = NS_AF_INET;
	hdrc->direction() = hdr_cmn::DOWN;

	// Needed to be not discarded by IP interface
	hdr_ip *iph = hdr_ip::access(pkt);
	//iph->daddr() = IP_BROADCAST;
	//iph->saddr() = ag_->addr_;

	// C2power Header
	hdr_c2power* hdr = hdr_c2power::access(pkt);
	hdr->tipo = C2POWER_ROUTING;
	hdr->mitt_id = ag_->addr_;

	// General Header for c2power routing
	hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt);
	hrcom->h_type = C2POWER_ROUT_RREP;

	// Initialize the route
	Route* rev_rt = rt->reverse();
	hrcom->dst = rev_rt->addrs.at(rev_rt->getLength()-1).c2p_addr;
	fillRoute(hrcom, rev_rt);

	if ( use_broadcast <= 0) {
		nsaddr_t next_ipif = (nsaddr_t)rev_rt->addrs.at(1).ipif_addr_in;
		if (Debug(C2POWER_DEBUG_ROUTING)) {
			printf(" - RREP unicast\n"); parse32Addr(next_ipif); printf("\n");
		}
		hdrc->next_hop() = next_ipif;
		iph->daddr() = next_ipif;
		iph->saddr() = (nsaddr_t)rev_rt->addrs.at(0).ipif_addr_out;
	}
	else {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf(" - RREP broadcast\n");
		hdrc->next_hop() = IP_BROADCAST;
		iph->daddr() = IP_BROADCAST;
//		iph->saddr() = ag_->addr_;
	}

	//if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tRev route: %s\n", rev_rt->print().c_str() );
	if (Debug(C2POWER_DEBUG_ROUTING)) hrcom->printRoute();

	// Specific Header for route reply
	hdr_c2p_rrep* hrrep = HDR_C2P_REPLY(pkt);
	hrrep->rreqId = rreqid;
	hrrep->next_hop = rev_rt->addrs.at(1).c2p_addr;
	hrrep->cost =  rt->cost;

	sendPkt(pkt,rev_rt->addrs[0].ipif_addr_out);

	return TCL_OK;
}

int MockRoutingManager::sendRERR( Route* route , int next_node_unreach ) {

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: Send a RERR, time: %f\n", ag_->addr_, NOW );

	Packet* pkt = Packet::alloc();

	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdrc->ptype() = PT_C2P_ROUTING;
	//hdrc->next_hop() = IP_BROADCAST;
	hdrc->addr_type() = NS_AF_INET;
	hdrc->direction() = hdr_cmn::DOWN;

	// Needed to be not discarded by IP interface
	hdr_ip *iph = hdr_ip::access(pkt);
	//iph->daddr() = IP_BROADCAST;
	//iph->saddr() = ag_->addr_;

	hdr_c2power* hdr = hdr_c2power::access(pkt);
	hdr->tipo = C2POWER_ROUTING;
	hdr->mitt_id = ag_->addr_;

	// set up the routing header reversing the handled route
	hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt);
	hrcom->h_type = C2POWER_ROUT_RERR;
	hrcom->dst = route->addrs[0].c2p_addr;

	Route* rev_rt = route->reverse();
	fillRoute(hrcom,rev_rt);

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tHandled route: %s\n", route->print().c_str() );
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tRev route: %s\n", rev_rt->print().c_str() );
	if (Debug(C2POWER_DEBUG_ROUTING)) hrcom->printRoute();

	hdr_c2p_rerr* hrerr = HDR_C2P_ERROR(pkt);
	hrerr->next_unreachable_node = next_node_unreach;
	hrerr->next_hop = getNextNode(hrcom,ag_->addr_);

	if ( use_broadcast <= 0) {
		nsaddr_t next_ipif = getNextIPIF(hrcom,ag_->addr_);//(nsaddr_t)rev_rt->addrs.at(1).ipif_addr_in;
		if (Debug(C2POWER_DEBUG_ROUTING)) {
			printf(" - RREP unicast\n"); parse32Addr(next_ipif); printf("\n");
		}
		hdrc->next_hop() = next_ipif;
		iph->daddr() = next_ipif;
		iph->saddr() = getEgressIPIF(hrcom,ag_->addr_);//(nsaddr_t)rev_rt->addrs.at(0).ipif_addr_out;
	}
	else {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf(" - RREP broadcast\n");
		hdrc->next_hop() = IP_BROADCAST;
		iph->daddr() = IP_BROADCAST;
		//iph->saddr() = ag_->addr_;
	}

	sendPkt(pkt, getEgressIPIF(hrcom,ag_->addr_) );

	return TCL_OK;

	/*if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: Send a RERR, time: %f\n", ag_->addr_, NOW );

	Packet* pkt = Packet::alloc();

	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdrc->ptype() = PT_C2P_ROUTING;
	hdrc->next_hop() = IP_BROADCAST;
	hdrc->addr_type() = NS_AF_INET;
	hdrc->direction() = hdr_cmn::DOWN;

	// Needed to be not discarded by IP interface
	hdr_ip *iph = hdr_ip::access(pkt);
	iph->daddr() = IP_BROADCAST;
//	iph->saddr() = ag_->addr_;

	hdr_c2power* hdr = hdr_c2power::access(pkt);
	hdr->tipo = C2POWER_ROUTING;
	hdr->mitt_id = ag_->addr_;

	// set up the routing header reversing the handled route
	hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt);
	hrcom->h_type = C2POWER_ROUT_RERR;
	hrcom->dst = route->addrs[0].c2p_addr;

	Route* rev_rt = route->reverse();
	fillRoute(hrcom,rev_rt);

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tHandled route: %s\n", route->print().c_str() );
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tRev route: %s\n", rev_rt->print().c_str() );
	if (Debug(C2POWER_DEBUG_ROUTING)) hrcom->printRoute();

	hdr_c2p_rerr* hrerr = HDR_C2P_ERROR(pkt);
	hrerr->next_unreachable_node = next_node_unreach;
	hrerr->next_hop = getNextNode(hrcom,ag_->addr_);

	sendPkt(pkt, getEgressIPIF(hrcom,ag_->addr_) );

	return TCL_OK;*/
}

int MockRoutingManager::Recv(Packet* pkt,int idSrc) {

	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt);

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: recvd pkt time: %f\n",ag_->addr_,NOW);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tc2p type %d", hrcom->h_type);
	if (Debug(C2POWER_DEBUG_ROUTING)) hrcom->printRoute();

	if(hdrc->direction() == hdr_cmn::UP) {
		if ( hrcom->h_type == C2POWER_ROUT_DATA ) {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: C2PowerRouting recvd C2POWER_ROUT_DATA, time: %f\n",ag_->addr_,NOW);
			hdr_c2p_data* data_h = hdr_c2p_data::access(pkt);

			//if (ag_->addr_ == hrcom->addrs[hrcom->routelen-1].c2p_addr) {
			if ( ag_->addr_ == hrcom->addrs[hrcom->routelen-1].c2p_addr &&
					ag_->addr_ == data_h->next_node ) {
				if (Debug(C2POWER_DEBUG_ROUTING)) printf("    C2POWER_ROUT_DATA, sent up: %f\n",NOW);
				ag_->SendUp(pkt);
			}
			else {
				RelayDataFrame(pkt);
			}
			return TCL_OK;
		}
		if ( hrcom->h_type == C2POWER_ROUT_RREQ ) {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: C2PowerRouting recvd C2POWER_ROUT_RREQ, time: %f\n",ag_->addr_,NOW);
			recvRREQ(pkt,idSrc);
			return TCL_OK;
		}
		else if ( hrcom->h_type == C2POWER_ROUT_RREP ) {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: C2PowerRouting recvd C2POWER_ROUT_RREP, time: %f\n",ag_->addr_,NOW);
			recvRREP(pkt,idSrc);
			return TCL_OK;
		}
		else if ( hrcom->h_type == C2POWER_ROUT_RERR ) {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: C2PowerRouting recvd C2POWER_ROUT_RERR, time: %f\n",ag_->addr_,NOW);
			recvRERR(pkt,idSrc);
			return TCL_OK;
		}
	}
	return TCL_OK;
}

int MockRoutingManager::recvRREQ(Packet* pkt,int idSrc) {
assert(idSrc>0);
	hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt);
	hdr_c2p_rreq* hrreq = HDR_C2P_REQUEST(pkt);

	// Retrieve the IP address of receiving module
	int lower_ipif = -1;
	for ( int i=0; i < ag_->getStack()->getSize(); i++) {
		if ( ag_->getStack()->getEntry(i).getMACid() == idSrc ) {
			lower_ipif = ag_->getStack()->getEntry(i).getIPIFaddr();
			break;
		}
		else if ( ag_->getStack()->getEntry(i).getIPIFid() == idSrc ) {
			lower_ipif = ag_->getStack()->getEntry(i).getIPIFaddr();
			break;
		}
	}
	assert(lower_ipif!=-1);

	// Update packet header with last node info
	hrcom->routelen++;
	c2p_route_entry* re = new c2p_route_entry;
	re->c2p_addr = ag_->addr_;
	re->ipif_addr_in = lower_ipif;
	hrcom->getAddrs()[hrcom->routelen-1] = *re;

	if (Debug(C2POWER_DEBUG_ROUTING)) hrcom->printRoute();
	if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s", rreqTable->print().c_str() );

	// Check if RREQ is at destination (on the basis of destination IPaddr )
	bool im_at_dest = false;
	for ( int i=0 ; i<ag_->getStack()->getSize() ; i++ ) {
		if ( ag_->getStack()->getEntry(i).getIPIFaddr() == hrcom->dst ) {
			im_at_dest = true;
			break;
		}
	}
	if ( im_at_dest ) {

		rreqTable->addRReq( hrcom->getSrc(), hrcom->dst, hrreq->rreqId, NOW );
		if (rreqValidityPeriod_->status() != TIMER_PENDING )
			rreqValidityPeriod_->resched(4);

		std::pair<int,int> key ( hrcom->getSrc(), hrreq->rreqId );

		// If a route is already decided for [src-rreqid] -> discard this rreq
		// - This is a latecomer RREQ pkt.
		if ( rreqTable->checkDecided( hrcom->getSrc(), hrreq->rreqId ) ) {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("  Decision for this S-D (%d-%d) already taken; discard RREQ.\n",key.first,key.second);
			if (Debug(C2POWER_DEBUG_ROUTING))
				ag_->Drop(pkt,ROUTE_ALREADY_DECIDED);
			else
				Packet::free(pkt);
			return TCL_OK;
		}

		// Extract 'Route' from packet.
		Route* r = new Route();
		for (int i=0; i<hrcom->routelen; i++) {
			r->addrs.push_back(hrcom->addrs[i]);
		}

		// Handle the route (store it and/or start rReqTimeOut timer)
		if ( rreqTable->getNumRoutes(hrcom->getSrc(), hrreq->rreqId) > 0 ) {
			// A route is already stored and timer for RREQs from SRC is running.
			rreqTable->addRoute(hrcom->getSrc(), hrreq->rreqId, r);
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tRREQ buffered.\n");
		}
		else {
			// this is a new RREQ.
			rreqTable->addRoute(hrcom->getSrc(), hrreq->rreqId, r);

			rreqTimeOut_ = new RREQTimeoutTimer(this, hrcom->getSrc(),hrreq->rreqId);
			rreqTimeOut_->sched(rReqTimeOut);

			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tNew RREQ buffered.\n");
		}

		Packet::free(pkt);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s", rreqTable->print().c_str() );
		return TCL_OK;
	}

	// Remove handled routes that match Src and Dst of the RREQ;
	// (this means that the handled route is no more valid)
//	bool rem = handledRoutes->remSrcDst(hrcom->addrs[0].c2p_addr, hrcom->dst);
//
//	if (rem) {
//		if (turnOnOff) checkTurnOffLongRange();
//	}

	// Check if receiving node forms a loop.
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tChk for loops..");
	int routelength = hrcom->routelen;
	for (int i=routelength-2; i>=0; i--) {
		if ( hrcom->getAddrs()[i].c2p_addr == hrcom->getAddrs()[routelength-1].c2p_addr ) {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tLOOP in the route: pkt deleted\n",ag_->addr_);
			if (Debug(C2POWER_DEBUG_ROUTING))
				ag_->Drop(pkt,ROUTE_WITH_LOOPS);
			else
				Packet::free(pkt);
			return TCL_OK;
		}
	}
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("ok\n");

	// Discard RREQ (not forward) if exceed number of hops limit
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tChk number of hops..");
	if ( hrcom->routelen > hoplimit ) {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tRoute has %d entries, hopLimit=%d => pkt deleted.\n",hrcom->routelen , hoplimit);
		if (Debug(C2POWER_DEBUG_ROUTING))
			ag_->Drop(pkt,HOP_LIMIT_EXCEEDED);
		else
			Packet::free(pkt);
		return TCL_OK;
	}
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("ok\n");

	// Check the re-broadcast limit
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tChk re-broadcast limit..");
	int i = rreqTable->findRReq( hrcom->getSrc(), hrreq->rreqId );
	if ( i >=0 && rreqTable->getEntry(i).reBroadcast > reBroadcastLimit ) {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tRe-Broadcast limit exceeded. RREQ deleted.");
		if (Debug(C2POWER_DEBUG_ROUTING))
			ag_->Drop(pkt,REBROADCAST_LIMIT_EXCEEDED);
		else
			Packet::free(pkt);
		return TCL_OK;
	}
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("ok\n");

	// Update route and forward packet
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tPKT forwarded:\n");

	rreqTable->incrReBroadcst( hrcom->getSrc(), hrcom->dst, hrreq->rreqId, NOW );
	if (rreqValidityPeriod_->status() != TIMER_PENDING )
		rreqValidityPeriod_->resched(4);

	if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s", rreqTable->print().c_str() );

	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdrc->ptype() = PT_C2P_ROUTING;
	hdrc->next_hop() = IP_BROADCAST;
	hdrc->addr_type() = NS_AF_INET;
	hdrc->direction() = hdr_cmn::DOWN;

//	 Needed to be not discarded by IP interface
	hdr_ip *iph = hdr_ip::access(pkt);
	iph->daddr() = IP_BROADCAST;// << Address::instance().nodeshift();
//	iph->saddr() = ag_->addr_;


	if ( ag_->getDownLaySAPnum() > 1 ) {

		// 1- send through short range

		// retrieve energy measures related to src node (short range IF).
		ClMsgC2pPtx* m = new ClMsgC2pPtx(UNICAST, ag_->getStack()->getEntry(0).getPHYid() );
		ag_->sendSyncClMsg(m);
		double Ptx = m->getTXPower();
		delete(m);
		ClMsgC2pDataRate* md = new ClMsgC2pDataRate(UNICAST, ag_->getStack()->getEntry(0).getMACid() );
		ag_->sendSyncClMsg(md);
		double Rb = md->getDataRate();
		delete(md);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tShort-range Eb=%g ( Ptx=%g / Rb=%g )\n", Ptx/Rb, Ptx, Rb);

		// update energy measure and send packet (energyCost is energy per bit)
		hrcom->getAddrs()[hrcom->routelen-1].energyCost = Ptx/Rb;
		hrcom->getAddrs()[hrcom->routelen-1].lifetime = ag_->getEnergyModel()->getResidualEnergy();

		int ipif_addr_out = ag_->getStack()->getEntry(0).getIPIFaddr();
		hrcom->getAddrs()[hrcom->routelen-1].ipif_addr_out = ipif_addr_out;
		sendPkt(pkt,ipif_addr_out);

		// 2- send through long range

		// Turn on long range interface

		// ----------------------------------------------------------------------
		// Note: For the last hop I use unicast packet.
		// This is to simulate a communication between MT-BS for the handshake
		// of the correct datarate.
		// Not sure that this works for all the MACs..
		// ----------------------------------------------------------------------

		// retrieve energy measures related to src node (short renge IF).
		ClMsgC2pPtx* m1 = new ClMsgC2pPtx(UNICAST, ag_->getStack()->getEntry(1).getPHYid() );
		ag_->sendSyncClMsg(m1);
		Ptx = m1->getTXPower();
		delete(m1);
		ClMsgC2pDataRate* md1 = new ClMsgC2pDataRate(UNICAST, ag_->getStack()->getEntry(1).getMACid() );
		ag_->sendSyncClMsg(md1);
		Rb = md1->getDataRate();
		delete(md1);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tLong-range Eb=%g ( Ptx=%g / Rb=%g )\n", Ptx/Rb, Ptx, Rb);

		// update energy measure and send packet (energyCost is energy per bit)
		Packet* pkt1 = pkt->copy();
		hdr_c2p_rout* hrcom = HDR_C2P_ROUTING(pkt1);
		hrcom->getAddrs()[hrcom->routelen-1].energyCost = Ptx/Rb;
		hrcom->getAddrs()[hrcom->routelen-1].lifetime = ag_->getEnergyModel()->getResidualEnergy();

		ipif_addr_out = ag_->getStack()->getEntry(1).getIPIFaddr();
		hrcom->getAddrs()[hrcom->routelen-1].ipif_addr_out = ipif_addr_out;

		sendPkt(pkt1,ipif_addr_out);

	} else {

		// retrieve energy measures related to src node (short renge IF).
		ClMsgC2pPtx* m = new ClMsgC2pPtx(UNICAST, ag_->getStack()->getEntry(0).getPHYid() );
		ag_->sendSyncClMsg(m);
		double Ptx = m->getTXPower();
		delete(m);
		ClMsgC2pDataRate* md = new ClMsgC2pDataRate(UNICAST, ag_->getStack()->getEntry(0).getMACid() );
		ag_->sendSyncClMsg(md);
		double Rb = md->getDataRate();
		delete(md);

		// update energy measure and send packet (energyCost is energy per bit)
		hrcom->getAddrs()[hrcom->routelen-1].energyCost = Ptx/Rb;
		hrcom->getAddrs()[hrcom->routelen-1].lifetime = ag_->getEnergyModel()->getResidualEnergy();

		int ipif_addr_out = ag_->getStack()->getEntry(0).getIPIFaddr();
		hrcom->getAddrs()[hrcom->routelen-1].ipif_addr_out = ipif_addr_out;

		sendPkt(pkt,ipif_addr_out);
	}

	return TCL_OK;
}

int MockRoutingManager::recvRREP(Packet* pkt,int idSrc) {

	hdr_cmn*      hdrc = hdr_cmn::access(pkt);
	hdr_ip*       iph  = hdr_ip::access(pkt);
	hdr_c2power*  hdr  = hdr_c2power::access(pkt);
	hdr_c2p_rout* hrout = HDR_C2P_ROUTING(pkt);
	hdr_c2p_rrep* hrrep = HDR_C2P_REPLY(pkt);

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\thdr_cmn ptype: %d\n", hdrc->ptype() );
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tC2Power type: %d\n", hdr->tipo);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tsrc c2p addr: %d\n", hdr->mitt_id);
	if (Debug(C2POWER_DEBUG_ROUTING)) hrout->printRoute();

	if ( hrrep->next_hop != ag_->addr_ ) {
		if (turnOnOff) {
			checkTurnOffLongRange(); // Maybe I turned on the long range for RREQ process but I was not selected
		}
		if (Debug(C2POWER_DEBUG_ROUTING))
			ag_->Drop(pkt,NOT_FOR_ME);
		else
			Packet::free(pkt);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t Not next hop; pkt deleted\n");
		return TCL_OK;
	}

	Route* r = new Route();
	header2Route(hrout,r);
	r->cost = hrrep->cost;
	Route* rn = r->reverse();
//	if (Debug(C2POWER_DEBUG_ROUTING)) printf("### %s\n",r->print().c_str());
//	if (Debug(C2POWER_DEBUG_ROUTING)) printf("### %s\n",rn->print().c_str());
	delete r;

	if ( hrout->dst == ag_->addr_ ) {
		// I'm at the destination -> update route table
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t RREP is at destination\n");

//		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "---> hrout->dst %d\n", hrout->dst );
//		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "---> hrrep->next_hop %d\n", hrrep->next_hop );
//		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "---> hrrep->next_hop %d\n", hrrep->rreqId );

		// remove record in rreqTable
		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s", rreqTable->print().c_str() );
		rreqTable->remRReq(ag_->addr_, hrrep->rreqId);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s", rreqTable->print().c_str() );

		if (rreqFail_->status() == TIMER_PENDING)
			rreqFail_->cancel();

		routeTable->updateRoute(rn, NOW);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "%s",  routeTable->print().c_str() );

		// send stored pkts
		for (int i=0 ; i<storedPkts.size() ; i++) {
			hdr_ip* hdr_ip = hdr_ip::access( storedPkts.at(i) );
			if ( hdr_ip->daddr() == rn->addrs.at(hrout->routelen-1).ipif_addr_in ) {
				Packet* newpkt = storedPkts.at(i)->copy();
				RelayDataFrame(newpkt);
				storedPkts.erase(storedPkts.begin()+i);
				i--; //Update i for pointing next vector element after erase.
			}
		}
		Packet::free(pkt);
		return TCL_OK;
	}
	else { // forward rrep on the route (Look over piggybacked route for next hop)
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t RREP is at node %d\n", ag_->addr_);

//		bool rem = handledRoutes->remSrcDst( rn->addrs.front().c2p_addr, rn->addrs.back().c2p_addr );
//		if (rem) {
//			if (turnOnOff) checkTurnOffLongRange();
//		}

		// Every handled route is added in the intermediate nodes since they must check
		// the integrity of the routes.
		handledRoutes->updateRoute( rn, NOW );
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("%s", handledRoutes->print().c_str() );

		hdrc->direction_ = hdr_cmn::DOWN;
		int nexthop = getNextNode(hrout,ag_->addr_);
		hrrep->next_hop = nexthop;

		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "\t RREP forwarded to node %d\n", nexthop );

		// Broadcast or unicast forward of RREP packet
		if ( use_broadcast <= 0) {
			nsaddr_t next_ipif = (nsaddr_t)getNextIPIF(hrout,ag_->addr_);
			if (Debug(C2POWER_DEBUG_ROUTING)) {
				printf(" - RREP unicast\n"); parse32Addr(next_ipif); printf("\n");
			}
			hdrc->next_hop() = next_ipif;
			iph->daddr() = next_ipif;
		}
		else {
			if (Debug(C2POWER_DEBUG_ROUTING)) printf(" - RREP broadcast\n");
			hdrc->next_hop() = IP_BROADCAST;
			iph->daddr() = IP_BROADCAST;
		}

		sendPkt( pkt, getEgressIPIF(hrout, ag_->addr_) );

		ag_->setBeaconingON();

		return TCL_OK;
	}

	if (Debug(C2POWER_DEBUG_ROUTING))
		ag_->Drop(pkt,UNKNOWN_REASON);
	else
		Packet::free(pkt);
	return TCL_OK;
}

int MockRoutingManager::recvRERR(Packet* pkt,int idSrc) {
//	if (Debug(C2POWER_DEBUG_ROUTING)) printf("node %d: Recvd a RERR, time: %f\n", ag_->addr_, NOW );

	hdr_cmn* hdrc = hdr_cmn::access(pkt);
	hdr_c2power* hdr = hdr_c2power::access(pkt);
	hdr_c2p_rout* hrout = HDR_C2P_ROUTING(pkt);
	hdr_c2p_rerr* hrerr = HDR_C2P_ERROR(pkt);

	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\thdr_cmn ptype: %d\n", hdrc->ptype() );
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\thdr_c2power tipo: %d\n", hdr->tipo);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tsrc c2p addr: %d\n", hdr->mitt_id);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tdest c2p addr: %d\n", hrout->dst);
	if (Debug(C2POWER_DEBUG_ROUTING)) hrout->printRoute();

	if ( hrerr->next_hop != ag_->addr_ ) {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t Not next hop; pkt deleted\n");
		if (Debug(C2POWER_DEBUG_ROUTING))
			ag_->Drop(pkt,NOT_FOR_ME);
		else
			Packet::free(pkt);
		return TCL_OK;
	}

	if ( hrout->dst == ag_->addr_) {

		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t RERR is at destination\n");

		// Remove from route table routes using the failed link
		routeTable->remRoutes1( hdr->mitt_id , hrerr->next_unreachable_node );

		return TCL_OK;
	}
	else {
		int egressIPIF = getEgressIPIF(hrout, ag_->addr_);
		int nextNode = getNextNode(hrout, ag_->addr_);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t RERR is forwaeded to node %d (getEgressIPIF=%d)\n",nextNode,egressIPIF);


		// - Delete from handled routes the route piggybacked by RERR
		// - Forward rerr on the route (Look over piggybacked route for next hop)
//		Route* r = new Route();
//		header2Route( hrout, r );
//		if (Debug(C2POWER_DEBUG_ROUTING)) printf("%s",r->print());
//		handledRoutes->remRoutes3( r );
//		if (Debug(C2POWER_DEBUG_ROUTING)) printf("%s",r->print());
//		delete r;
		bool rem = handledRoutes->remSrcDst( hrout->addrs[hrout->routelen-1].c2p_addr, hrout->addrs[0].c2p_addr );

		if (rem) {
			if (turnOnOff) checkTurnOffLongRange();
		}

		hrerr->next_hop = nextNode;
		sendPkt(pkt, egressIPIF );

	}
	if (Debug(C2POWER_DEBUG_ROUTING))
		ag_->Drop(pkt,UNKNOWN_REASON);
	else
		Packet::free(pkt);

	return TCL_OK;
}

int MockRoutingManager::selectRoute( int src, int rreqId ) {
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\tnode %d: selectRoute called (src=%d, rreqId=%d), time: %f\n", ag_->addr_, src, rreqId, NOW );

	std::vector<Route*>* routes = rreqTable->getRoutes(src,rreqId);

//	Selection based on minimum hop
//	int idx = 0;
//	int tmplenght = MAX_RT_LEN;
//	for (int i=0; i<rreqTable->getNumRoutes(src,rreqId); i++ ) {
//		if ( tmplenght > routes->at(i).getLength() ) {
//			tmplenght = routes->at(i).getLength();
//			idx = i;
//		}
//	}

	//	Selection based on Energy - Strategy 1
	int idx = -1;
	double cost_tmp = numeric_limits<double>::max();
	double totEb;
	double minLifetime;
	double sum;

	for (int i=0; i<rreqTable->getNumRoutes(src,rreqId); i++ ) {

		totEb = 0;
		for ( unsigned int j = 0; j < routes->at(i)->getLength()-1; j++ ) {
			totEb += routes->at(i)->addrs[j].energyCost;
		}
		totEb *= alpha_;

		minLifetime = numeric_limits<double>::max();
		for ( unsigned int k = 0; k < routes->at(i)->getLength()-1; k++ ) {
			minLifetime = minLifetime > routes->at(i)->addrs[k].lifetime ? routes->at(i)->addrs[k].lifetime : minLifetime;
		}
		minLifetime = (1.0-alpha_)/minLifetime;

		sum = totEb + minLifetime;

		// for debug purposes
		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "\t%s\n", routes->at(i)->print().c_str() );
		if (Debug(C2POWER_DEBUG_ROUTING)) printf( "\t  --> Cost of route=%g (%g + %g) ", sum, totEb, minLifetime );

		if ( cost_tmp > sum ){
			cost_tmp = sum;
			idx = i;
			if (Debug(C2POWER_DEBUG_ROUTING)) printf( "selected (cost_tmp=%g).\n",cost_tmp);
		}
		else
			if (Debug(C2POWER_DEBUG_ROUTING)) printf( "\n");
	}
	if (Debug(C2POWER_DEBUG_ROUTING)) printf( "\tChosen route No. %d\n" ,idx);

	assert( idx != -1 );
	routes->at(idx)->cost = cost_tmp;
	sendRREP(routes->at(idx), rreqId);

	return TCL_OK;
}

int MockRoutingManager::failedRREQ( int dstIP, int rreqId ) {

	if (rreqTable->chkAtSRC(ag_->addr_,dstIP) >= 0) {
		// RREQ failed..
		rreqTable->remRReq(ag_->addr_, dstIP, rreqId);
		sendRREQ(dstIP);
//		printf("C2power %d started RREQ becouse of a RREQ failed.\n",ag_->addr_);
	}

	return TCL_OK;
}

void MockRoutingManager::sendPkt( Packet* pkt, int egress_ipif ) {

//	if (Debug(C2POWER_DEBUG_ROUTING)) printf("sendPkt( pkt, %d );\n", egress_ipif);

	int using_what = ag_->using_long_range;

	int stack=-1;
	for (int i=0 ; i<ag_->getStack()->getSize() ; i++ ) {
		if ( ag_->getStack()->getEntry(i).getIPIFaddr() == egress_ipif ) {
			stack = i;
			break;
		}
	}
	if (stack == -1) {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("ERROR trying to send packet with egress_ipif=%d\n",egress_ipif);
		assert(stack != -1);
	}

	if (stack == 1) {
		ag_->TurnOnOffInterface(1,1);
		if (turnOnOff>0 && turnONperiod>0) {
			timerLongRange_->resched(turnONperiod);
		}
	}

	ag_->using_long_range = stack;
	ag_->recv(pkt);
	ag_->using_long_range = using_what;
}

void MockRoutingManager::sendPkt( Packet* pkt ) {
	printf("MockRoutingManager::sendPkt( Packet* pkt ); not used or to be implemented.\n");
	printf("  use MockRoutingManager::sendPkt( Packet* pkt, int egress_ipif )  instead??\n");
	exit(-1);
}

void MockRoutingManager::fillRoute(hdr_c2p_rout* hr, Route* r) {
	for (int i=0; i<r->getLength(); i++) {
		c2p_route_entry e = r->addrs.at(i);
		hr->addrs[i] = e;
	}
	hr->routelen = r->getLength();
}

void MockRoutingManager::header2Route(hdr_c2p_rout* hr, Route* r) {
	for ( int i=0; i<hr->routelen; i++ ) {
		r->addEntry(hr->addrs[i]);
	}
}
int MockRoutingManager::getEgressIPIF(hdr_c2p_rout* hr,int thisnodeaddr) {
	for (int i=0; i<hr->routelen; i++) {
		if ( hr->addrs[i].c2p_addr == thisnodeaddr ) {
			return hr->addrs[i].ipif_addr_out;
			break;
		}
	}
	printf("Error: MockRoutingManager::getEgressIPIF(hdr_c2p_rout* hr,int thisnodeaddr) returned no values.");
	exit(-1);
}
int MockRoutingManager::getNextIPIF(hdr_c2p_rout* hr, int thisnodeaddr) {
	for (int i=0; i<hr->routelen; i++) {
		if ( hr->addrs[i].c2p_addr == thisnodeaddr ) {
			return hr->addrs[i+1].ipif_addr_in;
			break;
		}
	}
	printf("Error: MockRoutingManager::getEgressIPIF(hdr_c2p_rout* hr,int thisnodeaddr) returned no values.");
	exit(-1);
}
int MockRoutingManager::getDestIPIF(hdr_c2p_rout* hr) {
	return hr->addrs[hr->routelen-1].ipif_addr_in;
}

int MockRoutingManager::getNextNode(hdr_c2p_rout* hr, int thisnodeaddr) {
	for (int i=0; i<hr->routelen; i++) {
		if ( hr->addrs[i].c2p_addr == thisnodeaddr ) {
			return hr->addrs[i+1].c2p_addr;
			break;
		}
	}
	printf("Error: MockRoutingManager::getEgressIPIF(hdr_c2p_rout* hr,int thisnodeaddr) returned no values.");
	exit(-1);
}

bool MockRoutingManager::checkTurnOffLongRange() {
	if (turnOnOff <=0 ) return false;

	// Check if I'm handling routes to update
	bool canIturnOff = true;
	for (int i=0; i < routeTable->getNumRoutes(); i++) {
		Route* r = routeTable->getEntry(i);
		// Return if I'm before the destination in some route (means that I have to upload data)
		if ( r->addrs.at(r->getLength()-2).c2p_addr == ag_->addr_ ) {
			return false;
		}
	}
	for (int i=0; i < handledRoutes->getNumRoutes(); i++) {
		Route* r = handledRoutes->getEntry(i);
		// Return if I'm before the destination in some route (means that I have to upload data)
		if ( r->addrs.at(r->getLength()-2).c2p_addr == ag_->addr_ ) {
			return false;
		}
	}
	if (canIturnOff) {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t%f c2p node %d: turn off long range\n",NOW,ag_->addr_);
		ag_->TurnOnOffInterface(0,1);
		return true;
	}
}

void MockRoutingManager::refreshRREQTable() {

	rreqTable->Refresh(10, NOW);

	rreqValidityPeriod_->resched(10);
}

void MockRoutingManager::refreshRouteTable() {
	if (routeValidity <= 0) {return;}

// Note: handledRoutes are managed (added and/or removed) on the basis of received
//       routing control packets.
//       Here we delete them considering some seconds of delay with respect to the
//       routes in routeTable. We don't want to fall in the situation where the src
//       knows a route but a relaying node not. (Means a possible packet loss without
//       any reason)
	if (Debug(C2POWER_DEBUG_ROUTING)) {
		printf("\t%f - c2p node %d - Call on MockRoutingManager::refreshRouteTable()\n",NOW,ag_->addr_);
		printf("Initial routeTable:");
		printf("%s", routeTable->print().c_str() );
		printf("Initial handledRoutes:");
		printf("%s", handledRoutes->print().c_str() );
	}

	//routeTable->Refresh(routeValidity, NOW);
	for (int i=0; i<routeTable->getNumRoutes(); i++) {
		Route* r = routeTable->getEntry(i);
		if ( (NOW - r->time_) > routeValidity ) {
			sendRREQ( r->getDestIP() );
		}
	}

	bool rem = handledRoutes->RefreshLastUsage(NOW);
	if (rem) {
		if (turnOnOff) checkTurnOffLongRange();
	}

	if (Debug(C2POWER_DEBUG_ROUTING)) {
		printf("Final routeTable:");
		printf("%s", routeTable->print().c_str() );
		printf("Final handledRoutes:");
		printf("%s", handledRoutes->print().c_str() );
	}

	routeValidityPeriod_->resched(1);
}

// Note: this class simulates that the long range is periodically enabled in order
//       to keep updated infos about connection.
void MockRoutingManager::periodicLongScanning(bool firstCall) {
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t%f - MockRoutingManager::periodicLongScanning()\n",NOW);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t  turnOnOff     = %d\n",turnOnOff);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t  turnONperiod  = %f\n",turnONperiod);
	if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t  turnOFFperiod = %f\n",turnOFFperiod);

	if (turnOnOff <= 0) {return;}
	if (turnONperiod <=0) {return;}

	if (firstCall){
		timerLongRange_->resched(turnONperiod);
		return;
	}

	if( isLongON ) {
		// I came from an ON period (or I need long IF)

		if ( checkTurnOffLongRange() ) {
			// I turned OFF -> schedule an OFF period
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t  (1) schedule OFF period\n");
			timerLongRange_->resched(turnOFFperiod);
		}
		else {
			// Not possible to turn OFF -> wait for an ON period
			if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t  (1) schedule ON period\n");
			timerLongRange_->resched(turnONperiod);
		}

	} else {
		// previously was a OFF period, I need to wake up Long IF ad schedule an ON period
		ag_->TurnOnOffInterface(1,1);
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("\t  (1) schedule ON period\n");
		timerLongRange_->resched(turnONperiod);
	}
}

