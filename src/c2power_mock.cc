#include "c2power_mock.h"
//#include "c2power_beacon.h"
#include <mphy_pktheader.h> //access to snr-based values
#include <math.h> //log10 function
//#include <module.h>
//#include <node-core.h>

void parse32Addr(nsaddr_t addr)
{
	int a=0,b=0,c=0,d=0;

	a= (addr & 0xff000000)>>24;
	b= (addr & 0x00ff0000)>>16;
	c= (addr & 0x0000ff00)>>8;
	d= (addr & 0x000000ff);

	printf("%d.%d.%d.%d",a,b,c,d);
}

MockClusterManager::MockClusterManager(C2PowerLayer* ag) :beaconTimer_(this,&ClusterManager::SendBeaconAndReschedule), beaconPeriod(ag->beaconPeriod),beaconingON(true),
neighbourListUpdateTimer_(this,&ClusterManager::RefreshNeighbourList),neighbourListPeriod(ag->neighbourListPeriod),
neighbourListRemoveTreshold(ag->neighbourListRemoveTreshold)
{
  ag_ = ag;
  RefreshNeighbourList();
  //beaconing = NULL;
}

MockClusterManager::~MockClusterManager() {
  //Beaconing(0);
}

int MockClusterManager::Beaconing() {
	if (beaconingON)
	{
		beaconTimer_.resched(beaconPeriod); //reschedule C2PowerLayer::StartBeaconingAndReschedule()
	}

  return TCL_OK;
}

int MockClusterManager::BeaconingON() {

  beaconingON = true;
  beaconTimer_.resched(beaconPeriod); //reschedule C2PowerLayer::StartBeaconingAndReschedule()

  return TCL_OK;
}

int MockClusterManager::BeaconingOFF() {

  beaconingON = false;

  return TCL_OK;
}

int MockClusterManager::RecvBeacon(Packet* pkt) {
  /******************
HERE
you can see an example of processing of a custom C2POWER packet.
   ******************/
  hdr_MPhy *ph = HDR_MPHY(pkt);

  hdr_c2power* hdr = hdr_c2power::access(pkt);
  hdr_ip *iph = hdr_ip::access(pkt);

  C2Power_NeighbourListEntry entry;
	entry.SetAddr(hdr->mitt_id);
	entry.SetTimeStamp(Scheduler::instance().clock());
	entry.SetSNR(10*log10(ph->Pr/ph->Pn));
	entry.SetSINR(10*log10(ph->Pr/(ph->Pn+ph->Pi)));

	entry.SetLongSNR(hdr->long_snr);
	entry.SetLongBSSID(hdr->LongBSS_ID);
	entry.SetLongDataRate(hdr->LongDataRate);
	entry.SetShortAddr(iph->saddr());
	entry.SetLongAddr(hdr->LongAddr);

  int ret = AddNode(entry);
  Packet::free(pkt);
  return ret;
}

int MockClusterManager::AddNode(C2Power_NeighbourListEntry entry) {
  neigbour_list_.addEntry(entry);
  return TCL_OK;
}

int MockClusterManager::RefreshNeighbourList()
{
	MR_PHY_MIB phymib;
	if (ag_->Debug(C2POWER_DEBUG_CLUSTERING))   	printf("node %d: refreshing neighbour list, time: %f\n",ag_->addr_,Scheduler::instance().clock());

	if(neighbourListPeriod<=0){
		//error
	}
	else{
		for(int i=0;i<neigbour_list_.getListSize();i++)
		{
			if(Scheduler::instance().clock()-neigbour_list_[i].GetTimeStamp()>=neighbourListRemoveTreshold)
			{
			  if (ag_->Debug(C2POWER_DEBUG_CLUSTERING)) printf(" obsolete entry: node %d, last update %f seconds ago (removed)\n",neigbour_list_[i].GetAddr(),Scheduler::instance().clock()-neigbour_list_[i].GetTimeStamp());

			    // Riccardo: advertise the c2power core about leaving neigbour.
				ag_->neigbourRemoved( neigbour_list_.getEntry(i).GetAddr() );

				neigbour_list_.removeEntry(i);
				i--;//update index after removing
			}
		}
		neighbourListUpdateTimer_.resched(neighbourListPeriod);
	}


	if (ag_->Debug(C2POWER_DEBUG_CLUSTERING)) {
		//printf("______________________\n");
		printf("NODE %d NEIGHBOUR LIST:\n",ag_->addr_);
		if(neigbour_list_.getListSize()>0){
			printf("-----------------------------------------------------------------------\n");
			printf("|%*s  (IP address) |  SNR   || BSSID (IP addr)  |  SNR   |  %*s    |\n",3,"ID",6,"rate");
			printf("-----------------------------------------------------------------------\n");
		}
		for(int idx=0;idx <neigbour_list_.getListSize();idx++)
		{
			if(neigbour_list_[idx].GetLongBSSID()>=0)
			{
			printf("|%*d    ",3,neigbour_list_[idx].GetAddr());
			parse32Addr(neigbour_list_[idx].GetShortAddr());
			printf("    | %3.3f ||   %d    ", neigbour_list_[idx].GetSNR(),neigbour_list_[idx].GetLongBSSID());
			parse32Addr(neigbour_list_[idx].GetLongAddr());
			printf("   | %3.3f |  %*s  %2.2f|\n",neigbour_list_[idx].GetLongSNR(),8,PhyMode2str((PhyMode)neigbour_list_[idx].GetLongDataRate()),(phymib.getRate((PhyMode)neigbour_list_[idx].GetLongDataRate()))/1000000);

			}
			else
			{
				printf("  %*d | %3.3f ||\n",3,neigbour_list_[idx].GetAddr(), neigbour_list_[idx].GetSNR());
			}


		}
		if(neigbour_list_.getListSize()>0)printf("-----------------------------------------------------------------------\n");
		printf("______________________\n");
	}

	return TCL_OK;
}

int MockClusterManager::SendBeaconAndReschedule() {

  ag_->SendBeacon();
  Beaconing();
  return TCL_OK;
}

C2Power_NeighbourList& MockClusterManager::GetNeighbourList()
{
	return neigbour_list_;
}


MultiradioRelayingManager::MultiradioRelayingManager(C2PowerLayer* ag, ClusterManager* cm)
{
  ag_ = ag;
  cm_ = cm;
  //RefreshNeighbourList();
  //beaconing = NULL;
}

int MultiradioRelayingManager::SelectInterface(int shortInterfaceID, int longInterfaceID,int LatestDataMode,PhyMode ShortDataRateVal)
{
	//CHECK IF COOPERATION IS REASONABLE AND POSSIBLE
	if(IsCooperationPossible(LatestDataMode,ShortDataRateVal))
	{
		return shortInterfaceID;
	}
	else
	{
   		return longInterfaceID;
   	}
}

bool MultiradioRelayingManager::IsCooperationPossible(int LatestDataMode,PhyMode ShortDataRateVal)
{
	if(cm_->GetNeighbourList().getListSize()>0)
		{
			//get a neighbour with best parameters
			C2Power_NeighbourListEntry tmp =cm_->GetNeighbourList().getBestNeighbour();
			MR_PHY_MIB phymib;
			double myRateToBS = (phymib.getRate((PhyMode)LatestDataMode))/1000000;
			double myRateToBestRelay = (phymib.getRate(ShortDataRateVal))/1000000;
			double RateFromRelayToBS = (phymib.getRate((PhyMode)tmp.GetLongDataRate()))/1000000;



			//double var2 = 1.0;
			//chceck whether transmission might be improved
			if(RateFromRelayToBS*myRateToBS*myRateToBestRelay!=0)
			{
				double var1 = 1.0/myRateToBS;
				double var2 = 1.0/myRateToBestRelay;
				double var3 = 1.0/RateFromRelayToBS;

				if(var1>var2+var3)
				{
					//printf("Rd=%2.1f, R1=%2.1f, R2=%2.1f\n",myRateToBS, myRateToBestRelay,RateFromRelayToBS);
					//printf("1/rd = %2.5f\n", 1/myRateToBS);
					//printf("1/R1 + 1/R2 = %2.5f\n", 1/myRateToBestRelay+1/RateFromRelayToBS);

					return true;
				}
			}
		}


		return false;
}
