
#ifndef ns_c2power_interfaces_h
#define ns_c2power_interfaces_h

#include "c2power_structures.h"
#include "c2power_neighbour_list.h"
#include <phymib.h>


class C2Power_NeighbourList;


/**
interface for the ClusterManager
*/
class ClusterManager {
 public:
  virtual int AddNode(C2Power_NeighbourListEntry entry) = 0;
  virtual int RecvBeacon(Packet* pkt) = 0;
  //virtual int Beaconing(bool start) = 0;
  //virtual int Beaconing() = 0;
  virtual int BeaconingON() = 0;
  virtual int BeaconingOFF() = 0;

  virtual int RefreshNeighbourList() = 0;
  virtual int SendBeaconAndReschedule() = 0;

  virtual C2Power_NeighbourList& GetNeighbourList() = 0;
  virtual ~ClusterManager() {}
};

/**
interface for the RoutingManager
*/
class RoutingManager {
 public:

	virtual int RemoveNode(int ne) = 0;

	virtual int RelayDataFrame(Packet* p) = 0;

	virtual int Recv(Packet* pkt,int idSrc) = 0;

	virtual ~RoutingManager() {}
};




/**
interfaces for the NodeSelection, for the mobile terminals and for the BS
*/
class NodeSelectionMT {
 public:
  virtual int SendStatisticsUp(statistics_node_selection* snsShort, statistics_node_selection* snsLong) = 0;
  virtual int SendStatisticsLongRange(statistics_node_selection* snsShort, statistics_node_selection* snsLong) = 0;
  virtual int SendStatisticsShortRange(statistics_node_selection* sns) = 0;

  virtual int ClearRoutes() = 0;
  virtual int ReceiveRoute(Packet* pkt) = 0;

  virtual int SetupRoute(int switchoffunused) = 0;
  virtual int RelayPacket(Packet* pkt) = 0;

  virtual int Dump() = 0;

  virtual ~NodeSelectionMT() {}
};
class NodeSelectionBS {
 public:
  virtual int ReceiveStatistics(Packet* pkt) = 0;

  virtual int RedirectPacket(Packet* pkt) = 0;

  virtual int ComputeRoutes(int green=0) = 0;

  virtual int SendRoutes() = 0;

  virtual int Dump() = 0;

  virtual ~NodeSelectionBS() {}
};

/**
interface for the RelayingManager
*/
class RelayingManager {

public:
	virtual int SelectInterface(int, int, int, PhyMode) = 0;

};
#endif // ns_c2power_interfaces_h
