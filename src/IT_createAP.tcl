################################################################################
# Create Access Point & Router procedure
################################################################################

# +------------------------------------------------------------------------+
# |                              6-7. CBR + Port                           |
# +------------------------------------------------------------------------+
# |                              5. Routing                                |
# +------------------------------------------------------------------------+
# |            3. Wireless           | |           IP  3. Wired IP         |
# +-----------------+----------------+ +----------------+------------------+
# |               2. MAC             |                  |
# +-----------------+----------------+ +----------------+------------------+
# |           1. Module/Phy          | |         To The Internet           |
# +-----------------+----------------+ ------------------------------------+
#                   |           
# +----------------------------------+ 
# |          DumbWirelessChannel     |
# +----------------------------------+ 

#global $peerstats

proc createC2PowerAP {wifiaddr wifinetmask wireaddr wirenetmask apDataMode apposx apposy} {

  global ns apPhy propagationLong  PHYDataRate maskLong per channelLong 
  global ap_macaddr apNode apIpIF_wire apIpIF_wifi apMAC apIpr apPort
  global peerstats
  global apNrgModel
  global apNode ;# in order to set Cbr modules outside ..
  global param nrgModelParam apC2Power

puts "## CREATE THE AP ##########################################"

  set apNode [$ns create-M_Node]

  #set apCbr			[new Module/CBR]
  set apPort		[new Module/Port/Map]
  set apIpr			[new Module/IP/Routing]
  set apC2Power		[new Module/C2Power]
  set apIpIF_wifi	[new Module/IP/Interface]
  set apIpIF_wire	[new Module/IP/Interface]

  $apIpIF_wifi addr   $wifiaddr
  $apIpIF_wifi subnet $wifinetmask

  $apIpIF_wire addr   $wireaddr
  $apIpIF_wire subnet $wirenetmask

  set apMAC [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail/C2PowerQueue" [$apIpIF_wifi addr] "" 100 ]
  set apPhy [new Module/MPhy/80211Phy]
  set interfap [new MInterference/MIV]
  set apPosition [new "Position/BM"]

  $apPhy set TxPower_ $param(power_tx_long)

  $apPosition setX_ $apposx
  $apPosition setY_ $apposy
  #$propagationLong  newNode $apPosition ;# it is for MPropagation/FullPropagation
  $apNode addPosition $apPosition

  #$apNode addModule 7 $apCbr       0 "apCbr    "
  $apNode addModule 6 $apPort      0 "apPort   "
  $apNode addModule 5 $apIpr       0 "apIpr    "
  $apNode addModule 4 $apC2Power   0 "apC2Power"
  $apNode addModule 3 $apIpIF_wifi 0 "apIpIFwif"
  $apNode addModule 3 $apIpIF_wire 0 "apIpIFwir"
  $apNode addModule 2 $apMAC       0 "apMAC    "
  $apNode addModule 1 $apPhy       0 "apPhy    "

  $apMAC dataMode_ $apDataMode
  $apMAC basicMode_ Mode6Mb
  $apMAC usePhy $apPhy


  set apNrgModel [new Plugin/C2PEnergyModel]
  $apNode addPlugin $apNrgModel 0 "apNRG"

  $apNrgModel setEnergy $nrgModelParam(TotalEnergy)

  $apPhy setEnergyModel
#  $apPhy setConsumption  $LongIdlePwr $LongTxPwr $LongRxPwr $LongSleepPwr
  $apPhy setConsumption  0 0 0 0 ;# AP is supposed to be attached to a power supply => no battery usage
#	$Phy_wf($id) setConsumption ENERGY_STATE_IDLE ENERGY_STATE_TX ENERGY_STATE_RCV ENERGY_STATE_SLEEP

  #$apPhy setEnergy $TotalEnergy
  #$apPhy 


  set ap_macaddr [$apMAC id];#$apMAC

  set apRA [new RateAdapter/ARF]
  $apRA attach2mac [$apMAC getMac]
  $apRA use80211g
  $apRA setmodeatindex 0

  $apMAC PeerStatsDB $peerstats

  $apPhy useMAC $apMAC
  $apPhy setPer $per
  $apPhy setSpectralMask $maskLong
  $apPhy setPropagation $propagationLong
  $apPhy setInterference $interfap

  #$apNode setConnection $apCbr       $apPort      0
  $apNode setConnection $apPort      $apIpr       0
  $apNode setConnection $apIpr       $apC2Power   0
  $apNode setConnection $apC2Power   $apIpIF_wifi $param(C2powerTrace)
  $apNode setConnection $apC2Power   $apIpIF_wire 0
  $apNode setConnection $apIpIF_wifi $apMAC       0
  $apNode setConnection $apMAC       $apPhy       0

  $apNode addToChannel  $channelLong  $apPhy 0

  $apMAC bss_id [$apMAC id] ;#"$ap_macaddr"


  $apC2Power discovery
  $apC2Power anyrange
#  $apC2Power routing

  $apC2Power nodsel_BS

  #NB: the access point has only the long range IF id
  #$apC2Power setC2PowerDevice			[$MACShort($id) Id_]
  #$apC2Power setLongRangeDevice			[$MACLong($id) Id_]
  $apC2Power setC2PowerDevice			[$apIpIF_wifi Id_]
  $apC2Power setLongRangeDevice			[$apIpIF_wifi Id_]
  #$apC2Power setShortRangeDeviceAddr	[$apIpIF_wifi addr]  
  #$apC2Power setShortRangeDeviceSubnet	[$apIpIF_wifi subnet]
  $apC2Power setLongRangeDeviceAddr		[$apIpIF_wifi addr]
  $apC2Power setLongRangeDeviceSubnet	[$apIpIF_wifi subnet]
  #$apC2Power setC2PowerDatarate Mode6Mb

#ALBANO commented them
#  $apC2Power set_rout_param  broadcast        $::rout_param(broadcast)   ;# use broadcast for routing cmd pkts (0 false or 1 true)
#  $apC2Power set_rout_param  alpha            $::rout_param(alpha) 
#  $apC2Power set_rout_param  hoplimit         $::rout_param(hoplimit)
#  $apC2Power set_rout_param  reBroadcastLimit $::rout_param(reBroadcastLimit)
#  $apC2Power set_rout_param  rreqtimeout      $::rout_param(rreqtimeout)
#  $apC2Power set_rout_param  routeValidity    $::rout_param(routeValidity) 






#  $apC2Power set_rout_param  turnLongOnOff    $::rout_param(turnLongOnOff)   ;# not used for AP...
#  $apC2Power set_rout_param  turnONperiod     $::rout_param(turnONperiod)   ;# not used for AP...
#  $apC2Power set_rout_param  turnOFFperiod    $::rout_param(turnOFFperiod)   ;# not used for AP...

puts " AP created at ([$apPosition getX_],[$apPosition getY_])  "
puts "##########################################################"

}

