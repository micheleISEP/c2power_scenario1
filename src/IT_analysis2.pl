#!/usr/bin/perl

$castro_i_dati = 2;

for ($dir_green=0;$dir_green<4;$dir_green++) {
    if ($dir_green == 0) {$dirname = "on_and_off";}
    if ($dir_green == 1) {$dirname = "on_and_offGREEN";}
    if ($dir_green == 2) {$dirname = "on_and_on";}
    if ($dir_green == 3) {$dirname = "on_and_onGREEN";}
    opendir (my $dir, $dirname);
    while ($fname = readdir($dir)) {
	if (($traffic,$rate,$num_nodes,$i,$nodsel) = $fname =~ /^run.(\d+).(\d+).(\d+).(\d+).(\d+).tcl.out/) {
	    $nodsel+=2*$dir_green;
#	    print "opening $dirname $fname $nodsel\n";
	    open FIN, "<$dirname/$fname";
	    $expected_id = 1;
	    $cost=0;
	    $packets=0;
	    while ($riga = <FIN>) {
		if ($riga =~ /^C2power/) {
		    if ($riga =~ /^C2power (\d+): 151\.000000 Trace Energy NRG_\d+ ([\d\.]+)/) {
			$found_id = $1;
			$cost += (5000-$2 - 50*0.110); # subtracting the first 50 seconds like it was idle. This way, I still consider overhead (time in tx & rx)
		    }
		    if ($riga =~ /^C2power (\d+): CBR_getrecvpkts: (\d+)/) {
			$found_id = $1;
			$packets += $2;
		    }
		    if ($expected_id != $found_id) {
			die "\nfalta um valor\n\n";
		    }
		    if ($num_nodes == $expected_id) {$expected_id = 0;}
		    $expected_id++;
		}
	    }
	    if ($expected_id != 1) {die "\nfalta o ultimo valor\n\n";}
	    close FIN;
	    $packets /= $num_nodes;
	    $cost /= $num_nodes;
	    if ($num_nodes == 5) {$num_nodes="05";}
	    if ($rate != 100) {$rate="0".$rate;}
	    my $oneresult = "$traffic,$rate,$num_nodes,$i,$nodsel,$packets,$cost\n";
	    push @results, $oneresult;
	}
    }
    closedir $dir;
}
@sortedresults = sort @results;


for ($kk=0;$kk<8;$kk++) {$pa[$kk] =0;$en[$kk] =0;}
if (2 == $castro_i_dati) {
    print "traffic,datarate,nodes,pk0b,pk0R,pk0G,pk1b,pk1R,pk1G,en0b,en0R,en0G,en1b,en1R,en1G,cost baseline,cost BUS,cost GREEN\n";
} elsif (1 == $castro_i_dati) {
    print "traffic,datarate,nodes,cost baseline,cost BUS,cost GREEN\n";
} else {
    print "traffic,datarate,nodes,pk0b,pk0R,pk0G,pk1b,pk1R,pk1G,en0b,en0R,en0G,en1b,en1R,en1G\n";
}
for ($i=0;$i<=$#sortedresults;$i++) {
    ($row,$count,$nodsel,$packets,$cost) = $sortedresults[$i] =~ /([,\.0-9]+),([0-9]),([0-9]),([0-9\.]+),([0-9\.]+)/;
#    print "$sortedresults[$i]";
#    print "$row - $count - $nodsel - $packets - $cost\n";
#    print "$nodsel\n";
    if (int($i%80/8) != $count) {die "\nerrore nell' analisi:\n\tcount $count i $i\n\n";}
    $en[$nodsel] += $cost;
    $pa[$nodsel] += $packets;
    if ($i%80 == 79) {
	for ($kk=0;$kk<8;$kk++) {$pa[$kk] /= 10;$en[$kk] /= 10;}
	print "$row";
	if (1 != $castro_i_dati) {
	    print ",$pa[0],$pa[1],$pa[3],$pa[4],$pa[5],$pa[7],$en[0],$en[1],$en[3],$en[4],$en[5],$en[7]";
	}
	if ((1 == $castro_i_dati) || (2 == $castro_i_dati)) {
	    $a[0] = $en[4] / $pa[4]/1024.0/8.0;
	    $a[1] = $en[1] / $pa[1]/1024.0/8.0;
	    $a[2] = $en[3] / $pa[3]/1024.0/8.0;
	    print ",$a[0],$a[1],$a[2]";
	}
	print "\n";
	for ($kk=0;$kk<8;$kk++) {$pa[$kk] =0;$en[$kk] =0;}
    }
}

