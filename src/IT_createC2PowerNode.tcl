################################################################################
# Create a C2POWER Node
################################################################################
# +------------------------------------------------------------------------+
# |                              4. PORT & APP                             |
# +------------------------------------------------------------------------+
# |                              4. C2POWER                                |
# +------------------------------------------------------------------------+
# |                3. IP             | |              3. IP                |
# +-----------------+----------------+ +----------------+------------------+
# |               2. MAC             | |              2. MAC               | 
# +-----------------+----------------+ +----------------+------------------+
# |           1. Module/Phy          | |          1. Module/Phy            |
# +-----------------+----------------+ +-----------------------------------+
#                   |           	                 |           
# +----------------------------------+ +-----------------------------------+ 
# |          DumbWirelessChannel     | |          DumbWirelessChannel      |
# +----------------------------------+ +-----------------------------------+ 


proc createC2PowerNode {adapter id ipadSh netmaskSh ipadLong netmaskLong longDatarate shortDatarate defaultGw posX posY} {

  global ns PHYDataRate per maskShort channelShort channelLong propagationShort propagationLong Position
  global C2Power maskLong ap_macaddr
  global Cbr IpIFLong IpIFShort MACLong MACShort PhyLong PhyShort 
  global peerstats mode moderng
  global NrgModel
  global Ipr Port
  global param nrgModelParam

puts "# CREATE C2POWER NODE No. $id ###############"
puts "#"

  set Node($id)  [$ns create-M_Node]
  set Cbr($id) [new Module/CBR]

  set Port($id) [new Module/Port/Map]
  set Ipr($id) [new Module/IP/Routing]
  set C2Power($id) [new Module/C2Power]
  
  set IpIFShort($id) [new Module/IP/Interface]
  set IpIFLong($id) [new Module/IP/Interface]
 

  $IpIFShort($id) addr $ipadSh
  $IpIFShort($id) subnet $netmaskSh

  $IpIFLong($id) addr $ipadLong
  $IpIFLong($id) subnet $netmaskLong

  $Ipr($id) defaultGateway $defaultGw

  ############ C2POWER DEVICE ###########################################################
  set MACShort($id) [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail/C2PowerQueue" [$IpIFShort($id) addr] "" 5000 ]
  #set MACShort($id) [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail" [$IpIFShort($id) addr] "" 5000 ]
  set PhyShort($id) [new Module/MPhy/80211Phy]
  set interf [new MInterference/MIV]

  $PhyShort($id) set TxPower_ $param(power_tx_short)

  $Cbr($id) set packetSize_ $param(cbrPacketSize)
  $Cbr($id) set period_ $param(PERIOD)
    
  set Position($id) [new "Position/BM"]

  $Node($id)  addPosition $Position($id)
  $Position($id) setX_ $posX
  $Position($id) setY_ $posY

  #$propagationShort newNode $Position($id) ;# it is for MPropagation/FullPropagation
  $Node($id)  addModule 7 $Cbr($id)       0 "Cbr_$id  "
  $Node($id)  addModule 6 $Port($id)      0 "Port_$id "
  $Node($id)  addModule 5 $Ipr($id)       0 "Ipr_$id  "
  $Node($id)  addModule 4 $C2Power($id)   0 "C2POWER_$id"
  $Node($id)  addModule 3 $IpIFShort($id) 0 "IpIFSh_$id "  
  $Node($id)  addModule 2 $MACShort($id)  0 "MACSh_$id  "
  $Node($id)  addModule 1 $PhyShort($id)  0 "PhySh_$id  "

  $MACShort($id) dataMode_ $shortDatarate
  $MACShort($id) basicMode_ Mode54Mb ;#Mode6Mb
  $MACShort($id) usePhy $PhyShort($id)

  $PhyShort($id) useMAC $MACShort($id)
  $PhyShort($id) setPer $per
  $PhyShort($id) setSpectralMask $maskShort
  $PhyShort($id) setPropagation $propagationShort
  $PhyShort($id) setInterference $interf

  #$PhyShort($id) setEnergy 1000
  #$Phy($id) setConsumption 1.0 2.0 3.0 0.01
  #microsecond counter
  #$PhyShort($id) setConsumption 1.0 1.0 1.0 1.0  
  #$PhyShort($id) setEnergy $nrgModelParam(TotalEnergy)
  #$PhyShort($id) setConsumption $ShortIdlePwr $ShortTxPwr $ShortRxPwr $ShortSleepPwr 


  $Node($id)  setConnection $C2Power($id)   $IpIFShort($id) $param(C2powerTrace)
  $Node($id)  setConnection $IpIFShort($id) $MACShort($id)  $param(C2powerTrace)
  $Node($id)  setConnection $MACShort($id)  $PhyShort($id)  $param(C2powerTrace)

  $Node($id)  addToChannel  $channelShort   $PhyShort($id)  $param(C2powerTrace)




  # LONG RANGE DEVICE #################################################################

  set MACLong($id) [createModuleMac80211Wrap "LL/Mrcl" "Queue/DropTail/C2PowerQueue" [$IpIFLong($id) addr] "" 200 ]
  set PhyLong($id) [new Module/MPhy/80211Phy]
  set interfLong [new MInterference/MIV]

  $PhyLong($id) set TxPower_ $param(power_tx_long)

  #$propagationLong newNode $Position($id) ;# it is for MPropagation/FullPropagation
  
  #$MACLong($id) dataMode_ Mode18Mb
  $MACLong($id) dataMode_  $longDatarate ;#$mode([$moderng integer 9])
  $MACLong($id) basicMode_ Mode6Mb   ;# equal to the basicMode of AP!
  $MACLong($id) usePhy $PhyLong($id)

  #$MACLong($id) bss_id "$ap_macaddr"

  #set C2PRateAdapter($id) [new RateAdapter/$adapter]
  #$C2PRateAdapter($id) attach2mac [$MACLong($id) getMac]
  #$C2PRateAdapter($id) use80211g
  #$C2PRateAdapter($id) setmodeatindex 0

  $MACLong($id) PeerStatsDB $peerstats

  $PhyLong($id) useMAC $MACLong($id)
  $PhyLong($id) setPer $per
  $PhyLong($id) setSpectralMask $maskLong
  $PhyLong($id) setPropagation $propagationLong
  $PhyLong($id) setInterference $interfLong

  $Node($id)  addModule 3 $IpIFLong($id) 0 "IpIFL_$id  "  
  $Node($id)  addModule 2 $MACLong($id)  0 "MACLong_$id"
  $Node($id)  addModule 1 $PhyLong($id)  0 "PhyLong_$id"

  #$PhyLong($id) setEnergy 1000
  #$Phy($id) setConsumption 1.0 2.0 3.0 0.01
  #microsecond counter
  #$PhyLong($id) setConsumption 1.0 1.0 1.0 1.0  
  #$PhyLong($id) setEnergy $nrgModelParam(TotalEnergy)
  #$PhyLong($id) setConsumption $nrgModelParam(LongIdlePwr) $nrgModelParam(LongTxPwr) $nrgModelParam(LongRxPwr) $nrgModelParam(LongSleepPwr)

  set NrgModel($id) [new Plugin/C2PEnergyModel]
  $Node($id) addPlugin $NrgModel($id) 0 "NRG_$id"

  $NrgModel($id) setEnergy $nrgModelParam(TotalEnergy)

  $PhyLong($id) setEnergyModel
  $PhyLong($id) setConsumption  $nrgModelParam(LongIdlePwr) $nrgModelParam(LongTxPwr) $nrgModelParam(LongRxPwr) $nrgModelParam(LongSleepPwr) 

  $PhyShort($id) setEnergyModel
  $PhyShort($id) setConsumption $nrgModelParam(ShortIdlePwr) $nrgModelParam(ShortTxPwr) $nrgModelParam(ShortRxPwr) $nrgModelParam(ShortSleepPwr) 

  $Node($id)  setConnection $Cbr($id)      $Port($id)     1
  $Node($id)  setConnection $Port($id)     $Ipr($id)      0
  $Node($id)  setConnection $Ipr($id)      $C2Power($id)  0
  $Node($id)  setConnection $C2Power($id)  $IpIFLong($id) $param(C2powerTrace)
  $Node($id)  setConnection $IpIFLong($id) $MACLong($id)  $param(C2powerTrace)
  $Node($id)  setConnection $MACLong($id)  $PhyLong($id)  0
  $Node($id)  addToChannel  $channelLong   $PhyLong($id)  0

  $Port($id)  assignPort    $Cbr($id)



#  $C2Power($id) routing
  $C2Power($id) discovery

  $C2Power($id) anyrange

  $C2Power($id) nodsel_MT

  #$C2Power($id) setC2PowerDevice [$MACShort($id) Id_]
  #$C2Power($id) setLongRangeDevice [$MACLong($id) Id_]

  $C2Power($id) setC2PowerDevice [$IpIFShort($id) Id_]
  $C2Power($id) setLongRangeDevice [$IpIFLong($id) Id_]

  $C2Power($id) setShortRangeDeviceAddr [$IpIFShort($id) addr]  
  $C2Power($id) setShortRangeDeviceSubnet [$IpIFShort($id) subnet]

  $C2Power($id) setLongRangeDeviceAddr [$IpIFLong($id) addr]
  $C2Power($id) setLongRangeDeviceSubnet [$IpIFLong($id) subnet]

  $C2Power($id) setC2PowerDatarate Mode54Mb


# ALBANO commented here
#  $C2Power($id) set_rout_param  broadcast        $::rout_param(broadcast)   ;# use broadcast for routing cmd pkts (0 false or 1 true)
#  $C2Power($id) set_rout_param  alpha            $::rout_param(alpha) 
#  $C2Power($id) set_rout_param  hoplimit         $::rout_param(hoplimit)
#  $C2Power($id) set_rout_param  reBroadcastLimit $::rout_param(reBroadcastLimit)
#  $C2Power($id) set_rout_param  rreqtimeout      $::rout_param(rreqtimeout)
#  $C2Power($id) set_rout_param  routeValidity    $::rout_param(routeValidity) 
#  $C2Power($id) set_rout_param  turnLongOnOff    $::rout_param(turnLongOnOff)
#  $C2Power($id) set_rout_param  turnOFFperiod    $::rout_param(turnOFFperiod)
#  $C2Power($id) set_rout_param  turnONperiod     $::rout_param(turnONperiod)


  puts "# C2PowerNode created at ([$Position($id) getX_],[$Position($id) getY_])"
  puts "##########################################################"

  return $Node($id) 
}

