
#ifndef _c2power_routing_ROUTETABLE_H_
#define _c2power_routing_ROUTETABLE_H_


#include <vector>
#include <map>
#include <string>
#include <stdio.h>
#include <sstream>
#include <limits>
#include <iostream>

/******************************************************************************************/
/******************************************************************************************/
/******************************************************************************************/

// This contain the address of c2power layer and the used underlying mac addresses
//  - In and out MAC are used in order to allow switching of technology
class c2p_route_entry {
public:
	c2p_route_entry() : c2p_addr(-1),ipif_addr_in(-1),ipif_addr_out(-1),lifetime(-1),energyCost(-1) {};

	int c2p_addr;
	int ipif_addr_in;
	int ipif_addr_out;

	// Energy-related measures
	double lifetime;
	double energyCost;
};

class Route
{
public:
	Route();
	Route( std::vector<c2p_route_entry> r );
	Route( std::vector<c2p_route_entry> r , double c);
	~Route();

	std::vector<c2p_route_entry> addrs;

	double cost;
	double time_;
	double time_last_use;

	void addEntry(c2p_route_entry entry);
	int getLength();
	int getSrcIP();
	int getDestIP();

	Route* reverse();

	std::string print();
	std::string printShort();
};

//typedef vector<c2p_route_entry>::iterator RouteIt;

/******************************************************************************************/
/******************************************************************************************/
/******************************************************************************************/

//typedef std::map<int, RouteRecord*> RoutingTable;
//typedef RouteTable::iterator RTit;

class RouteTable
{
	public:
		RouteTable();
		virtual ~RouteTable();

	private:

		std::vector<Route*> routingCache;

	public:

		int getNumRoutes() { return routingCache.size(); };
		Route* getEntry(int idx) { return routingCache.at(idx); };

		/** Return the best route match dstIP */
		Route* getRoute(int dstIP);

		/** Add a Route */
		void addRoute( Route* r, double now );

		/** Substitute routes with the new one */
		void updateRoute( Route* r, double now );

		/** Update last use of a matching route */
		void updateRouteUsage( c2p_route_entry addrs[], int length, double now);

		/** Check if exist at least one route to reach dstNode*/
		bool existRoute(int dstIPaddr);

		/** Return a vector with routes having a link between n1-n2*/
		std::vector<Route*> getlist(int n1, int n2);

		/** Remove a routes that contains a link between n1-n2*/
		bool remRoutes1(int n1, int n2);

		/** Remove routes matching node as the second node (node after the source) */
		bool remRoutes2(int node);

		/** Remove routes matching Src and Dst nodes */
		bool remSrcDst(int src, int dst);

		/** Checks if the table is empty */
		bool empty();

		/** Removes routes older than "validity" */
		bool Refresh(double validity, double now);

		/** If there is 2 routes with same [src-dst] keep the last used */
		bool RefreshLastUsage(double now);

		// Utility functions:
		std::string print();

};

#endif /* _c2power_routing_ROUTETABLE_H_ */
