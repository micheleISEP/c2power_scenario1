

#include <scheduler.h>
#include <mac.h>
//#include "c2power.h"

#ifndef __nsc2power_timers__
#define __nsc2power_timers__

/* ======================================================================
   Timers
   ====================================================================== */
//class Mac802_11mr;
//class C2PowerLayer;
class ClusterManager;
//class MockClusterManager;

//typedef int(C2PowerLayer::*firefunction)(void);
//
typedef int(ClusterManager::*firefunction)(void);
//typedef int(MockClusterManager::*firefunctionManager)(void);

class C2PowerTimer : public TimerHandler {

public:
	C2PowerTimer(ClusterManager *m, firefunction fx) : TimerHandler(), m_(m), fire(fx), expire_(0.0), busy_(false) {}
	//C2PowerTimer(MockClusterManager *m, firefunctionManager fx) : TimerHandler(), m_(m), fire(fx), expire_(0.0), busy_(false) {}

        inline void resched(double delay) {
                expire_ = Scheduler::instance().clock() + delay;
                TimerHandler::resched(delay);
                busy_ = true;
        }

        /*
         * Query when this timer would expire
         */
        inline double expiretime() { return expire_;}

        /*
         * Query whether this timer is working
         */
        inline bool busy(){
                if(busy_ && expire_>= Scheduler::instance().clock())
                        return true;
                else return false;
        }

        /*
         * To stop the timer's working
         */
        inline void stop() {
                if(status_ == TIMER_PENDING)
                        cancel();
                expire_ = Scheduler::instance().clock();                                        // the stopping time
                busy_ = false;
        }

        inline void cancel(){
                busy_ = false;
                expire_ = Scheduler::instance().clock();
                TimerHandler::cancel();
        }

protected:
        virtual void expire(Event *);
        double expire_;
        bool busy_;
        //C2PowerLayer *m_;
        ClusterManager* m_;
        firefunction fire; // Using Dynamic Function definition for timers
};


/**************************************************************************
 * Riccardo: Timers for the c2power routing
 **************************************************************************/
class MockRoutingManager;

/**
 * RREQTimeoutTimer is used to wait for RREQs at the destination.
 */
class RREQTimeoutTimer : public TimerHandler
{

public:
//	RREQTimeoutTimer(MockRoutingManager *m) : TimerHandler() { module = m; }
	RREQTimeoutTimer(MockRoutingManager *m,int s,int id) : TimerHandler() { module = m; src=s; rreqId=id;}

	int src;
	int rreqId;

protected:
  virtual void expire(Event *e);
  MockRoutingManager* module;
};

/**
 * RREQFailed is used to trigger a failed RREQ process event.
 */
class RREQFailed : public TimerHandler
{

public:
	RREQFailed(MockRoutingManager *m,int d,int id) : TimerHandler() { module = m; dstIP=d; rreqId=id;}

	int dstIP;
	int rreqId;

protected:
  virtual void expire(Event *e);
  MockRoutingManager* module;
};

/**
 * RouteTableTimer define the timer to refresh the route table
 */
class RREQTableTimer : public TimerHandler
{

public:
	RREQTableTimer(MockRoutingManager *m) : TimerHandler() { module = m;}

protected:
  virtual void expire(Event *e);
  MockRoutingManager* module;
};


/**
 * RouteTableTimer define the timer to refresh the route table
 */
class RouteTableTimer : public TimerHandler
{

public:
	RouteTableTimer(MockRoutingManager *m) : TimerHandler() { module = m;}

protected:
  virtual void expire(Event *e);
  MockRoutingManager* module;
};


/**
 * Schedule ON/OFF switch periods for the long range
 */
class LongONOFFTimer : public TimerHandler
{

public:
	LongONOFFTimer(MockRoutingManager *m) : TimerHandler() { module = m;}

protected:
  virtual void expire(Event *e);
  MockRoutingManager* module;
};


#endif /* __c2power_timers__ */
