
#include "c2power_routing_RouteTable.h"
#include <stdlib.h>

/*******************************************************************************************/
/*******************************************************************************************/
/*******************************************************************************************/

Route::Route() {
	cost = -1;
}
Route::Route( std::vector<c2p_route_entry> r) {
	addrs = r;
	cost = -1;
}
Route::Route( std::vector<c2p_route_entry> r, double c) {
	addrs = r;
	cost = c;
}
Route::~Route() {
	addrs.clear();
}

int Route::getLength() {
	return addrs.size();
}

int Route::getSrcIP() {
	int length = getLength();
	if (!length>0) return -1;

	return addrs.at(0).ipif_addr_out;
}

int Route::getDestIP() {
	int length = getLength();
	if (!length>0) return -1;

	return addrs.at(length-1).ipif_addr_in;
}

void Route::addEntry(c2p_route_entry entry) {
	addrs.push_back(entry);
	return;
}

Route* Route::reverse() {
	Route* rev_r = new Route();
	for ( int i=getLength()-1; i>=0; i-- ) {
		c2p_route_entry e = addrs.at(i);
		std::swap(e.ipif_addr_in,e.ipif_addr_out);
		rev_r->addrs.push_back(e);
	}
	rev_r->cost=cost;
	return rev_r;
}

std::string Route::print() {
	std::stringstream out;

	for ( int i=0 ; i<addrs.size(); i++ ) {
		out << "[";
		out << addrs.at(i).c2p_addr << " | ";
		out << ((addrs.at(i).ipif_addr_in & 0xff000000)>>24)<<"."<<((addrs.at(i).ipif_addr_in & 0x00ff0000)>>16)<<"."<<((addrs.at(i).ipif_addr_in & 0x0000ff00)>>8)<<"."<<((addrs.at(i).ipif_addr_in & 0x000000ff))<<" ";
		out << ((addrs.at(i).ipif_addr_out & 0xff000000)>>24)<<"."<<((addrs.at(i).ipif_addr_out & 0x00ff0000)>>16)<<"."<<((addrs.at(i).ipif_addr_out & 0x0000ff00)>>8)<<"."<<((addrs.at(i).ipif_addr_out & 0x000000ff))<<" ";
		out << addrs.at(i).energyCost	<< " ";
		out << addrs.at(i).lifetime		<< " ] ";
	}
	out << " Cost=" << cost << " ";
	return out.str();
}
std::string Route::printShort() {
	std::stringstream out;
	out << "[";
	for ( int i=0 ; i<addrs.size(); i++ ) {
		out << addrs.at(i).c2p_addr << " ";
	}
	out << "]";
	return out.str();
}

/*******************************************************************************************/
/*******************************************************************************************/
/*******************************************************************************************/
RouteTable::RouteTable() {}
RouteTable::~RouteTable()
{
	routingCache.clear();
}


/* Return the best route that match dstIP */
Route* RouteTable::getRoute(int dstIP) {

	double cost = std::numeric_limits<double>::max();
	Route* foundroute;
	for (int i=0; i<routingCache.size(); i++) {
		Route* r = routingCache[i];
		if ( dstIP == r->addrs[r->getLength()-1].ipif_addr_in ) {
			if ( r->cost <= cost ) {
				foundroute = r;
				cost = r->cost;
			}
		}
	}
//	printf("\tRouteTable::getRoute(int dstIP) found route:\n");
//	printf("\t%s\n", foundroute->print().c_str() );

	return foundroute;
}

/* Add a Route  */
void RouteTable::addRoute( Route* route , double now ) {
	route->time_ = now;
	route->time_last_use = now;
	routingCache.push_back(route);
}

/* Update a Route  */
void RouteTable::updateRoute( Route* route , double now ) {
	int dstIPaddr = route->getDestIP();
	int srcIPaddr = route->getSrcIP();

	for (int i=0; i<routingCache.size(); i++) {
		Route* r = routingCache[i];
		if ( dstIPaddr == r->getDestIP() && srcIPaddr == r->getSrcIP() ) {
			routingCache.erase(routingCache.begin()+i);
			i--;
		}
	}

	route->time_ = now;
	route->time_last_use = now;
	routingCache.push_back(route);
}

/* Update a Route usage time */
void RouteTable::updateRouteUsage( c2p_route_entry addrs[], int length, double now) {

//	printf("\n---------------------------------------------------\n");
//	printf("pkt route: ");
//	for (int i=0; i<length; i++)
//		printf("%d %d %d |", addrs[i].c2p_addr, addrs[i].ipif_addr_in, addrs[i].ipif_addr_out);
//	printf("\n---\n");

	for (int i=0; i<routingCache.size(); i++) {
		Route* r = routingCache[i];
		if ( length != r->getLength() ) continue;

//		printf("stored route:");
//		for (int i=0; i<length; i++)
//			printf("%d %d %d |", r->addrs.at(i).c2p_addr,r->addrs.at(i).ipif_addr_in, r->addrs.at(i).ipif_addr_out);
//		printf("\n");

		bool equal = true;
		for (int i=0; i<length; i++) {
			if ( 	addrs[i].c2p_addr != r->addrs.at(i).c2p_addr ||
					addrs[i].ipif_addr_in != r->addrs.at(i).ipif_addr_in ||
					addrs[i].ipif_addr_out != r->addrs.at(i).ipif_addr_out ) {
				equal = false;
				break;
			}
		}

		if (equal) {
			r->time_last_use = now;
//			printf("equal.\n");
		}
//		else {
//			printf("not equal.\n");
//		}
	}

//	printf("---------------------------------------------------\n\n");
}

/* Check if exist at least one route */
bool RouteTable::existRoute(int dstIPaddr ) {
	if ( routingCache.empty() )
		return false;

	for (int i=0; i<routingCache.size(); i++) {
		Route* r = routingCache[i];
		if ( dstIPaddr == r->addrs[r->getLength()-1].ipif_addr_in ) {
			return true;
		}
	}

	return false;
}

/* Return a vector with routes having a link between n1-n2*/
std::vector<Route*> RouteTable::getlist(int n1, int n2) {
	std::vector<Route*> vect;
	for (int i=0; i<routingCache.size(); i++){
		Route* r = routingCache[i];
		for (int j=0; j<r->addrs.size()-2; j++ ) {
			if ( r->addrs[j].c2p_addr == n1 && r->addrs[j+1].c2p_addr == n2 ) {
				vect.push_back(r);
			}
		}
	}
	return vect;
}

/* Remove a routes that contains a link between n1-n2 */
bool RouteTable::remRoutes1(int n1, int n2) {

	bool deleted = false;

	if ( routingCache.size() == 0 ) return false;

	for (int i=0; i<routingCache.size(); i++ ) {
		int ll = routingCache[i]->getLength()-1;
		for (int k=0; k<ll; k++) {

			if ( routingCache[i]->addrs.at(k).c2p_addr == n1 &&
					routingCache[i]->addrs.at(k+1).c2p_addr == n2) {

				routingCache.erase(routingCache.begin()+i);
				i--;
				deleted = true;
				break;
			}
		}
	}

	return deleted;
}

/* Remove routes having node as first node (node after the source) */
bool RouteTable::remRoutes2(int node) {

	bool deleted = false;

	for (int i=0; i<routingCache.size(); i++ ) {

		if ( routingCache[i]->addrs.at(1).c2p_addr == node ) {

			routingCache.erase(routingCache.begin()+i);
			i--;
			deleted = true;
//			break;
		}

	}

	return deleted;
}

/* Remove routes matching Src and Dst nodes */
bool RouteTable::remSrcDst(int src, int dst) {

	Route* r;
	int length;

	bool deleted = false;

	for (int i=0; i<routingCache.size(); i++ ) {
		r = routingCache[i];
		length = r->getLength();

		if ( r->addrs.at(0).c2p_addr == src &&
				r->addrs.at( length-1 ).c2p_addr == dst) {
			routingCache.erase(routingCache.begin()+i);
			i--;
			deleted = true;
//			break;
		}

	}

	return deleted;
}

bool RouteTable::empty() {
	if ( routingCache.empty() )
		return true;
	else
		return false;
}

bool RouteTable::Refresh(double validity, double now) {

	bool deleted = false;

	for ( int i=0; i<routingCache.size(); i++ ) {

		if ( now - routingCache.at(i)->time_ > validity ) {

			routingCache.erase(routingCache.begin()+i);
			i--;
			deleted = true;
		}
	}

	return deleted;
}

bool RouteTable::RefreshLastUsage(double now) {
	bool deleted = false;

	std::map< std::pair<int,int> ,double> lastusage;
	std::map< std::pair<int,int> ,double>::iterator it;

//	printf("\n--------------------------------------------\n");
//	printf("%s",print().c_str());

	// For each Src-Dst find the route with the last usage time
	for ( int i=0; i<routingCache.size(); i++ ) {
		Route* r = routingCache.at(i);
		std::pair<int,int> p = std::pair<int,int>( r->getSrcIP() , r->getDestIP() );

		it = lastusage.find(p);
		if ( it != lastusage.end() ) {
			if ( lastusage.find(p)->second < r->time_last_use )
				lastusage.find(p)->second = r->time_last_use;
//			printf("%f\n",r->time_last_use);
		}
		else {
			lastusage.insert( std::pair< std::pair<int,int> ,double>(p,r->time_last_use) );
//			printf("%f\n",r->time_last_use);
		}
	}

	// For a Src-Dst keep only the last used route
	for ( int i=0; i<routingCache.size(); i++ ) {
		Route* r = routingCache.at(i);
		std::pair<int,int> p = std::pair<int,int>( r->getSrcIP() , r->getDestIP() );
		it = lastusage.find(p);

		if ( r->time_last_use < (*it).second ) {
			routingCache.erase(routingCache.begin()+i);
			i--;
			deleted = true;
		}
	}

//	printf("%s",print().c_str());
//	printf("--------------------------------------------\n");

	return deleted;
}

std::string RouteTable::print() {

	std::stringstream out;
	out << "-------------------------------------\n";
	out << "ROUTE TABLE:";
	if ( routingCache.empty() ) {
		out << "\t **empty**\n";
		out << "-------------------------------------\n";
		return out.str();
	}

	out << "\n";
	for ( int i=0; i<routingCache.size(); i++ ) {
		out << "\t[" << routingCache[i]->addrs[0].c2p_addr << "] - ";

		out << routingCache[i]->print();
		out << "\n";
	}
	out << "-------------------------------------\n";
	return out.str();
}
