
#ifndef C2POWER_GLPK_H
#define C2POWER_GLPK_H

#include <stdlib.h>
#include "c2power_structures.h"
#include <glpk.h>

class C2PowerGlpk {
 private:
  // measurements in 10^-9 J/bit
  double RandCostWifi() {
    /* we use a maximum cost of 111 * 10^-9 J/bit
       we use a minimum cost of 4 * 10^-9 J/bit
    */
    return rand()*(111-4.0)/RAND_MAX + 4.0;
  }

  double RandCostUwb() {
    /* we use an energy cost of 560 * 10^-6 W
       we use a maximum cost of 560 /53.3 10*-12 W =~ 10 * 10^-12
       we use a minimum cost of 560 /480 10*-12 W =~ 1.2 * 10^-12
    */
    return (rand()*(10-1.2)/RAND_MAX + 1.2)*2.0/1000.0;
  }



 public:
  C2PowerGlpk() {
    cost_Lr = cost_Ls = cost_Srs = NULL;
    names_r = names_s = NULL;
    without_cooperation = 0;
  }

  ~C2PowerGlpk() {
    Reset();
  }

  double without_cooperation;
  double saving;

  double* cost_Lr;
  double* cost_Ls;
  double* cost_Srs;
  char** names_r;
  char** names_s;

  bs_relay_source** Solve(int printdata);
  void Solve_c2power();
  void PrintData(glp_prob* lp);
  int seed, relays, sources, green; 
  void SetRandomProblem(int m, int n);
  void SetRandomProblem(int m, int n, char g);
  void SetRandomProblem(int m, int n, char g, int s);
  void SetMN(int m, int n);
  void Reset();
  void SetLongRelay(int i, double c);
  void SetLongRelay(int i, double c, char* name);
  void SetLongSource(int i, double c);
  void SetLongSource(int i, double c, char* name);
  void SetShortRS(int i, int j, double c);
  int CheckMatrix();
};





#endif // C2POWER_GLPK_H

