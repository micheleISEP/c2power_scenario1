#include "c2power_neighbour_list.h"
#include <stdio.h>


//C2Power_InterfaceInfo::C2Power_InterfaceInfo()
//{
//}

//C2Power_InterfaceInfo::~C2Power_InterfaceInfo()
//{
//}

//C2Power_InterfaceInfo::C2Power_InterfaceInfo(int, double, double)
//{
//}

C2Power_NeighbourList::C2Power_NeighbourList()
{
}

C2Power_NeighbourList::~C2Power_NeighbourList()
{
	m_EntryContainer.clear();
}

bool C2Power_NeighbourList::addEntry(C2Power_NeighbourListEntry e)
{
	int node_index = getEntryByID(e.GetAddr());

	//check if the node_id already exists
	if(node_index!=-1)
	{
		//printf("  update node %d, time: %f (last %f), SNR = %f (last %f), SINR = %f (last %f)\n",e.GetAddr(),e.GetTimeStamp(),m_EntryContainer[node_index].GetTimeStamp(),
		//		e.GetSNR(),m_EntryContainer[node_index].GetSNR(),e.GetSINR(),m_EntryContainer[node_index].GetSINR());

		m_EntryContainer[node_index].SetTimeStamp(e.GetTimeStamp());
		m_EntryContainer[node_index].SetSNR(e.GetSNR());
		m_EntryContainer[node_index].SetSINR(e.GetSINR());

		m_EntryContainer[node_index].SetLongSNR(e.GetLongSNR());
		m_EntryContainer[node_index].SetLongBSSID(e.GetLongBSSID());
		m_EntryContainer[node_index].SetLongDataRate(e.GetLongDataRate());
		m_EntryContainer[node_index].SetShortAddr(e.GetShortAddr());
		m_EntryContainer[node_index].SetLongAddr(e.GetLongAddr());
		return true;
	}

	//printf("  add node %d at time %f \n",e.GetAddr(),e.GetTimeStamp());
	m_EntryContainer.push_back(e);
	return true;
}

int C2Power_NeighbourList::getListSize()
{
	return m_EntryContainer.size();
}

bool C2Power_NeighbourList::clearList()
{
	m_EntryContainer.clear();
	return true;
}

bool C2Power_NeighbourList::removeEntry(int idx)
{

	if (idx >= getListSize() || idx < 0)
	{
		return false;
	}

	m_EntryContainer.erase (m_EntryContainer.begin()+idx);

	return true;
}


C2Power_NeighbourListEntry& C2Power_NeighbourList::getEntry(int idx)
{
	if (idx >= getListSize() || idx < 0)
	{
		//error
	}

	return m_EntryContainer.at(idx);
}

C2Power_NeighbourListEntry& C2Power_NeighbourList::operator[](int idx)
{
	return getEntry(idx);
}

int C2Power_NeighbourList::getEntryByID(int node_id)
{
	int idx = -1;
	int size = getListSize();

	for(int i=0;i<size;i++)
	{
		if(m_EntryContainer[i].GetAddr()==node_id)
		{
			idx = i;
			break;
		}
	}

  return idx;

}


C2Power_NeighbourListEntry C2Power_NeighbourList::getBestNeighbour()
{
	int bestMode =0;
	double bestLongSNR = 0;
	double bestShortSNR = 0;
	int idx =-1;
	int size = getListSize();
	if(size<=0){
		//ERROR
	}
	bestMode = m_EntryContainer[0].GetLongDataRate();
	bestLongSNR = m_EntryContainer[0].GetLongSNR();
	bestShortSNR = m_EntryContainer[0].GetSNR();
	idx = 0;
	for(int i=0;i<size;i++)
	{
		if(m_EntryContainer[i].GetLongDataRate()>=bestMode)
		{
			if(m_EntryContainer[i].GetLongSNR()>=bestLongSNR)
			{
				idx = i;

				bestMode = m_EntryContainer[idx].GetLongDataRate();
				bestLongSNR = m_EntryContainer[idx].GetLongSNR();
			}
		}
	}


	return m_EntryContainer[idx];
}

void C2Power_NeighbourListEntry::SetAddr(int a) {addr_=a;}
void C2Power_NeighbourListEntry::SetTimeStamp(double tm){time_stamp_=tm;}
void C2Power_NeighbourListEntry::SetSNR(double snr){snr_=snr;}
void C2Power_NeighbourListEntry::SetSINR(double sinr){sinr_=sinr;}

//void C2Power_NeighbourListEntry::SetInterfaceNum(int num){interfaceNum_=num;}

int C2Power_NeighbourListEntry::GetAddr() {return addr_;}
double C2Power_NeighbourListEntry::GetTimeStamp(){return time_stamp_;}
double C2Power_NeighbourListEntry::GetSNR(){return snr_;}
double C2Power_NeighbourListEntry::GetSINR(){return sinr_;}

void C2Power_NeighbourListEntry::SetLongRangeDevice(bool cond){longRangeDeviceAvailable = cond;}
bool C2Power_NeighbourListEntry::IsLongRangeDevice(){return longRangeDeviceAvailable;}

void C2Power_NeighbourListEntry::SetLongSNR(double snr){long_snr_=snr;}
void C2Power_NeighbourListEntry::SetLongSINR(double sinr){long_sinr_=sinr;}
double C2Power_NeighbourListEntry::GetLongSNR(){return long_snr_;}
double C2Power_NeighbourListEntry::GetLongSINR(){return long_sinr_;}
//int C2Power_NeighbourListEntry::GetInterfaceNum() {return interfaceNum_;}

void C2Power_NeighbourListEntry::SetLongBSSID(int id){long_bssid_=id;}
int C2Power_NeighbourListEntry::GetLongBSSID(){return long_bssid_;}

void C2Power_NeighbourListEntry::SetLongDataRate(int val){long_data_rate_=val;}
int C2Power_NeighbourListEntry::GetLongDataRate(){return long_data_rate_;}

void C2Power_NeighbourListEntry::SetLongAddr(int val){longAddr_=val;}
int C2Power_NeighbourListEntry::GetLongAddr(){return longAddr_;}

void C2Power_NeighbourListEntry::SetShortAddr(int val){shortAddr_=val;}
int C2Power_NeighbourListEntry::GetShortAddr(){return shortAddr_;}

C2Power_NeighbourListEntry::C2Power_NeighbourListEntry():addr_(-1),snr_(-1),time_stamp_(-1){}
C2Power_NeighbourListEntry::C2Power_NeighbourListEntry(int a):addr_(a),snr_(-1),time_stamp_(-1){}
C2Power_NeighbourListEntry::C2Power_NeighbourListEntry(int a, double snr):addr_(a),snr_(snr),time_stamp_(-1){}
C2Power_NeighbourListEntry::C2Power_NeighbourListEntry(int a, double snr, double tm):addr_(a),snr_(snr),time_stamp_(tm){}
