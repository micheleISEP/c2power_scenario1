#!/usr/bin/perl
$my_ns2 = "/home/albano/ns-allinone-2.34/ns-2.34-miracle/ns";
$green = 1;

sub diuc {
#data taken from
# Shyam Parekh et al.: "WiMAX Forum System Level Simulator NS-2 MAC+PHY Add-On for WiMAX (IEEE 802.16)", WiMAX Forum, Release 2.6, March 20th 2009.
    my $dist = shift(@_);
    my $diuc = -1;
    if ($dist >=0 && $dist < 1200) {$diuc = 7;}#64QAM_3_4
    if ($dist >=1200 && $dist < 1500) {$diuc = 6;}#64QAM_2_3
    if ($dist >=1500 && $dist < 2100) {$diuc = 5;}#16QAM_3_4
    if ($dist >=2100 && $dist < 2700) {$diuc = 4;}#16QAM_1_2
    if ($dist >=2700 && $dist < 3100) {$diuc = 3;}#QPSK_3_3
    if ($dist >=3100 && $dist < 4000) {$diuc = 2;}#QPSK_1_2
    if ($dist >=4000 && $dist < 5000) {$diuc = 1;}#BPSK_1_2
    print "selected $diuc\n";
    return $diuc;
}

if ($#ARGV < 0) {
    die "tell me how many nodes [no default], how many scripts [def = 1], and the seed [def = random]\n";
}

$nnodes = $ARGV[0];
$nscripts = 1;
$seed = int(rand(999999999));
if ($#ARGV >= 1) {
    $nscripts = $ARGV[1];
}
if ($#ARGV >= 2) {
    $seed = $ARGV[2];
    if ($seed <= 0) {$seed = int(rand(999999999));}
}

open CFG, ">analysis.cfg";
open SCRIPT, ">doit.sh";

for ($nn = 0 ; $nn < $nscripts ; $nn ++) {

    for ($c2p = 0; $c2p<2; $c2p++) {
#restore seed of non-c2p for c2p simulation, else refresh the seed
    if ($c2p == 0) {$seed = int(rand(999999999));}
    srand $seed;
open FIN, "<wifi_wimax_ITtemplate.tcl";
open FOUT, ">sim$nn.$c2p.tcl";
print SCRIPT "$my_ns2 sim$nn.$c2p.tcl\n";
$state = 1;

while ($riga = <FIN>) {
    if (!($riga =~ /ALBANO/)) {
	if (1 == $state) {
	    print FOUT "$riga";
	}
    }
    if ($riga =~ /ALBANO1s/) {
	print CFG "sim.$nn.$c2p.tr $nnodes\n";

	print FOUT "set tf [open sim.$nn.$c2p.tr w]\n";
	print FOUT "\$ns trace-all \$tf\n";

	print FOUT "\nset N_WIFI_NODES $nnodes\n\n";
	for ($i=1;$i<=$nnodes;$i++) {
	    $x[$i] = 100*rand(100);$x[$i] = int($x[$i])/100;
	    $y[$i] = 100*rand(100);$y[$i] = int($y[$i])/100;
	}
	if ($green > 0) {
	    @y = sort {$a <=> $b} @y;
	}
	for ($i=1;$i<=$nnodes;$i++) {
	    print FOUT "set pos($i,x) $x[$i]\n";
	    print FOUT "set pos($i,y) $y[$i]\n";
	}
	$state = 2;
    }
    if ($riga =~ /ALBANO1e/) {
	$state = 1;
    }
    if ($riga =~ /ALBANO2s/) {
	for ($i=1;$i<=$nnodes;$i++) {
	    $diuc1[$i] = diuc(sqrt((1250-$y[$i])*(1250-$y[$i])+$x[$i]*$x[$i]));
	    print FOUT "attach_WIMAX_SStoWIFI $i $diuc1[$i]\n";
	}
	print FOUT "\n";
	$nnod2 = (1+$nnodes)/2;
	for ($i=1;$i<=$nnodes;$i++) {
	    $tmp1 = $i+int($nnod2);
	    if (($i <= $nnod2)&&($c2p > 0)&&(7==$diuc1[$tmp1])) {
		print "indirect $i $tmp1\n";
		print CFG "$i,";
		print FOUT "setup_indirect_routes $i $tmp1\n";
	    } else {
		print FOUT "setup_direct_routes $i\n";
	    }
	}
	$state = 2;
	print CFG "\n";
    }
    if ($riga =~ /ALBANO2e/) {
	$state = 1;
    }
}

close FOUT;
close FIN;



}
}
close CFG;
close SCRIPT;
