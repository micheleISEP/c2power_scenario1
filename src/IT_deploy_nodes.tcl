################################################################################
proc deploy_nodes { } {

	global param rvposition topology;

	set topology(pos_AP_x) [expr $param(area_x)/2]
	set topology(pos_AP_y) [expr $param(area_y)/2]

	for {set i 1} {$i <= $param(num_nodes)} {incr i} {
		set topology(pos_x_$i) [expr  [$rvposition value]]
		set topology(pos_y_$i) [expr  [$rvposition value]]
	}

log 1 "---------------------------------------------------------"
for {set i 1} {$i <= $param(num_nodes)} {incr i} {
log 1 "pos_x $i = $topology(pos_x_$i)"
log 1 "pos_y $i = $topology(pos_y_$i)"
}

}

################################################################################
proc choose_datarates { kind } {

global param

log 2 "------ choose_datarates ------"

	if { $kind == "long-range"} {
		foreach {a b} [array get ::long_range_perc] {
			set nodes_percentage($a) $b
		}
	}
	if { $kind == "short-range"} {
		foreach {a b} [array get ::short_range_perc] {
			set nodes_percentage($a) $b
		}
	}



	for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
		set num_nodes($i) 0
	}

if  { $kind == "long-range"} {  ;# update num_nodes($i) preferring lower datarates
	set sum 0
	for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
		set current_perc 0
		while { $current_perc < $nodes_percentage($i) } {
			incr num_nodes($i)
			incr sum
			set current_perc [expr ( 100*($num_nodes($i)+1)/$param(num_nodes) )]
			if {$sum == $param(num_nodes) } { break }
		}
		if {$sum == $param(num_nodes) } { break }
	}
}

if  { $kind == "short-range"} {  ;# update num_nodes($i) preferring lower datarates
	set sum 0
	for {set i [expr $param(num_dr_modes)-1]} {$i >= 0} {set i [expr $i-1]} {
		set current_perc 0
		while { $current_perc < $nodes_percentage($i) } {
			incr num_nodes($i)
			incr sum
			set current_perc [expr ( 100*($num_nodes($i)+1)/$param(num_nodes) )]
			if {$sum == $param(num_nodes) } { break }
		}
		if {$sum == $param(num_nodes) } { break }
	}
}

	for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
log 3 "Num nodes with LR $::datarate_modes($i) = $num_nodes($i) | expected % = $nodes_percentage($i) | actual % = [expr ( 100*$num_nodes($i)/$param(num_nodes) )]"
	}

	if { $sum < $param(num_nodes) } {
		if  { $kind == "long-range"} {  ;# update num_nodes($i) preferring lower datarates
			while { true } {
				for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
					set current_perc [expr ( 100*($num_nodes($i))/$param(num_nodes) )]
					if { $nodes_percentage($i) == 0 } { continue }
					if { $current_perc > $nodes_percentage($i) } { continue }
					incr num_nodes($i)
					incr sum
					if {$sum == $param(num_nodes) } { break }
				}
				if {$sum == $param(num_nodes) } { break }
log 3 "------"
for {set j 0} {$j < $param(num_dr_modes)} {incr j} {
log 3 "Num nodes with $::datarate_modes($j) = $num_nodes($j) | expected % = $nodes_percentage($j) | actual % = [expr ( 100*$num_nodes($j)/$param(num_nodes) )]"
}
			}
		}
		if  { $kind == "short-range"} {  ;# update num_nodes($i) preferring higher datarates
			while { true } {
				for {set i [expr $param(num_dr_modes)-1]} {$i >= 0} {set i [expr $i-1]} {
					set current_perc [expr ( 100*($num_nodes($i))/$param(num_nodes) )]
					if { $nodes_percentage($i) == 0 } { continue }
					if { $current_perc > $nodes_percentage($i) } { continue }
					incr num_nodes($i)
					incr sum
					if {$sum == $param(num_nodes) } { break }
				}
				if {$sum == $param(num_nodes) } { break }
log 3 "------"
for {set j 0} {$j < $param(num_dr_modes)} {incr j} {
log 3 "Num nodes with $::datarate_modes($j) = $num_nodes($j) | expected % = $nodes_percentage($j) | actual % = [expr ( 100*$num_nodes($j)/$param(num_nodes) )]"
}
			}
		}
	}

log 1 "------"
for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
log 1 "Num nodes with $::datarate_modes($i) = $num_nodes($i) | expected % = $nodes_percentage($i) | actual % = [expr ( 100*$num_nodes($i)/$param(num_nodes) )]"
}
	set sum 0
	for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
		set sum [expr $sum+$num_nodes($i)]
	}
log 2 "------> tot numbers of node set: $sum"

	# Assign randomly a datarate for each node accordingly to the percentages calculated before

	for {set i 1} {$i <= $param(num_nodes)} {incr i} {
		set stream($i) -1
	}
	set count 1
	while {$count <= $param(num_nodes)} {
		set xxx [RandomInteger4 1 $param(num_nodes) $::mRNG]

		while { true } {
			set already_exist 0
			for {set i 1} {$i <= $param(num_nodes)} {incr i} {
				if { $stream($i) == $xxx } {
					set already_exist 1
					break
				}
			}
			if { $already_exist == 1 } {
				set xxx [RandomInteger4 1 $param(num_nodes) $::mRNG]
			}
			if { $already_exist == 0 } {
				break
			}
		}
		set stream($count) $xxx
		incr count
	}
for {set i 1} {$i <= $param(num_nodes)} {incr i} {
log 3 "-- $stream($i)"
}

	set count 1
	for {set i 0} {$i < $param(num_dr_modes)} {incr i} {
		for {set j 0} {$j < $num_nodes($i)} {incr j} {
			set deployed_datarate($stream($count)) $::datarate_modes($i)
			incr count
		}
	}

	if { $kind == "long-range"} {
		foreach {a b} [array get deployed_datarate] {
			set ::deployed_datarates_LR($a) $b
		}
	}
	if { $kind == "short-range"} {
		foreach {a b} [array get deployed_datarate] {
			set ::deployed_datarates_SR($a) $b
		}
	}

}

################################################################################
proc log { verb m } {
	set v 0
	if {$verb <= $v} { 
		puts "$m" 
		return
	}
}

