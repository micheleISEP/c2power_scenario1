#include <packet.h>
#include <c2power.h>
#include <sstream>
#include <string>
//#include <module.h>
#include "c2power_routing_pkts.h"

#include <packettracer.h>
#include <sap.h>

class C2PowerTracer : public Tracer
{
	public:
		C2PowerTracer();
	protected:
		void format(Packet *p, SAP* sap);
};


C2PowerTracer::C2PowerTracer() : Tracer(3) {}


void C2PowerTracer::format(Packet *p, SAP *sap)
{

	hdr_cmn *ch = hdr_cmn::access(p);
	hdr_c2power* hdr = hdr_c2power::access(p);

	if ( hdr->tipo == C2POWER_BEACON) {
		writeTrace(sap, " [C2P] BEACON ");
		return;
	}
	else if ( hdr->tipo == C2POWER_ROUTING ) {
		hdr_c2p_rout *hr = HDR_C2P_ROUTING(p);
		hdr_c2p_rreq* hr_rreq;
		hdr_c2p_rrep* hr_rrep;
		hdr_c2p_data* hr_data;

		switch ( hr->h_type ) {
			case C2POWER_ROUT_DATA:
				hr_data = hdr_c2p_data::access(p);
				writeTrace(sap, " [C2P] DATA %s Next:%d ",
						hr->strRoute().c_str(),
						hr_data->next_node);
				break;
			case C2POWER_ROUT_RREQ:
				hr_rreq = hdr_c2p_rreq::access(p);
				writeTrace(sap, " [C2P] RREQ ID:%d DST:%d.%d.%d.%d %s ",
						hr_rreq->rreqId,
						(hr->dst & 0xff000000)>>24,
						(hr->dst & 0x00ff0000)>>16,
						(hr->dst & 0x0000ff00)>>8,
						(hr->dst & 0x000000ff),
						hr->strRoute().c_str() );
				break;
			case C2POWER_ROUT_RREP:
				hr_rrep = hdr_c2p_rrep::access(p);
				writeTrace(sap, " [C2P] RREP ID:%d Next:%d DST:%d %s ",
						hr_rrep->rreqId,
						hr_rrep->next_hop,
						hr->dst,
						hr->strRoute().c_str() );
				break;
			case C2POWER_ROUT_RERR:
				writeTrace(sap, " [C2P] RERR %s",
						hr->strRoute().c_str() );
				break;
			default:
				writeTrace(sap, " [C2P] UNKN pkt ");
		}
		return;
	}


}

// Why tracer doesn't work using C2powertracer_Init??
// Does this function must be defined somewhere??
extern "C" int C_Init()
{
	SAP::addTracer(new C2PowerTracer);
	return 0;
}

