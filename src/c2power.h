
#ifndef ns_c2power_h
#define ns_c2power_h

#include <node-core.h>
#include <module.h>

#include <vector>
#include <phymib.h>

//#include <mac-802_11mr.h>
//#include <module-80211-wrap.h>

#include <list>

#include "c2power_structures.h"
#include "c2power_interfaces.h"
#include "c2power_routing_pkts.h"

#include <sap.h>
#include "c2power-plugin.h"
#include "c2power_clmsg.h"


#define C2POWER_BEACON 1
#define C2POWER_HDR_LEN 16
//#define C2POWER_BEACON_NO_LR 2
//#define C2POWER_BEACON_LR 3

//#define C2POWER_RReq 4
//#define C2POWER_RRes 5
//#define C2POWER_RAck 6

#define C2POWER_NODSEL_STATS 2
#define C2POWER_NODSEL_ROUTE 3
#define C2POWER_ROUTING 4

// powers of 2:
#define C2POWER_DEBUG_SKELETON 1
#define C2POWER_DEBUG_NODSEL 2
#define C2POWER_DEBUG_CLUSTERING 4
#define C2POWER_DEBUG_ROUTING 8

#define BS_ADDRESS 255

extern packet_t PT_C2POWER;

// Riccardo: define which algorithm to enable within c2power layer when a data packet is handled from higher layers
#define C2P_UNDEFINED		0
#define C2P_RELAYNG			1
#define C2P_NODESELECTION	2
#define C2P_ROUTING			3

/** common header for the C2POWER layer */
class hdr_c2power {
 public:
  char tipo; // see C2POWER_* list
  int mitt_id; double mitt_x, mitt_y; // example of data collection about nodes

  //int interface_num;
  bool hasLongRangeDev; //indicates if long range device is available
  //some parameters broadcasted in a beacon
  double long_snr;
  double long_sinr;

  int LongBSS_ID;
  int LongDataRate;

  nsaddr_t LongAddr;
  nsaddr_t ShortAddr;

  static int offset_;
  inline static int& offset() {return offset_;}
  inline static hdr_c2power* access(const Packet* p) {
    return (hdr_c2power*) p->access(offset_);
  }
};

class C2PowerLayer : public Module {
  friend class MockNodeSelectionMT;
  friend class MockNodeSelectionBS;
 public:
  C2PowerLayer();
  virtual ~C2PowerLayer();
  virtual int command(int argc, const char*const* argv);
  virtual void recv(Packet*pkt);
  virtual void recv(Packet*pkt, int idSrc);

  virtual int recvAsyncClMsg(ClMessage* m);

  int addr_;
  int Debug(int what) {
    // if (debug_>0 && what==C2POWER_DEBUG_SKELETON) return 1; //Riccardo: always put skeleton debug info if debug enabled
    return debug_ & what;
  }

  int using_long_range;

  double beaconPeriod;
  double neighbourListPeriod;

  double neighbourListRemoveTreshold;// if last beacon arrived before this time the entry is removed as obsolete

  // Riccardo: Indicates which algorithm to use (see C2PowerLayer::recv() function )
//  int useAlgorithm;

  // Riccardo: If within a scenario we want to create a node with disabled c2power layer
  //           and follow IP routing rules
//  bool useC2Power;


 private:
  static int last_addr_;
  double area_size_x, area_size_y;

  nsaddr_t LongAddr;
  nsaddr_t ShortAddr;
  int C2PowerDeviceID;
  int LongRangeDeviceID;
  int LongBSS_ID;

  nsaddr_t LongSubnet;
  nsaddr_t ShortSubnet;

  PhyMode ShortDataRateVal; //value of C2Power interface datarate [Mbps]

  // Riccardo: stores info of layers below (see c2power_structures.cc)
  stack_struct* node_stacks;

  // Riccardo: this is the plug-in containing the energy model
  C2PEnergyModel* energyModel_;

  ClusterManager* cm_;
  RelayingManager* rm_;

  NodeSelectionMT* ns_mt_;
  NodeSelectionBS* ns_bs_;

  RoutingManager* rtm_;

  vector<double> SNRvect; //collects SNRs form received frames form BS in order to calculate the average value;
  int LatestDataMode; //tracks current data rate

  inline double my_x() {return getPosition()->getX();}
  inline double my_y() {return getPosition()->getY();}


  // the ClusterManager decides when/how to send the beacon (politics)
  // the c2power module performs the actual network operations
 public:
  int SendBeacon();
  int SendData(int DeviceID);

  bool IsCooperationPossible();

  //  friend void C2PowerBeaconTimer::handle(Event *e);

  int SendPkt(Packet* pkt); //used by those blocks who want to send directly pkts

  void TurnOnOffInterface(int onoff, int longrange);
  int down_if(int longrange);

  // Riccardo: this function informs c2power algorithm about a removed entry in the neightbour list.
  void neigbourRemoved(int c2paddr);

  // Riccardo: abilitate c2p algorithms to set on/off the beaconing when they need neighbour table info.
  // NOTE: Any drawback if an algorithm set the beaconing OFF?
  //       do we need a test on which algorithms need the beaconong facility?
  void setBeaconingON();
  void setBeaconingOFF();

  // Riccardo
	  void sendSyncClMsg(ClMessage* m);
	  void SendUp(Packet* pkt);
	  void discovery();
	  stack_struct* getStack() {return node_stacks;};
	  C2PEnergyModel* getEnergyModel() { assert(energyModel_!=0); return energyModel_; };
	  void Drop(Packet* pkt,const char* str) {drop(pkt,0,str);};
  //


  /**
     HERE the code would let you manage different ClusterManagers from tcl code. On the other hand, the simulator would be slightly slower, and the tcl code would be more complex (for each node, you would have to create one or more ClusterManagers, add them to the node, and finally activate one of the ClusterManagers)

  list<ClusterManager> cluster_manager
  */

	  // michele: energy cost for an interface
	  double CostInterface(int which);
};


#endif // ns_c2power_h
