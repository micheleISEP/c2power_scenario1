#ifndef C2POWER_NEIGHBOUR_TABLE_H_
#define C2POWER_NEIGHBOUR_TABLE_H_

#include<vector>


//class C2Power_InterfaceInfo
//{
//public:
//	C2Power_InterfaceInfo();
//	C2Power_InterfaceInfo(int,double,double);
//	~C2Power_InterfaceInfo();

//private:
//	int id_;
//	int type_;
//	double snr_;
//	double sinr_;
//};

/** Class represents an entry in neighbour list */
class C2Power_NeighbourListEntry
{
	public:

	C2Power_NeighbourListEntry();
	C2Power_NeighbourListEntry(int);
	C2Power_NeighbourListEntry(int,double);
	C2Power_NeighbourListEntry(int,double,double);

	void SetAddr(int);
	int GetAddr();

	void SetSNR(double);
	double GetSNR();

	void SetSINR(double);
	double GetSINR();

	void SetTimeStamp(double);
	double GetTimeStamp();

	//void SetInterfaceNum(int);
	//int GetInterfaceNum();

	void SetLongRangeDevice(bool);
	bool IsLongRangeDevice();

	void SetLongSNR(double);
	double GetLongSNR();

	void SetLongSINR(double);
	double GetLongSINR();

	void SetLongBSSID(int);
	int GetLongBSSID();

	void SetLongDataRate(int);
	int GetLongDataRate();

	void SetLongAddr(int);
	int GetLongAddr();

	void SetShortAddr(int);
	int GetShortAddr();

private:
int addr_; //c2power address (ID)
double snr_;
double sinr_;
double time_stamp_;

bool longRangeDeviceAvailable;

double long_snr_;
double long_sinr_;
int long_bssid_;
int long_data_rate_;

int longAddr_;
int shortAddr_;

//int interfaceNum_;

//std::vector<C2Power_InterfaceInfo> m_Interfaces;

};


/** C2Power_NeighbourList collects information about one-hop neighbours*/

class C2Power_NeighbourList
{
public:
	C2Power_NeighbourList();

	~C2Power_NeighbourList();

	bool addEntry(C2Power_NeighbourListEntry);
	//bool removeEntry(C2P_NeighbourListEntry&);
	bool removeEntry(int);
	int getListSize();

	C2Power_NeighbourListEntry& getEntry(int idx);
	int getEntryByID(int node_id);
	//int getEntryIndex(C2P_NeighbourListEntry&);

	C2Power_NeighbourListEntry& operator[](int idx);

	C2Power_NeighbourListEntry getBestNeighbour();


	bool clearList();



private:
	std::vector<C2Power_NeighbourListEntry> m_EntryContainer;

};

#endif /* C2POWER_NEIGHBOUR_TABLE_H_ */
