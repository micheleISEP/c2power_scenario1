#!/usr/bin/perl

opendir (my $dir, ".");
while ($fname = readdir($dir)) {
    if (($traffic,$rate,$num_nodes,$i,$nodsel) = $fname =~ /^run.(\d+).(\d+).(\d+).(\d+).(\d+).tcl.out/) {
	open FIN, "<$fname";
	$expected_id = 1;
	$cost=0;
	$packets=0;
	while ($riga = <FIN>) {
	    if ($riga =~ /^C2power/) {
		if ($riga =~ /^C2power (\d+): 151\.000000 Trace Energy NRG_\d+ ([\d\.]+)/) {
		    $found_id = $1;
		    $cost += (5000-$2 - 50*0.110); # subtracting the first 50 seconds like it was idle. This way, I still consider overhead (time in tx & rx)
		}
		if ($riga =~ /^C2power (\d+): CBR_getrecvpkts: (\d+)/) {
		    $found_id = $1;
		    $packets += $2;
		}
		if ($expected_id != $found_id) {
		    die "\nfalta um valor\n\n";
		}
		if ($num_nodes == $expected_id) {$expected_id = 0;}
		$expected_id++;
	    }
	}
	if ($expected_id != 1) {die "\nfalta o ultimo valor\n\n";}
	close FIN;
	$packets /= $num_nodes;
	$cost /= $num_nodes;
	my $oneresult = "$traffic,$rate,$num_nodes,$i,$nodsel,$packets,$cost\n";
	push @results, $oneresult;
    }
}

closedir $dir;
@sortedresults = sort @results;
#print "@sortedresults";
$pa[0] = 0; $pa[1] = 0; $en[0] = 0; $en[1] = 0;
for ($i=0;$i<=$#sortedresults;$i++) {
    ($row,$count,$nodsel,$packets,$cost) = $sortedresults[$i] =~ /([,\.0-9]+),([0-9]),([0-9]),([0-9\.]+),([0-9\.]+)/;
#    print "$sortedresults[$i]";
#    print "$row - $count - $nodsel - $packets - $cost\n";
    if (int($i%20/2) != $count) {die "\nerrore nell' analisi\n\n";}
    $en[$i%2] += $cost;
    $pa[$i%2] += $packets;
    if ($i%20 == 19) {
	$pa[0] /= 10; $pa[1] /= 10; $en[0] /= 10; $en[1] /= 10;
	$fpa = $pa[1]/$pa[0]; $fen = $en[1]/$en[0];
	print "$row,$pa[0],$pa[1],$en[0],$en[1],$fpa,$fen\n";
	$pa[0] = 0; $pa[1] = 0; $en[0] = 0; $en[1] = 0;
    }
}
