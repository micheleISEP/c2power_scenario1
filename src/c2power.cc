//#include <ip.h>
#include "c2power.h"
//#include "c2power_beacon.h"
#include "c2power_mock.h"
#include <mrcl-mac80211.h>
#include <c2power_clmsg.h>

#include <math.h>
#include <ipmodule.h>

#include <clmsg-phy-on-off-switch.h>
#include <../mmac/mmac.h>

//struct C2P_Notify_Crl_Message;

int hdr_c2power::offset_=0;
packet_t PT_C2POWER;
int C2PowerLayer::last_addr_ = 0;

C2PowerLayer::C2PowerLayer() :beaconPeriod(0.5),neighbourListPeriod(1),neighbourListRemoveTreshold(3) {
  addr_ = ++last_addr_;

  // -1 use routing table, 0 short range, 1 long range
  using_long_range = -1;
  //  bind("addr_", &addr_);
  bind("beaconPeriod_", &beaconPeriod);
  bind("neighbourListPeriod_", &neighbourListPeriod);
  bind("neighbourListRemoveTreshold_", &neighbourListRemoveTreshold);

  if (Debug(C2POWER_DEBUG_CLUSTERING)) {
    printf(" beaconPeriod set to %f\n",beaconPeriod);
    printf(" neighbourListPeriod set to %f\n",neighbourListPeriod);
    printf(" neighbourListRemoveTreshold set to %f\n",neighbourListRemoveTreshold);
  }
  cm_ = new MockClusterManager(this);
  C2PowerDeviceID =-1;
  LongRangeDeviceID =-1;

//  rm_ = new MultiradioRelayingManager(this,cm_);
  rm_ = NULL;

  rtm_ = NULL;

  ns_mt_ = NULL;
  ns_bs_ = NULL;
  //  ns_mt_ = new MockNodeSelectionMT(this);

  node_stacks = new stack_struct(); // Riccardo: istantiate node_stacks structure

//  useAlgorithm = C2P_UNDEFINED;
//  useC2Power = true; // We use c2power layer by default

  //calcPackets = 0;
}

C2PowerLayer::~C2PowerLayer() {
  if (cm_)
    delete cm_;
  if (rm_)
    delete rm_;
  if (rtm_)
    delete rtm_;
  if (ns_mt_)
    delete ns_mt_;
  if (ns_bs_)
    delete ns_bs_;
  if (node_stacks)
      delete node_stacks;
}

int C2PowerLayer::SendBeacon() {
  /******************
HERE
you can see an example of creation of a packet, to be sent
to neighboring nodes.
   ******************/
  Packet* pkt = Packet::alloc();
  hdr_c2power* hdr = hdr_c2power::access(pkt);
  hdr->tipo = C2POWER_BEACON;
  hdr->mitt_id = addr_;
  hdr->mitt_x = my_x();
  hdr->mitt_y = my_y();
  if(LongRangeDeviceID!=-1 && SNRvect.size() >0) //if long Range Dev is installed and received any frames
  {
	  hdr->hasLongRangeDev = true;
	  double avgSNR = 0;
	  for(int i=0; i<SNRvect.size();i++){
		  avgSNR += SNRvect[i];
	  }
	  avgSNR = avgSNR/SNRvect.size();
	  hdr->long_snr = avgSNR;
	  hdr->LongBSS_ID = LongBSS_ID;
	  hdr->LongDataRate = LatestDataMode;
	  hdr->LongAddr = LongAddr;

	  //printf("!!!!!!!!!!! %d\n",hdr->LongDataRate);
  } else {
	  hdr->hasLongRangeDev = false;
	  hdr->long_snr = -100;
	  hdr->LongBSS_ID = -100;
	  hdr->LongDataRate = -18;
  }

  hdr_cmn* hdrc = hdr_cmn::access(pkt);
  hdrc->ptype() = PT_C2POWER;
  hdrc->next_hop() = IP_BROADCAST;
  hdrc->addr_type() = NS_AF_INET;
  hdrc->direction() = hdr_cmn::DOWN;
  hdrc->size() += C2POWER_HDR_LEN;

  hdr_ip *iph = hdr_ip::access(pkt);
  iph->daddr() = IP_BROADCAST;// << Address::instance().nodeshift();
  iph->ttl() = IP_DEF_TTL;
  iph->saddr() = 0;
  hdrc->size() += IP_HDR_LEN;
  //  iph->saddr() = addr_;

  if (Debug(C2POWER_DEBUG_CLUSTERING)) printf("node %d: sending a beacon\n",addr_);


  sendDown(C2PowerDeviceID,pkt);
  return TCL_OK;
}


/*
void MockRoutingManager::sendPkt( Packet* pkt, int egress_ipif ) {

//	if (Debug(C2POWER_DEBUG_ROUTING)) printf("sendPkt( pkt, %d );\n", egress_ipif);

	int using_what = ag_->using_long_range;

	int stack=-1;
	for (int i=0 ; i<ag_->getStack()->getSize() ; i++ ) {
		if ( ag_->getStack()->getEntry(i).getIPIFaddr() == egress_ipif ) {
			stack = i;
			break;
		}
	}
	if (stack == -1) {
		if (Debug(C2POWER_DEBUG_ROUTING)) printf("ERROR trying to send packet with egress_ipif=%d\n",egress_ipif);
		assert(stack != -1);
	}

	if (stack == 1) {
		ag_->TurnOnOffInterface(1,0);
		if (turnOnOff>0 && turnONperiod>0) {
			timerLongRange_->resched(turnONperiod);
		}
	}
	ag_->using_long_range = stack;
	ag_->recv(pkt);
	ag_->using_long_range = using_what;
}
*/


// retrieve energy measures related to src node
double C2PowerLayer::CostInterface(int which) {
  ClMsgC2pPtx* m;
  double Ptx, Rb, Cost;
  ClMsgC2pDataRate* md;
  //  ClMsgC2pGetNrgPointer* mm;

  m = new ClMsgC2pPtx(UNICAST, getStack()->getEntry(which).getPHYid() );
  sendSyncClMsg(m);
  Ptx = m->getTXPower();
  Cost = m->getCost();
  md = new ClMsgC2pDataRate(UNICAST, getStack()->getEntry(which).getMACid() );
  sendSyncClMsg(md);
  Rb = md->getDataRate();
  //      mm = new ClMsgC2pGetNrgPointer(UNICAST, getStack()->getEntry(0).getPHYid() );
  delete m;
  delete md;

  /*  printf("IF %s Ptx %g Rb %g\n", which ? "long" : "short", Ptx, Rb);
  printf("\tenergyCost:\t");
  for (int i=0;i<4;i++) printf("%g\t", Cost[i]);
  printf("\n");
  */

  //  return Ptx/Rb;
  // ENERGY_STATE_TX = 1
  return Cost/Rb;

  /*
    mm = new ClMsgC2pGetNrgPointer();
    sendSyncClMsg(mm);
    EnergyModel* em = mm->getPointer();
    delete mm;
  */
}


int C2PowerLayer::command(int argc, const char*const* argv) {
  Tcl& tcl = Tcl::instance();

  // Skeleton:
  if (argc == 2) {
    if (!strcmp(argv[1], "shortrange")) {
      using_long_range = 0;
      return TCL_OK;
    }
    if (!strcmp(argv[1], "longrange")) {
      using_long_range = 1;
      return TCL_OK;
    }
    if (!strcmp(argv[1], "anyrange")) {
      using_long_range = -1;
      return TCL_OK;
    }
    if (!strcmp(argv[1], "long_off")) {
      TurnOnOffInterface(0, 1);
      return TCL_OK;
    }
    if (!strcmp(argv[1], "long_on")) {
	  TurnOnOffInterface(1, 1);
	  return TCL_OK;
	}
    if (!strcmp(argv[1], "short_off")) {
      TurnOnOffInterface(0, 0);
      return TCL_OK;
    }
    if (!strcmp(argv[1], "short_on")) {
      TurnOnOffInterface(1, 0);
      return TCL_OK;
    }




    // Clustering:
    if (!strcmp(argv[1], "beacon")) {
      return SendBeacon();
    }
    if (!strcmp(argv[1], "start_beaconing")) {
      //return cm_->Beaconing(true);
    	return cm_->BeaconingON();
    }
    if (!strcmp(argv[1], "stop_beaconing")) {
      //return cm_->Beaconing(false);
    	return cm_->BeaconingOFF();
    }




    // Node selection:
    if (!strcmp(argv[1], "nodsel_MT")) {
      assert(ns_mt_ == NULL && ns_bs_ == NULL);
      ns_mt_ = new MockNodeSelectionMT(this);
      return TCL_OK;
    }
    if (!strcmp(argv[1], "nodsel_BS")) {
      //      addr_ = BS_ADDRESS;
      assert(ns_mt_ == NULL && ns_bs_ == NULL);
      ns_bs_ = new MockNodeSelectionBS(this);
      return TCL_OK;
      //    	return cm_->BeaconingOFF();
    }

    if (!strcmp(argv[1], "nodsel_setuproutes")) {
      if (ns_mt_)
	ns_mt_->SetupRoute(1);
      return TCL_OK;
    }

    if (!strcmp(argv[1], "nodsel_setuproutes_IFON")) {
      if (ns_mt_)
	ns_mt_->SetupRoute(0);
      return TCL_OK;
    }

    if (!strcmp(argv[1], "nodsel_deliverroutes")) {
      assert(ns_bs_);
      ns_bs_->ComputeRoutes();
      ns_bs_->SendRoutes();
      return TCL_OK;
    }

    if (!strcmp(argv[1], "nodsel_sendstats")) {
      assert(ns_mt_);
      /*
      C2Power_NeighbourList* nl = cm_->GetNeighbourList();
      for (int i=0;i<nl->getListSize();i++) {
	C2Power_NeighbourListEntry nle = nl->getEntry(i);
	if (Debug(C2POWER_DEBUG_NODSEL)) printf("neighs data: addr_ %d, nle.GetAddr() %d, nle.GetSNR() %f\n", addr_, nle.GetAddr(), nle.GetSNR());

	statistics_node_selection sns(addr_, nle.GetAddr(), nle.GetSNR());
      */
      // short_range
      statistics_node_selection snsShort(addr_, ShortAddr, CostInterface(0), 0);
      statistics_node_selection snsLong(addr_, LongAddr, CostInterface(1), 1);

      return ns_mt_->SendStatisticsUp(&snsShort, &snsLong);
    }

    if (!strcmp(argv[1], "nodsel_dumpdata")) {

      // retrieve energy measures related to src node
      double energyCostShort = CostInterface(0);
      double energyCostLong = CostInterface(1);

      printf("energyCostShort %g energyCostLong %g\n", energyCostShort, energyCostLong);

      if (ns_mt_) ns_mt_->Dump();
      if (ns_bs_) ns_bs_->Dump();

	/*	for (int i=0; i<hr->routelen; i++) {
		if ( hr->addrs[i].c2p_addr == thisnodeaddr ) {
			return hr->addrs[i].ipif_addr_out;
			break;
		}
		}

*/
	/*
	  for(int idx=0;idx <neigbour_list_.getListSize();idx++) {
	    if(neigbour_list_[idx].GetLongBSSID()>=0) {
	      printf("|%*d    ",3,neigbour_list_[idx].GetAddr());
	      parse32Addr(neigbour_list_[idx].GetShortAddr());
	      printf("    | %3.3f ||   %d    ", neigbour_list_[idx].GetSNR(),neigbour_list_[idx].GetLongBSSID());
	      parse32Addr(neigbour_list_[idx].GetLongAddr());
	      //printf("   | %3.3f |  %*s  %2.2f|\n",neigbour_list_[idx].GetLongSNR(),8,PhyMode2str((PhyMode)neigbour_list_[idx].GetLongDataRate()),(phymib.getRate((PhyMode)neigbour_list_[idx].GetLongDataRate()))/1000000);
	    } else {
	      printf("  %*d | %3.3f ||\n",3,neigbour_list_[idx].GetAddr(), neigbour_list_[idx].GetSNR());
	    }
	    MR_PHY_MIB phymib;
	    double myRateToBS = (phymib.getRate((PhyMode)LatestDataMode))/1000000;
	    double myRateToBestRelay = (phymib.getRate(ShortDataRateVal))/1000000;
	    double RateFromRelayToBS = (phymib.getRate((PhyMode)neigbour_list_[idx].GetLongDataRate()))/1000000;
	    printf("myRateToBS %g myRateToBestRelay %g RateFromRelayToBS %g\n", myRateToBS, myRateToBestRelay, RateFromRelayToBS);
	  }
	  printf("-----------------------------------------------------------------------\n");
	}
	*/
		return TCL_OK;
      }

    if (!strcmp(argv[1], "getAddr")) {
      tcl.resultf("%d", addr_);
      return TCL_OK;
    }

    if (!strcmp(argv[1], "relaying")) {
      rm_ = new MultiradioRelayingManager(this,cm_);
	  return TCL_OK;
	}

    if (!strcmp(argv[1], "routing")) {
	  rtm_ = new MockRoutingManager(this);
	  return TCL_OK;
	}

    // Riccardo: Launch discovery function to initialize the "node_stacks" structure.
	if (!strcmp(argv[1], "discovery")) {
		discovery();
		return TCL_OK;
	}

  }
  if (argc == 3) {
//		if (!strcmp(argv[1], "useC2Power")) {
//			if (!strcmp(argv[2], "true")) {
//				printf("set useC2Power:  %s \n", argv[2]);
//				useC2Power = true;
//			}
//			else if (!strcmp(argv[2], "false")) {
//				printf("set useC2Power:  %s \n", argv[2]);
//				useC2Power = false;
//			}
//			else
//				return TCL_ERROR;
//			return TCL_OK;
//		}
//	    // Riccardo: added to set useAlgorithm (see recv() function)
//		if (!strcmp(argv[1], "useAlgorithm"))
//		{
//			printf("set useAlgorithm:  %d \n", atoi(argv[2]));
//			useAlgorithm = atoi(argv[2]);
//			return TCL_OK;
//		}
	    if (!strcmp(argv[1], "setC2PowerDevice"))
	    //if(strcasecmp(argv[1], "setC2PowerDevice")==0)
	    {
	    	printf("set C2PowerDevice:  %d \n", atoi(argv[2]));
	    	C2PowerDeviceID = atoi(argv[2]);
	    	return TCL_OK;
	    }
	    if (!strcmp(argv[1], "setLongRangeDevice"))
	    //if(strcasecmp(argv[1], "setC2PowerDevice")==0)
	    {
	    	printf("set LongRangeDevice:  %d \n", atoi(argv[2]));
	    	LongRangeDeviceID = atoi(argv[2]);
	    	return TCL_OK;
	    }

	    if (!strcmp(argv[1], "setLongRangeDeviceAddr"))
	    //if(strcasecmp(argv[1], "setC2PowerDevice")==0)
	    {
	    	printf("set LongRangeDeviceAddr: ");// %d \n", atoi(argv[2]));
	    	LongAddr = atoi(argv[2]);
	    	parse32Addr(LongAddr);
	    	printf("\n");
	    	return TCL_OK;
	    }

	    if (!strcmp(argv[1], "setShortRangeDeviceAddr"))
	    //if(strcasecmp(argv[1], "setC2PowerDevice")==0)
	    {
	    	printf("set ShortRangeDeviceAddr: ");// %d \n", atoi(argv[2]));
	    	ShortAddr = atoi(argv[2]);
	    	parse32Addr(ShortAddr);
	    	printf("\n");
	    	return TCL_OK;
	    }
	    if (!strcmp(argv[1], "setShortRangeDeviceSubnet"))
	    //if(strcasecmp(argv[1], "setC2PowerDevice")==0)
	    {
	    	printf("set ShortRangeDeviceSubnet: ");// %d \n", atoi(argv[2]));
	    	ShortSubnet = atoi(argv[2]);
	    	parse32Addr(ShortSubnet);
	    	printf("\n");
	    	return TCL_OK;
	    }
	    if (!strcmp(argv[1], "setLongRangeDeviceSubnet"))
	    //if(strcasecmp(argv[1], "setC2PowerDevice")==0)
	    {
	    	printf("set LongRangeDeviceSubnet:  ");//%d \n", atoi(argv[2]));

	    	LongSubnet = atoi(argv[2]);
	    	parse32Addr(LongSubnet);
	    	printf("\n");
	    	//printf('\n');
	    	return TCL_OK;
	    }
	    if (!strcmp(argv[1], "setC2PowerDatarate")) {
	      //return cm_->Beaconing(false);
	    	//printf("--->>> %d",calcPackets);
	    	ShortDataRateVal =  str2PhyMode(argv[2]);
	    	return TCL_OK;
	    }
  }

  // Riccardo: set routing parameters
  if (argc == 4) {
	if (!strcmp(argv[1], "set_rout_param")) {
		assert(rtm_);
		MockRoutingManager* rrr =  (MockRoutingManager*)rtm_;
		assert(rrr);

		if (!strcmp(argv[2], "alpha")) {
			printf("set routing alpha: " );
			rrr->alpha_ = atof(argv[3]);
			printf("%f\n", rrr->alpha_ );
			return TCL_OK;
		}
		if (!strcmp(argv[2], "hoplimit")) {
			printf("set routing hoplimit: ");
			rrr->hoplimit = atoi(argv[3]);
			printf("%d\n", rrr->hoplimit );
			return TCL_OK;
		}
		if (!strcmp(argv[2], "reBroadcastLimit")) {
			printf("set routing reBroadcastLimit: ");
			rrr->reBroadcastLimit = atoi(argv[3]);
			printf("%d\n", rrr->reBroadcastLimit );
			return TCL_OK;
		}
		if (!strcmp(argv[2], "rreqtimeout")) {
			printf("set routing rreqtimeout: ");
			rrr->rReqTimeOut = atof(argv[3]);
			printf("%f\n", rrr->rReqTimeOut );
			return TCL_OK;
		}
		if (!strcmp(argv[2], "broadcast")) {
			printf("set routing broadcast: ");
			rrr->use_broadcast = atoi(argv[3]);
			printf("%d\n", rrr->use_broadcast );
			return TCL_OK;
		}
		if (!strcmp(argv[2], "routeValidity")) {
			printf("set routing routeValidity: ");
			rrr->routeValidity = atof(argv[3]);
			printf("%f\n", rrr->routeValidity );
			rrr->refreshRouteTable();
			return TCL_OK;
		}
		if (!strcmp(argv[2], "turnLongOnOff")) {
			printf("set routing turnLongOnOff: ");
			rrr->turnOnOff = atoi(argv[3]);
			printf("%d\n", rrr->turnOnOff );
			if (rrr->turnOnOff>0) { // Always starts with long ON if we want turnLongOnOff capability
				TurnOnOffInterface(1,1);
				rrr->isLongON = true;
			}
			return TCL_OK;
		}
		if (!strcmp(argv[2], "turnONperiod")) {
			printf("set routing turnONperiod: ");
			rrr->turnONperiod = atof(argv[3]);
			printf("%f\n", rrr->turnONperiod );
			if (rrr->turnOnOff>0) {
				rrr->periodicLongScanning(true);
			}
			return TCL_OK;
		}
		if (!strcmp(argv[2], "turnOFFperiod")) {
			printf("set routing turnOFFperiod: ");
			rrr->turnOFFperiod = atof(argv[3]);
			printf("%f\n", rrr->turnOFFperiod );
			return TCL_OK;
		}
	}
  }



  /******************
HERE
you can write your handler for the commands to the C2POWER layer.
For example, wanting to create a cluster, you can call methods
like SendBeacon to create messages to other nodes.
In the other nodes, you will intercept the message, and process
it, possibly doing like RecvBeacon, storing some information
about the node that sent the beacon.

   ******************/


  return Module::command(argc, argv);
}

// Riccardo:
//   I need to call send-up method from routing block
//   (otherwise if I call recv() function I fall in a endless loop.)
void C2PowerLayer::SendUp(Packet* pkt) {
	int numUpLayers = getUpLaySAPnum();
	//      printf("from %d ... sending up\n", idSrc);
	sendUp(getUpLaySAP(0)->getModuleUpId(), pkt, 0.0);
}

void C2PowerLayer::recv(Packet* pkt) {
  return recv(pkt, -1);
}

void C2PowerLayer::recv(Packet* pkt, int idSrc) {
  hdr_c2power* hdr = hdr_c2power::access(pkt);
  hdr_ip* iph = hdr_ip::access(pkt);
  hdr_cmn* hdrc = hdr_cmn::access(pkt);

  if (Debug(C2POWER_DEBUG_SKELETON))
    printf("C2PowerLayer of %d received from %d upward? %d time %f\n", addr_, idSrc, hdrc->direction() == hdr_cmn::UP, NOW);


  if (NULL != ns_mt_)
    if (ns_mt_->RelayPacket(pkt))
      return;

  if (NULL != ns_bs_)
    if (ns_bs_->RedirectPacket(pkt))
      return;

  if(hdrc->direction() == hdr_cmn::UP) {
    if ((iph->daddr()%256 == hdrc->next_hop_%256) &&
        (iph->daddr() != hdrc->next_hop_)) {
      printf("iph->daddr %d LongAddr %d hdrc->next_hop_ %d ShortAddr %d\n",
	     iph->daddr(), LongAddr, hdrc->next_hop_, ShortAddr);
      if (Debug(C2POWER_DEBUG_SKELETON))
        printf("the packet IS for me. Redirecting to correct node interface\n");
      hdrc->next_hop_ = iph->daddr();
    }
  /*
    // ALBANO: if it is for me, I bypass the multi-interface logic
    if (
	(iph->daddr()==LongAddr && hdrc->next_hop_==ShortAddr) ||
	(iph->daddr()==ShortAddr && hdrc->next_hop_==LongAddr)) {
      printf("iph->daddr %d LongAddr %d hdrc->next_hop_ %d ShortAddr %d\n",
	     iph->daddr(), LongAddr, hdrc->next_hop_, ShortAddr);
      hdrc->next_hop_ = iph->daddr();
      if (Debug(C2POWER_DEBUG_SKELETON))
	printf("the packet IS for me. Redirecting to correct node interface\n");
    }
  */

  /******************
HERE
you intercept the custom C2POWER packets.
   ******************/
    if (hdr->tipo == C2POWER_BEACON) {
      cm_->RecvBeacon(pkt);
      return;
    } else if ( hdr->tipo == C2POWER_ROUTING ) {
	  rtm_->Recv(pkt,idSrc);
	  return;
    }  else if (hdr->tipo == C2POWER_NODSEL_STATS) {
      if (Debug(C2POWER_DEBUG_NODSEL)) printf("NODSEL node %d received statistics\n", addr_);
      if (ns_bs_ != NULL) ns_bs_->ReceiveStatistics(pkt);
    } else if (hdr->tipo == C2POWER_NODSEL_ROUTE) {
      if (Debug(C2POWER_DEBUG_NODSEL)) printf("NODSEL node %d received route\n", addr_);
      assert(ns_mt_);
      ns_mt_->ReceiveRoute(pkt);
    } else {
      int numUpLayers = getUpLaySAPnum();
      if (Debug(C2POWER_DEBUG_SKELETON))
	printf("from %d ... sending up\n", idSrc);
      if (numUpLayers == 1) {
	sendUp(getUpLaySAP(0)->getModuleUpId(), pkt, 0.0);
      } else {
	printf("\n\nmore than 1 up layer ... more than 1 IP routing layers ... WHY??????\n\n");
	exit(-1);
      }
    }

  } else { // direction == DOWN

	// Riccardo: Moved to the bottom... It is possible that I want to use c2power also if
	// I have only one IF (in case of routing)
//	int numDownLayers = getDownLaySAPnum();
//	if (numDownLayers == 1) {
//		sendDown(getDownLaySAP(0)->getModuleDownId(), pkt, 0.0);
//		return;
//	}

	// XXX Riccardo: ..I ask to test if I'm in-line with your intention:
	//	- using_long_range is used from every algorithms to send directly a packet to a specific IPIF
	//	- if (using_long_range == -1) then use specific policy on the basis of active algorithms

	if (Debug(C2POWER_DEBUG_SKELETON)) {
	printf("  using_long_range=%d", using_long_range);
	printf("    LongRangeDeviceID=%d", LongRangeDeviceID);
	printf("    C2PowerDeviceID=%d  \n", C2PowerDeviceID);
	}

	// Riccardo: Every algorithm that wants to send a packet sets using_long_range value properly.
    if (using_long_range == 1) {
    	if ( LongRangeDeviceID == -1 ) {
    		printf("LongRangeDeviceID not set. Not possible to use \"sendDown(LongRangeDeviceID,pkt,0.0);\"\n");
    		exit(-1);
    	}
    	sendDown(LongRangeDeviceID,pkt,0.0);
    	return;
    }
	if (using_long_range == 0) {
		if ( C2PowerDeviceID == -1 ) {
			printf("C2PowerDeviceID not set. Not possible to use \"sendDown(C2PowerDeviceID,pkt,0.0);\"\n");
			exit(-1);
		}
		sendDown(C2PowerDeviceID,pkt,0.0);
		return;
	}

	// Riccardo: Here falls only packets coming from higher layers.
	// On the basis of which algorithm is initialized, I use different policies..
	if (using_long_range == -1) {
	  if (NULL != ns_bs_) {



	  }



		if (NULL == rm_ && NULL==rtm_ ) {
			if (Debug(C2POWER_DEBUG_SKELETON)) printf(" Chosen direct handling\n");
			int net = int(hdrc->next_hop_/256);
			int host = hdrc->next_hop_ - net*256;
			if (Debug(C2POWER_DEBUG_SKELETON)) {
				printf("next hop = %d.%d\t", net, host);
				printf("dest = %d.%d\n", iph->daddr()/256, iph->daddr()%256);
			}
			if (2 == net) { // WIMAX
				sendDown(getDownLaySAP(1)->getModuleDownId(), pkt, 0.0);
				//	  sendDown(LongRangeDeviceID, pkt, 0.0);
			}
			if (1 == net) { // WiFi
				sendDown(getDownLaySAP(0)->getModuleDownId(), pkt, 0.0);
				//	  sendDown(C2PowerDeviceID,pkt,0.0);
			}
			return;
		}
		else if ( NULL != rm_ && NULL == rtm_ ) {
			if (Debug(C2POWER_DEBUG_SKELETON)) printf(" Chosen Relaying\n");
			if(rm_->SelectInterface(C2PowerDeviceID,LongRangeDeviceID,LatestDataMode,ShortDataRateVal)==C2PowerDeviceID) {
				C2Power_NeighbourListEntry tmp =cm_->GetNeighbourList().getBestNeighbour();
				//printf("node %d: Cooperation via %d\n",addr_,tmp.GetAddr());

				hdr_ip *iph = hdr_ip::access(pkt);
				iph->saddr() = LongAddr;//set my ip address of Long range

				hdrc->next_hop() = tmp.GetShortAddr();

				sendDown(C2PowerDeviceID,pkt,0.0);
			} else {
				sendDown(LongRangeDeviceID, pkt, 0.0);
			}
			return;
		}
		else if ( NULL == rm_ && NULL != rtm_ ) {
			if (Debug(C2POWER_DEBUG_SKELETON)) printf(" Chosen Routing\n");
			hdr_c2p_rout* hrout = HDR_C2P_ROUTING(pkt);

			if (	hrout->h_type == C2POWER_ROUT_RREQ ||
					hrout->h_type == C2POWER_ROUT_RREP ||
					hrout->h_type == C2POWER_ROUT_RERR )
				sendDown(pkt);
			else
			  {if(cm_->GetNeighbourList().getListSize()>0)
			      rtm_->RelayDataFrame(pkt);
			    else {
			      TurnOnOffInterface(1,1);
			      hdr_ip *iph = hdr_ip::access(pkt);
			      iph->saddr() = node_stacks->getEntry(0).getIPIFaddr();//quick hack to count pkts in CBR layer statistics...
			      sendDown(LongRangeDeviceID,pkt,0.0);
			    }
			  }
			return;
		}
		/*				rtm_->RelayDataFrame(pkt);
			return;
			}*/
		else if ( NULL != rm_ && NULL != rtm_ ) {
			if (Debug(C2POWER_DEBUG_SKELETON)) printf(" Choose between relaying and routing..");
			if(rm_->SelectInterface(C2PowerDeviceID,LongRangeDeviceID,LatestDataMode,ShortDataRateVal)==C2PowerDeviceID) {
				if (Debug(C2POWER_DEBUG_SKELETON)) printf(" chosen relaying.\n");
				C2Power_NeighbourListEntry tmp =cm_->GetNeighbourList().getBestNeighbour();
				//printf("node %d: Cooperation via %d\n",addr_,tmp.GetAddr());

				hdr_ip *iph = hdr_ip::access(pkt);
				iph->saddr() = LongAddr;//set my ip address of Long range

				hdrc->next_hop() = tmp.GetShortAddr();

				sendDown(C2PowerDeviceID,pkt,0.0);

			} else {
				if (Debug(C2POWER_DEBUG_SKELETON)) printf(" Chosen Routing\n");
				hdr_c2p_rout* hrout = HDR_C2P_ROUTING(pkt);

				if (	hrout->h_type == C2POWER_ROUT_RREQ ||
						hrout->h_type == C2POWER_ROUT_RREP ||
						hrout->h_type == C2POWER_ROUT_RERR )
					sendDown(pkt);
				else
					rtm_->RelayDataFrame(pkt);
			}
			return;
		}

	}
	// Moved to the bottom... It is possible that I want to use c2power also if
	// I have only one IF (i.e. relaying for routing)
	int numDownLayers = getDownLaySAPnum();
	if (numDownLayers == 1) {
		sendDown(getDownLaySAP(0)->getModuleDownId(), pkt, 0.0);
		return;
	}
	printf("ERROR: C2PowerLayer::recv(Packet* pkt, int idSrc) - Why I'm here!?\n");
	exit(-1);
  } // end of direction == DOWN
}

bool C2PowerLayer::IsCooperationPossible()
{
	if(cm_->GetNeighbourList().getListSize()>0)
	{
		//get a neighbour with best parameters
		C2Power_NeighbourListEntry tmp =cm_->GetNeighbourList().getBestNeighbour();
		MR_PHY_MIB phymib;
		double myRateToBS = (phymib.getRate((PhyMode)LatestDataMode))/1000000;
		double myRateToBestRelay = (phymib.getRate(ShortDataRateVal))/1000000;
		double RateFromRelayToBS = (phymib.getRate((PhyMode)tmp.GetLongDataRate()))/1000000;



		//double var2 = 1.0;
		//chceck whether transmission might be improved
		if(RateFromRelayToBS*myRateToBS*myRateToBestRelay!=0)
		{
			double var1 = 1.0/myRateToBS;
			double var2 = 1.0/myRateToBestRelay;
			double var3 = 1.0/RateFromRelayToBS;

			if(var1>var2+var3)
			{
				//printf("Rd=%2.1f, R1=%2.1f, R2=%2.1f\n",myRateToBS, myRateToBestRelay,RateFromRelayToBS);
				//printf("1/rd = %2.5f\n", 1/myRateToBS);
				//printf("1/R1 + 1/R2 = %2.5f\n", 1/myRateToBestRelay+1/RateFromRelayToBS);

				return true;
			}
		}
	}


	return false;
}

int  C2PowerLayer::recvAsyncClMsg(ClMessage* m) {
  //	printf("C2POWER::recvAsyncClMsg by node %d\n",addr_);
	if (m->type()==C2P_80211_SNR){
		//printf("  -->C2P_80211_SNR\n");


		//printf("   -->SNR: %f\n",10*log10(((C2P_Notify_Crl_Message*)m)->SNR_));
		//printf("   -->SINR: %f\n",10*log10(((C2P_Notify_Crl_Message*)m)->SINR_));

		//this is the BSS_ID I am connected to
		//LongBSS_ID = -1;
		LongBSS_ID = ((C2P_Notify_Crl_Message*)m)->GetBSSID();
		//printf("     BSS ID: %d\n", LongBSS_ID);


		//it = SNRvect.begin();
		SNRvect.insert(SNRvect.begin() , 10*log10(((C2P_Notify_Crl_Message*)m)->SNR_)); //add the recent value at the beginning

		LatestDataMode = ((C2P_Notify_Crl_Message*)m)->dataRate_;

		//printf("!!!!!!!!!!! %d",LatestDataMode);

		if(SNRvect.size()>=6) //keep last 6 frames
		{
			SNRvect.pop_back();
			//DataModevect.pop_back();
		}

		//((C2P_Notify_Crl_Message*)m)->

		//C2P_Notify_Crl_Message a;
		//C2P_Notify_Crl_Message b;

	}

	return 0;
}


int C2PowerLayer::down_if(int longrange) {
  int ret = -1;
  int numDownLayers = getDownLaySAPnum();
  if (numDownLayers != 2) {

    printf("ERROR in the number of down IFs: it is %d!\n", numDownLayers);
    exit(-1);
  }
  if (0 == longrange)
    ret = getDownLaySAP(0)->getModuleDownId();
  if (1 == longrange)
    ret = getDownLaySAP(1)->getModuleDownId();
  if (-1 == longrange) {
    printf("ERROR you are asking with IF while should be routing determined!\n");
    exit(-1);
  }
  return ret;
}

//Riccardo: Added for plug-in comm
void C2PowerLayer::sendSyncClMsg(ClMessage* m) {
	PlugIn::sendSyncClMsg(m);
}

void C2PowerLayer::TurnOnOffInterface(int onoff, int longrange) {
  //    printf("turn on called on %s\n", name());
//
//  MMac *mmac = (MMac*)(TclObject::lookup("ss_mac"));
//
//  /*  int ifip_id = down_if(longrange);
//  Module* ifip =
//
//  ClMsgPhyOnOffSwitch m(
//  getDownLaySAP(0);
//->downModule_;
//  if (onoff)
//    m.setOn();
//  else
//    m.setOff();
//  sendSyncClMsgDown(&m);
//  */
//  ClMsgPhyOnOffSwitch m;
//  m.setOff();
//  sendAsyncClMsg(&m);
//  //sendSynchronousClSAP

  printf("%s: IF %s switched %s at %f - sent clmsg to ID %d\n", getTag(), longrange?"LONG":"SHORT", onoff?"ON":"OFF", NOW, getStack()->getEntry(longrange).getPHYid() );
  // Riccardo:
	if ( onoff == 0 ) {
		if (Debug(C2POWER_DEBUG_SKELETON))
		  printf("%s: IF (longrange=%d) switched OFF at %f - sent clmsg to ID %d\n", getTag(), longrange, NOW, getStack()->getEntry(longrange).getPHYid() );
		ClMsgPhyOnOffSwitch* m = new ClMsgPhyOnOffSwitch( getStack()->getEntry(longrange).getPHYid()  );
		m->setOff();
		sendSyncClMsg(m);
		delete m;
	} else {
		if (Debug(C2POWER_DEBUG_SKELETON))
		  printf("%s: IF (longrange=%d) switched ON at %f - sent clmsg to ID %d\n", getTag(), longrange, NOW, getStack()->getEntry(longrange).getPHYid() );
		ClMsgPhyOnOffSwitch* m = new ClMsgPhyOnOffSwitch( getStack()->getEntry(longrange).getPHYid()  );
		m->setOn();
		sendSyncClMsg(m);
		delete m;
	}

}

void C2PowerLayer::neigbourRemoved(int c2paddr) {

	// Inform the routing algorithm that c2paddr is no longer a neighbor.
	if (rtm_) {
		rtm_->RemoveNode(c2paddr);
	}

}
void C2PowerLayer::setBeaconingON() {
	cm_->BeaconingON();
}
void C2PowerLayer::setBeaconingOFF() {
	cm_->BeaconingOFF();
}




//int C2PowerLayer::RefreshNeighbourAndReschedule()
//{
//	cm_->RefreshNeighbourList();//refresh and reschedule refreshing
//}


/**
 *  Riccardo: It discovers needed info about layers below...
 *  It uses both cl messages and sap connections:
 *  	- cross-layer msgs: in order to retrieve info (module IDs and addresses)
 *  	- SAP connections: in order to understand the position of modules in the different stacks
 *                         (added getModuleDown() function in SAP.cc of nsmiracle for this purpose)
 */
void C2PowerLayer::discovery() {

	int numSAP = getDownLaySAPnum();
	for (int i=0; i<numSAP; i++ ) {
		SAP* lowerSAP = getDownLaySAP(i);

		stack_entry e;
//		printf("C2PowerLayer::discovery() - stack no. %d / %d\n",i,numSAP);

		bool loop = true;
		while (loop==true) {
			Module* mod = lowerSAP->getModuleDown();

			if (!mod) break;

//			printf("C2PowerLayer::discovery() -   send cl msg to moduleid %d..",mod->getId() );
			ClMsgC2pDisc* m = new ClMsgC2pDisc(UNICAST, mod->getId() );
			sendSyncClMsg(m);
//			printf("ok\n");

			if ( m->getKind() == IPIF) {
//				printf("C2PowerLayer::discovery() -   IPIF..");
				e.setIPIFaddr(  m->getAddr() );
				e.setIPIFid  (  mod->getId() );
				if (mod->getDownLaySAPnum()==0) break;
				lowerSAP = mod->getDownLaySAP(0);
				delete(m);
//				printf("ok\n");
				continue;
			}
			else if ( m->getKind() == MAC) {
//				printf("C2PowerLayer::discovery() -   MAC..");
				e.setMACaddr(  m->getAddr() );
				e.setMACid  (  mod->getId() );
				if (mod->getDownLaySAPnum()==0) break;
				lowerSAP = mod->getDownLaySAP(0);
				delete(m);
//				printf("ok\n");
				continue;
			}
			else if (mod->getLayer()==1) {
//				printf("C2PowerLayer::discovery() -   PHY");
				e.setPHYid( mod->getId() );
				delete(m);
				loop=false;
//				printf("ok\n");
				break;
			}
			if (mod->getDownLaySAPnum()==0) break;
			lowerSAP = mod->getDownLaySAP(0);
		}
		node_stacks->addEntry( i, e );
	}

	ClMsgC2pGetC2PEnergyModel* m = new ClMsgC2pGetC2PEnergyModel();
	sendSyncClMsg(m);
	energyModel_ = m->getPointer();
	delete(m);
	assert(energyModel_!=0);
	printf("C2P addr: %d\n", addr_ );
	for ( int i=0; i<node_stacks->getSize(); i++ ) {
		printf("STACK No. %d\n", i );
		printf("  IPIF: id=%d addr=", node_stacks->getEntry(i).getIPIFid() );
		parse32Addr(node_stacks->getEntry(i).getIPIFaddr()); printf("\n");
		printf("  MAC:  id=%d addr=%d\n", node_stacks->getEntry(i).getMACid() , node_stacks->getEntry(i).getMACaddr() );
		printf("  PHY:  id=%d\n", node_stacks->getEntry(i).getPHYid() );
	}


}

