
#include "c2power-plugin.h"
#include <iostream>
#include <clsap.h>

ClMessage_t CLMSG_C2P_GETC2PENERGYMODEL = 0;

/*-------------------------------------------------------------------------------------------------------
	methods for ClMsgC2pGetC2PEnergyModel class
---------------------------------------------------------------------------------------------------------*/
ClMsgC2pGetC2PEnergyModel::ClMsgC2pGetC2PEnergyModel()
  : ClMessage(C2POWER_CLMSG_VERBOSITY, CLMSG_C2P_GETC2PENERGYMODEL)
{
	emp_ = 0;
}

ClMsgC2pGetC2PEnergyModel::ClMsgC2pGetC2PEnergyModel(ClMsgC2pGetC2PEnergyModel *m) : ClMessage(m)
{
	emp_ = 0;
}

// retrun a copy of the message
ClMessage *ClMsgC2pGetC2PEnergyModel::copy()
{
  ClMessage *temp = new ClMsgC2pGetC2PEnergyModel(this);
  return (temp);
}
/*-------------------------------------------------------------------------------------------------------*/

/* ======================================================================
   TCL Hooks for the simulator
   ====================================================================== */
static class C2PEnergyModel_Class : public TclClass {
public:
	C2PEnergyModel_Class() : TclClass("Plugin/C2PEnergyModel") {}
	TclObject* create(int, const char*const*) {
		return (new C2PEnergyModel());
	}
} class_c2penergymodel;


C2PEnergyModel::C2PEnergyModel()
{
	em_ = new EnergyModel(NULL, -1, 0, 0);
}

C2PEnergyModel::~C2PEnergyModel()
{
	delete(em_);
}

// TCL command interpreter
int C2PEnergyModel::command(int argc, const char*const* argv)
{
	Tcl& tcl = Tcl::instance();

	if(argc == 2) {
		if(strcasecmp(argv[1], "traceEnergy")==0)
		{

			double res = em_->et()+em_->er()+ em_->ei()+em_->es();
			tcl.resultf("%f Trace Energy %s %f %f %f %f %f %f ", 	NOW,
																	tag_,
																	em_->energy(),
																	res,
																	em_->et(),
																	em_->er(),
																	em_->ei(),
																	em_->es() );

			return TCL_OK;
		}
	}
	if (argc == 3)
	{
		if(strcasecmp(argv[1], "setEnergy")==0)
		{
			assert(em_!=0);
			em_->setenergy(atof(argv[2]));
			return TCL_OK;
		}
	}

	return PlugIn::command(argc, argv);
}

int C2PEnergyModel::recvSyncClMsg(ClMessage* m)
{
	if(m->type() == CLMSG_C2P_GETNRGPOINTER)
	{
		ClMsgC2pGetNrgPointer* mm = (ClMsgC2pGetNrgPointer*)m;
		if ( mm ) {
			mm->setPointer(em_);
		} else {
			cout << "WANR: ClMessage is not a ClMsgC2pGetNrgPointer.." << endl;
			return TCL_OK;
		}
		return TCL_OK;
	}
	if(m->type() == CLMSG_C2P_GETC2PENERGYMODEL)
	{
		ClMsgC2pGetC2PEnergyModel* mm = (ClMsgC2pGetC2PEnergyModel*)m;
		if ( mm ) {
			mm->setPointer(this);
		} else {
			cout << "WANR: ClMessage is not a ClMsgC2pGetC2PEnergyModel.." << endl;
			return TCL_OK;
		}
		return TCL_OK;
	}

	return PlugIn::recvSyncClMsg(m);

  return 0;
}

double C2PEnergyModel::getResidualEnergy() {
	return em_->energy();
}

// XXX Riccardo:
//   Commented all the old file... it seems old stuff not used by other parts of the code

//#include "c2power-plugin.h"
////#include "node-core.h"
//#include <iostream>
//#include <clsap.h>
//#include "ip-clmsg.h"
//
//ClMessage_t C2P_CLM_ENTRY_REQ = 0;		// request for an entry (from c2power layer)
//
///* ======================================================================
//   TCL Hooks for the simulator
//   ====================================================================== */
//static class C2PowerPlugInClass : public TclClass {
//public:
//	C2PowerPlugInClass() : TclClass("C2PowerPlugIn") {}
//	TclObject* create(int, const char*const*) {
//		return (new C2PowerPlugIn());
//	}
//} class_c2powerplugin;
//
//
//C2PowerPlugIn::C2PowerPlugIn()
//{
//}
//
//C2PowerPlugIn::~C2PowerPlugIn()
//{
//}
//
//// TCL command interpreter
//int C2PowerPlugIn::command(int argc, const char*const* argv)
//{
//	if (argc == 2)
//	{
//		if (!strcmp(argv[1], "discovery"))
//		{
//			IPClMsgSendAddr*m = new IPClMsgSendAddr();
//			m->setSource(getId());
//			sendSyncClMsg(m);
//			delete m;
//
//			return TCL_OK;
//		}
//		if (!strcmp(argv[1], "printMap")) {
//			printMap();
//			return TCL_OK;
//		}
//	}
//	if (argc == 4) {
//		if (!strcmp(argv[1], "addMapEntry"))
//		{
//			listentry le( atoi(argv[2]) , atoi(argv[3]) );
//			map_.push_back(le);
//			return TCL_OK;
//		}
//	}
//	return PlugIn::command(argc, argv);
//}
//
//void C2PowerPlugIn::printMap()
//{
//	list<listentry>::iterator iter = map_.begin();
//	printf("%s - printMap()\n", getTag());
//	if (iter == map_.end()) {
//		cout << "WARN: No modules discovered." << endl;
//		cout << " Maybe you want to use tcl command of C2PowerPlugIn for filling module info:" << endl;
//		cout << " - discovery (e.g.: $ns at 0.1 $plgin($i) discovery)" << endl;
//		cout << " - addMapEntry (e.g.: $plgin($i) addMapEntry [$MAC($i) Id_] [$MAC($i) id])" << endl;
//		return;
//	}
//
//	cout << "\tDiscovered modules/plugins: " << endl;
//	while (iter != map_.end())
//	{
//		cout << "\t ModuleID: " << iter->getId() << " - Addr: " << iter->getAddress() << endl;
//		iter++;
//	}
//
//}
//
//int C2PowerPlugIn::recvAsyncClMsg(ClMessage* m)
//{
//	// Returned msg from IP layer (upon a IPClMsgSendAddr)
//	if(m->type() == IP_CLMSG_SEND_ADDR)
//	{
////		printf(">>>>> %d-%d\n",((IPClMsgSendAddr*)m)->getSource(), ((IPClMsgSendAddr*)m)->getAddr());
//		IPClMsgSendAddr* mm = (IPClMsgSendAddr*)m;
//		listentry le(mm->getSource(), mm->getAddr());
//		map_.push_back(le);
//		return 0;
//	}
//	return PlugIn::recvSyncClMsg(m);
//
//  return 0;
//}
//
//int C2PowerPlugIn::recvSyncClMsg(ClMessage* m)
//{
//	if(m->type() == C2P_CLM_ENTRY_REQ)
//	{
//		C2PClMsgEntryReq* mm = (C2PClMsgEntryReq*)m;
//		if (mm->getAddr() ==-1) { // Update the address field of C2PClMsgEntryReq
//			list<listentry>::iterator it = map_.begin();
//			if (it ==  map_.end())
//			{
//				cout << "WANR: No address found; empty list of Modules" << endl;
//				cout << " Maybe you want to use tcl command of C2PowerPlugIn for filling module info:" << endl;
//				cout << " - discovery (e.g.: $ns at 0.1 $plgin($i) discovery)" << endl;
//				cout << " - addMapEntry (e.g.: $plgin($i) addMapEntry [$MAC($i) Id_] [$MAC($i) id])" << endl;
//				return 0;
//			}
//			while (it != map_.end())
//			{
//			  if ( it->getId() == mm->getModId() ) {
//				  mm->setAddr( it->getAddress() );
//				  it++;
//				  return 0;
//			  }
//			}
//			cout << "WANR: No address found for ModuleID=" << mm->getModId() << endl;
//		}
//
//		return 0;
//	}
//	return PlugIn::recvSyncClMsg(m);
//
//  return 0;
//}
//
//void C2PowerPlugIn::setClMsgType(ClMessage_t type)
//{
//	clMsgType_ = type;
//}
//
//ClMessage_t C2PowerPlugIn::getClMsgType()
//{
//	return(clMsgType_);
//}
//
//extern "C" int C2PowerF_Init()
//{
//	/*
//	Put here all the commands which must be execute when the library is loaded (i.e. TCL script execution)
//	Remember to ruturn 0 if all is OK, otherwise return 1
//	*/
//	return 0;
//}
//extern "C" int  CygC2PowerF_Init()
//{
//	C2PowerF_Init();
//}
//
//
///*-------------------------------------------------------------------------------------------------------
//	methods for C2PClMsgEntryReq class
//---------------------------------------------------------------------------------------------------------*/
//C2PClMsgEntryReq::C2PClMsgEntryReq()
//  : ClMessage(C2PPLGIN_CLMSG_VERBOSITY, C2P_CLM_ENTRY_REQ)
//{
//	modId_= -1;
//	addr_ = -1;
//}
//
//
//C2PClMsgEntryReq::C2PClMsgEntryReq(C2PClMsgEntryReq *m) : ClMessage(m)
//{
//	modId_= m->getModId();
//	addr_ = m->getAddr();
//}
//
//
//// retrun a copy of the message
//ClMessage *C2PClMsgEntryReq::copy()
//{
//  ClMessage *temp = new C2PClMsgEntryReq(this);
//  return (temp);
//}

