################################################################################
# Command-line parameters
################################################################################

##### DEFAULT PARAMETERS

# if allow_off_if is 1, all the unused interfaces will be switched off
# if it is 0, all unused interfaces will still be on, in idle state
set allow_off_if 0

#set param(nodsel) 1;
set param(nodsel)   <NODSEL>
#set param(SEED)				2
set param(SEED)				<SEED>
#set param(PERIOD)			0.1 ; PERIOD of CBRs=(0.1 0.05 0.033333333 0.025 0.02 0.016666667 0.014285714 0.0125 0.01111111 0.01)
set param(PERIOD)			<PERIOD>

#set param(TRAFFIC)		2;   mode 1 and 2
set param(TRAFFIC)		<TRAFFIC>

#set param(num_nodes) 10;
set param(num_nodes) <NUM_NODES>
#set param(area_x)    20;
#set param(area_y)    20;
set param(area_x)    <AREA>
set param(area_y)    <AREA>



set param(cbrPacketSize)	1024
set param(C2powerTrace) 0
set rout_param(broadcast)        0  ;# use broadcast for routing cmd pkts (0 false or 1 true)
#set rout_param(broadcast)        <BROADCAST>  ;# use broadcast for routing cmd pkts (0 false or 1 true)
set rout_param(alpha)            1
#set rout_param(alpha)            <ALPHA>


set rout_param(hoplimit)         3
set rout_param(reBroadcastLimit) 2
set rout_param(rreqtimeout)      0.5
set rout_param(routeValidity)    10

set rout_param(turnLongOnOff)    1 ;# [0 |  1] - if = 1 then turns on long phy at the beginning
set rout_param(turnOFFperiod)    0 ;# [0 | 10]
set rout_param(turnONperiod)     0  ;# [0 |  1] - if > 0 then periodicLongScanning(true) is called in initialization
#set rout_param(turnLongOnOff)    <TURN_ON_OFF>     ;# [0 |  1] - if = 1 then turns on long phy at the beginning
#set rout_param(turnOFFperiod)    <TURN_OFF_PERIOD> ;# [0 | 10]
#set rout_param(turnONperiod)     <TURN_ON_PERIOD>  ;# [0 |  1] - if > 0 then periodicLongScanning(true) is called in initialization

set param(startCbr) 50.0
set param(stopCbr)  150.0
set param(cbrFlowperiod) [ expr ($param(stopCbr)-$param(startCbr))/$param(num_nodes) ]
set param(stopSim)  [ expr $param(stopCbr) + 1 ]


set param(power_tx_short) 0.0000340  ;# Module/MPhy/80211Phy set TxPower_          0.001     ;# default = 0.01
#set param(power_tx_short) 0.01  ;# Module/MPhy/80211Phy set TxPower_          0.001     ;# default = 0.01
set param(power_tx_long)  0.01  ;# Module/MPhy/80211Phy set TxPower_          0.001     ;# default = 0.01

#### Datarate Settings ####
set param(num_dr_modes) 8

set datarate_modes(0) Mode6Mb
set datarate_modes(1) Mode9Mb
set datarate_modes(2) Mode12Mb
set datarate_modes(3) Mode18Mb
set datarate_modes(4) Mode24Mb
set datarate_modes(5) Mode36Mb
set datarate_modes(6) Mode48Mb
set datarate_modes(7) Mode54Mb

# Percentage of nodes having a specific datarate mode
set long_range_perc(0) 21
set long_range_perc(1) 28
set long_range_perc(2) 21
set long_range_perc(3) 0
set long_range_perc(4) 0
set long_range_perc(5) 0
set long_range_perc(6) 15
set long_range_perc(7) 15

set short_range_perc(0) 0
set short_range_perc(1) 0
set short_range_perc(2) 0
set short_range_perc(3) 0
set short_range_perc(4) 0
set short_range_perc(5) 30
set short_range_perc(6) 40
set short_range_perc(7) 30

####################################
#### EnergyModel parameters 
####

set nrgModelParam(TotalEnergy)  5000.0

set nrgModelParam(LongTxPwr)		1.900
set nrgModelParam(LongRxPwr)		1.340
set nrgModelParam(LongIdlePwr)		0.110
set nrgModelParam(LongSleepPwr)		0.005

set nrgModelParam(ShortTxPwr)		1.900
set nrgModelParam(ShortRxPwr)		1.340
set nrgModelParam(ShortIdlePwr)		0.110
set nrgModelParam(ShortSleepPwr)	0.005

################################################################################
# IMPORT PROCEDURES AND LIBRARIES
################################################################################
#set libfolder /home/albano/c2power_cnet/c2power/src/.libs
set libfolder /usr/local/lib
#/home/fedrizzi/BitbucketRep/c2powerWK/ns-allinone-2.34/lib/lib
set myfolder /home/albano/c2power_cnet/c2power/src/
#/home/fedrizzi/BitbucketRep/c2powerWK/mysims


source IT_createAP.tcl
#source $myfolder/proc_createC2PowerAP.tcl
#source proc_createSinkNode.tcl
source IT_createC2PowerNode.tcl
#source proc_createAdHocWiFiNode.tcl
#source $myfolder/proc_createC2PowerNode.tcl
#source proc_createC2PowerNodeShort.tcl
source IT_deploy_nodes.tcl


load $libfolder/libMiracle.so
load $libfolder/libmiraclelink.so
load $libfolder/libMiracleBasicMovement.so
load $libfolder/libMiracleWirelessCh.so
#load $libfolder/libMiraclePhy802_11.so
#load $libfolder/libMiracleMac802_11.so
load $libfolder/libmiraclecbr.so
load $libfolder/libmphy.so
load $libfolder/libMiracleIp.so
load $libfolder/libMiracleIpRouting.so
load $libfolder/libmiracleport.so
load $libfolder/libdei80211mr.so
load .libs/libC2Power.so

load $libfolder/libcbrtracer.so
load $libfolder/libroutingtracer.so
load .libs/libC2PowerTracer.so
load $libfolder/libalttracermac80211.so
load $libfolder/libarptracer.so
#load $libfolder/libmphytracer.so

#load $libfolder/libmiracletcp.so


################################################################################
# Simulator instance
################################################################################

set ns [new Simulator]
$ns use-Miracle

################################################################################
# RANDOM NUMBERS GENERATORS
################################################################################

#set opt(dmax)   [expr $param(apgrid) ]
#set opt(dmin)   [expr -$param(apgrid)]
proc RandomInteger {min max} {
	return [expr {int([$::mRNG value]*($max-$min+1)+$min)}]
}
proc Rand2Int {val} {
	return [expr {int($val)}]
}
proc RandomInteger4 {min max mRNG} {
	return [expr {int([$mRNG value]*($max-$min+1)+$min)}]
}

global defaultRNG
set positionrng [new RNG]
set startrng [new RNG]
set stoprng [new RNG]
set moderng [new RNG]

# seed random number generator according to replication number
set seed $param(SEED) 
for {set j 0} {$j < $seed} {incr j} {
    $defaultRNG next-substream
    $positionrng next-substream
    $startrng next-substream
    $stoprng next-substream
	$moderng next-substream
}

set rvposition [new RandomVariable/Uniform]
$rvposition set min_ 0
$rvposition set max_ $param(area_x)
$rvposition use-rng $positionrng

set start [new RandomVariable/Uniform]
$start set min_ 0
$start set max_ 1
$start use-rng $startrng

set stop [new RandomVariable/Uniform]
$stop set min_ 6
$stop set max_ 6.9
$stop use-rng $stoprng

set mRNG [new RandomVariable/Uniform]
$mRNG set min_ 0
$mRNG set max_ 1
$mRNG use-rng $moderng



################################################################################
# FINISH PROCEDURES
################################################################################

proc finish {} { 
	global ns tf

	$ns flush-trace
	close $tf

puts "\n\n"

	puts "- Energy consumptions values ----------------------"
	## put energy results #######################

	if {$::param(num_nodes) > 0} {
		puts "C2POWER_NODES"
		for {set nodeIdx 1} {$nodeIdx <= $::param(num_nodes)} {incr nodeIdx} {

			set newidx [expr {$nodeIdx+0}]
			#DATA ORDER: residualEnergy consumedEnergy energyTX energyRX energyIDLE energySLEEP
			puts "C2power $nodeIdx: [$::NrgModel([expr {$nodeIdx+0}]) traceEnergy]"

		}
	}

	set tcl_precision 5
	puts "Results for AP"
	puts "[$::apNrgModel traceEnergy]"

	puts "---------------------------------------------------"
	if {$::param(num_nodes) > 0} {
		puts "Traffic C2POWER_NODES"
		for {set nodeIdx 1} {$nodeIdx <= $::param(num_nodes)} {incr nodeIdx} {

			set newidx [expr {$nodeIdx+0}]

			if { [info exist ::apCbr($newidx)] } {
				#puts "- Traffic AP -> CLASS_A C2power $nodeIdx -------------------------"
#				puts "C2power $newidx: CBR_getfft: [$::apCbr($newidx) getftt]"
				#puts "CBR_getrtt: [$::apCbr($newidx) getrtt]"
#				puts "C2power $newidx: CBR_getper: [$::apCbr($newidx) getper]"
#				puts "C2power $newidx: CBR_getthr: [$::apCbr($newidx) getthr]"
				#puts "CBR_getrttstd: [$::apCbr($newidx) getrttstd]"
#				puts "C2power $newidx: CBR_getfttstd: [$::apCbr($newidx) getfttstd]"
#				puts "C2power $newidx: CBR_getsentpkts: [$::apCbr($newidx) getsentpkts]"
				puts "C2power $newidx: CBR_getrecvpkts: [$::apCbr($newidx) getrecvpkts]"
			} else {
				puts "No apCbr created for C2Power($newidx)"
			}
			#puts "- Traffic CLASS_A C2power $nodeIdx -> AP -------------------------"
			#puts "CBR_getfft: [$::Cbr($newidx) getftt]"
			#puts "CBR_getrtt: [$::Cbr($newidx) getrtt]"
			#puts "CBR_getper: [$::Cbr($newidx) getper]"
			#puts "CBR_getthr: [$::Cbr($newidx) getthr]"
			#puts "CBR_getrttstd: [$::Cbr($newidx) getrttstd]"
			#puts "CBR_getfttstd: [$::Cbr($newidx) getfttstd]"
			#puts "CBR_getsentpkts: [$::Cbr($newidx) getsentpkts]"
			#puts "CBR_getrecvpkts: [$::Cbr($newidx) getrecvpkts]"

		}
	}

	puts "..::SIMULATION FINISHED::.. [ns-random]"

puts "\n\n"

}



################################################################################
# Tracing files
################################################################################

set machine_name [exec uname -n]
#set opt(fstr)        ${param(AdHocPairsNum)}_${param(NoC2PowerNodeNum)}.${machine_name}
set opt(resultfname) "${argv0}-[exec whoami].log"
set opt(tracefile)   "${argv0}-[exec whoami].tr"

### file for nsmiracle trace
set tf [open $opt(tracefile) w]
$ns trace-all $tf


################################################################################
# CHANNELS DEFINITION
################################################################################

set channelShort [new Module/DumbWirelessCh]
#set propagationShort [new MPropagation/FullPropagation]
set propagationShort [new MPropagation/MFreeSpace]
#$propagationShort set xFieldWidth_ 60
#$propagationShort set yFieldWidth_ 60
$propagationShort set debug_ 0

set channelLong [new Module/DumbWirelessCh]
#set propagationLong [new MPropagation/FullPropagation]
set propagationLong [new MPropagation/MFreeSpace]
#$propagationLong set xFieldWidth_ 60
#$propagationLong set yFieldWidth_ 60
$propagationLong set debug_ 0

create-god [expr  ( 2*($param(num_nodes)) + 1 ) ]


## Channels 802.11 ##
#            	   1     2     3     4     5     6     7     8     9     10    11
# Spectral masks 2.412 2.417 2.422 2.427 2.432 2.437 2.442 2.447 2.452 2.257 2.462

set maskShort [new MSpectralMask/Rect]
$maskShort setFreq 2.412e9
$maskShort setBandwidth 22e6
$maskShort setPropagationSpeed 3e8

set maskLong [new MSpectralMask/Rect]
$maskLong setFreq 2.457e9
$maskLong setBandwidth 22e6
$maskLong setPropagationSpeed 3e8



################################################################################
# OVERRIDE DEFAULT MODULE CONFIGURATION
################################################################################


####################################
#### WiFi initialization
####

set noisePower 7e-12 

Module/MPhy/80211Phy set TxPower_          0.001     ;# default = 0.01
Module/MPhy/80211Phy set SlotTime_         0.000020  ;# default = 0.000020
Module/MPhy/80211Phy set CWMin_            32        ;# default = 32
Module/MPhy/80211Phy set CWMax_            1024      ;# default = 1024
Module/MPhy/80211Phy set useShortPreamble_ false     ;# default = false
Module/MPhy/80211Phy set SlotTime_         0.000020  ;# default = 0.000020
Mac/802_11/Multirate set ShortRetryLimit_  3
Mac/802_11/Multirate set LongRetryLimit_   5

Module/MPhy/80211Phy set SIFS_ 0.000020       ;# DIFS = SIFSTime + 2 * SlotTime
Module/MPhy/80211Phy set bSyncInterval_ 0 ;# dfault = 0
Module/MPhy/80211Phy set gSyncInterval_ 0 ;# default = 0

Module/MPhy/80211Phy set CSThresh_ [expr $noisePower * 1.1]
Module/MPhy/80211Phy set CCAMode_ 0
Module/MPhy/80211Phy set VerboseCounters_ 1


Queue/DropTail/PriQueue set Prefer_Routing_Protocols
Queue/DropTail/PriQueue set size_ 1000


set per [new PER]
$per set noise_ $noisePower
$per set debug_ 0
$per loadPERTable80211gTrivellato

Mac/802_11/MrclMultirate set VerboseCounters_ 1
set peerstats [new PeerStatsDB/Static]

# set numpeers as the number of wifi macs created
$peerstats numpeers [expr  ( 2*($param(num_nodes)) + 1 ) ]


RateAdapter/RRAA set timeout_ 6
RateAdapter/ARF set debug_ 0


####################################
#### C2power initialization
####

Module/C2Power set beaconPeriod_ 5.0
Module/C2Power set neighbourListPeriod_ 5.0
Module/C2Power set neighbourListRemoveTreshold_ 10.0



####################################
#### DEBUG values
####

# note: debug values for C2Power module:
#	define C2POWER_DEBUG_SKELETON 1
#	define C2POWER_DEBUG_NODSEL 2
#	define C2POWER_DEBUG_CLUSTERING 4
#	define C2POWER_DEBUG_ROUTING 8
Module/C2Power set debug_ 0

Mac/802_11/Multirate set debug_ 0
Module/MPhy/80211Phy set debug_ 0
Module/IP/Interface debug_ 0
$per set debug_ 0
$propagationShort set debug_ 0
$propagationLong set debug_ 0


####################################
#### CBR module
####

Module/CBR set pktSize2_ 0.0
Module/CBR set debug_ 0

####################################
#### Definition of an ethernet link simulating internet...
####

set dlink [new Module/DuplexLink]
$dlink delay      0.1
$dlink bandwidth  100000000
$dlink qsize      10
$dlink settags "dlink"


################################################################################
# Scenario settings
################################################################################

deploy_nodes

array set deployed_datarates_LR [list]
array set deployed_datarates_SR [list]
choose_datarates "long-range"
choose_datarates "short-range"

puts "---------------------------------------------------------"
for {set i 1} {$i <= $param(num_nodes)} {incr i} {
	puts "POS NODE $i \[ $topology(pos_x_$i) , $topology(pos_y_$i) \]"
}
puts "POS AP  \[ $topology(pos_AP_x) , $topology(pos_AP_y) \]"

puts "---------------------------------------------------------"
for {set i 1} {$i <= $param(num_nodes)} {incr i} {   puts "node $i | sr= $deployed_datarates_SR($i) | lr= $deployed_datarates_LR($i)"   }
#for {set i 1} {$i <= $param(num_nodes)} {incr i} {   puts "node $i | lr= $deployed_datarates_LR($i)"   }
puts "---------------------------------------------------------"


#### Creation of nodes ####

for {set id 1} {$id <= $param(num_nodes)} {incr id} {
	# proc createNoC2PowerNode {id ipad netmask defaultGw longDatarate posX posY}
	createC2PowerNode "arf" "$id" "3.0.0.$id" "255.255.255.0" "2.0.0.$id" "255.255.255.0" "$deployed_datarates_LR($id)" "$deployed_datarates_SR($id)" "2.0.0.254" "$topology(pos_x_$id)" "$topology(pos_y_$id)"
}

# proc createC2PowerAP {wifiaddr wifinetmask wireaddr wirenetmask apDataMode apposx apposy}
createC2PowerAP "2.0.0.254" "255.255.255.0" "1.0.0.1" "255.255.255.0" "Mode6Mb" "$topology(pos_AP_x)" "$topology(pos_AP_y)"
	$apIpr addRoute "3.0.0.0" "255.255.255.0" "2.0.0.254"
	$apIpr addRoute "2.0.0.0" "255.255.255.0" "2.0.0.254"
	$apIpr addRoute "1.0.0.0" "255.255.255.0" "1.0.0.1"

#### Set bss_id for long range devices ####
for {set id 1} {$id <= $param(num_nodes)} {incr id} {
	$MACLong($id) bss_id "$ap_macaddr"
}

################################################################################
# Define the traffic
################################################################################

if {$param(TRAFFIC)==1} {
	for {set id 1} {$id <= $param(num_nodes)} {incr id} {
		# define ap CBR
		set apCbr($id)       [new Module/CBR]
		$apNode addModule 7  $apCbr($id) 0 "apCbr($id) "
	  	$apNode setConnection $apCbr($id) $apPort  1

		# c2p -> ap
		$Cbr($id) set destAddr_ [$apIpIF_wifi addr]
		$Cbr($id) set destPort_ [$apPort assignPort $apCbr($id)]

		# ap -> c2p
#		$apCbr($id) set destAddr_ [$IpIFShort($id) addr]
		$apCbr($id) set destAddr_ [$IpIFLong($id) addr]
		$apCbr($id) set destPort_ [$Port($id) assignPort $Cbr($id)]

		set start [ expr ($id-1)*$param(cbrFlowperiod)+$param(startCbr) ]
		set stop  [ expr ($id)*$param(cbrFlowperiod)+$param(startCbr) ]
		puts "Traffic for node $id | $start | $stop |"

		$ns at $start "$Cbr($id) start"
		$ns at $stop  "$Cbr($id) stop"
	}
}
if {$param(TRAFFIC)==2} {
	for {set id 1} {$id <= $param(num_nodes)} {incr id} {
		# define ap CBR
		set apCbr($id)       [new Module/CBR]
		$apNode addModule 7  $apCbr($id) 0 "apCbr($id) "
	  	$apNode setConnection $apCbr($id) $apPort  1

		# c2p -> ap
		$Cbr($id) set destAddr_ [$apIpIF_wifi addr]
		$Cbr($id) set destPort_ [$apPort assignPort $apCbr($id)]

		# ap -> c2p
#		$apCbr($id) set destAddr_ [$IpIFShort($id) addr]
		$apCbr($id) set destAddr_ [$IpIFLong($id) addr]
		$apCbr($id) set destPort_ [$Port($id) assignPort $Cbr($id)]

		set start $param(startCbr)
		set stop  $param(stopCbr)
		puts "Traffic for node $id | $start | $stop |"

		$ns at $start "$Cbr($id) start"
		$ns at $stop  "$Cbr($id) stop"
#		$ns at $start "$apCbr($id) start"
#		$ns at $stop  "$apCbr($id) stop"
	}
}

################################################################################
# SIMULATION
################################################################################

puts "---> BEGIN SIMULATION"

#$ns at 101.0 "finish; $ns halt"
$ns at $param(stopSim) "finish; $ns halt"

if {$param(nodsel)} {
    $ns at 39.0 "$apC2Power nodsel_deliverroutes"
    for {set id 1} {$id <= $param(num_nodes)} {incr id} {
	$ns at [expr 38+($id/100.0)] "$C2Power($id) nodsel_sendstats"
	if ($allow_off_if) {
	    $ns at 40.0  "$C2Power($id) nodsel_setuproutes"
	} else {
	    $ns at 40.0  "$C2Power($id) nodsel_setuproutes_IFON"
	    $ns at [expr $param(stopSim)-0.1]  "$C2Power($id) short_off"
	}
	#    $ns at 63.0  "$C2Power($id) nodsel_dumpdata"
    }
} else {
    for {set id 1} {$id <= $param(num_nodes)} {incr id} {
	$ns at 40.0  "$C2Power($id) longrange"
	if ($allow_off_if) {
	    $ns at 40.0  "$C2Power($id) short_off"
	} else {
	    $ns at [expr $param(stopSim)-0.1]  "$C2Power($id) short_off"
	}
    }
}

$ns run


