#include "c2power-timers.h"
#include "c2power_mock.h"

void
C2PowerTimer::expire(Event *e){
    (m_->*fire)();
}

void RREQTimeoutTimer::expire(Event *e)
{
	module->selectRoute( this->src, this->rreqId );
}

void RREQFailed::expire(Event *e)
{
	module->failedRREQ( this->dstIP, this->rreqId );
}

void RREQTableTimer::expire(Event *e)
{
	module->refreshRREQTable();
}

void RouteTableTimer::expire(Event *e)
{
	module->refreshRouteTable();
}

void LongONOFFTimer::expire(Event *e)
{
	module->periodicLongScanning(false);
}
