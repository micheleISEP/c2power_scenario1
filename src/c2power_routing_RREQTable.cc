//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "c2power_routing_RREQTable.h"

Record::Record() {
	srcId = -1;
	destIP = -1;
	rreqId = -1;
	reBroadcast = -1;
	decisionTaken = -1;
}
//Record::Record(int sId, int rId, int rBdc) {
//	srcId = sId;
//	rreqId = rId;
//	reBroadcast = rBdc;
//	decisionTaken = 0;
//}
Record::Record(int sId, int dIP, int rId, int rBdc) {
	srcId = sId;
	destIP = dIP;
	rreqId = rId;
	reBroadcast = rBdc;
	decisionTaken = 0;
}

Record::~Record() {
	routes.clear();
}

/*******************************************************************************/
/*******************************************************************************/
/*******************************************************************************/

RREQTable::RREQTable() {
}

RREQTable::~RREQTable() {
	rreqCache.clear();
}

// Get list size.
int RREQTable::getSize() {
	return rreqCache.size();
}

// Add RREQ ID to the list of rx RREQs  with reBroadcast=0.
void RREQTable::addRReq(int srcNode, int dstIP, int rreq, double time) {

	bool found = false;
	for ( int i=0; i<getSize(); i++) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].destIP == dstIP && rreqCache[i].rreqId == rreq ) {
			found = true;
			break;
		}
	}

	if ( found == false ) {
		Record r;
		r.srcId = srcNode;
		r.destIP = dstIP;
		r.rreqId = rreq;
		r.reBroadcast = 0;
		r.decisionTaken = 0;
		r.time = time;
		rreqCache.push_back( r );
	}
}

// Add RREQ ID to the list of rx RREQs
//   - if couple <srcId-rreqId> already exist increment reBroadcast number;
//	 - if couple <srcId-rreqId> doesn't exist it adds a new record with reBroadcast=1.
void RREQTable::incrReBroadcst(int srcNode, int dstIP, int rreq, double time) {

	bool found = false;
	int i=0;
	for ( i=0; i<getSize(); i++) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].destIP == dstIP && rreqCache[i].rreqId == rreq ) {
			found=true;
			break;
		}
	}

	if ( found == false ) {
		Record r;
		r.srcId = srcNode;
		r.destIP = dstIP;
		r.rreqId = rreq;
		r.reBroadcast = 1;
		r.decisionTaken = 0;
		r.time = time;
		rreqCache.push_back( r );
	}
	else {
		rreqCache[i].reBroadcast += 1;
	}
}

/** @brief set this record as decided */
void RREQTable::setDecided(int srcNode, int rreq) {

	bool found = false;
	int i=0;
	for ( i=0; i<getSize(); i++ ) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].rreqId == rreq ) {
			found=true;
			break;
		}
	}

	if ( found == false ) {
		printf("WARN: Try to set as decided a RREQ that is not in the RREQTable..\n");
	}
	else {
		rreqCache[i].decisionTaken = 1;
	}
}

/** @brief check if for this record a decision is already taken*/
bool RREQTable::checkDecided(int srcNode, int rreq) {

	bool found = false;
	int i=0;
	for ( i=0; i<getSize(); i++ ) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].rreqId == rreq ) {
			found=true;
			break;
		}
	}

	if ( found == false ) {
		return false;
	}
	else {
		if ( rreqCache[i].decisionTaken == 1 )
			return true;
		else
			return false;
	}
}

int RREQTable::findRReq(int srcNode, int rreq) {
	int idx = -1;

	for ( int i=0; i<getSize(); i++ ) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].rreqId == rreq ) {
			idx = i;
			break;
		}
	}
	return idx;
}

// Used in order to check at the source if a node already started a RREQ for a dest.
int RREQTable::chkAtSRC(int srcNode, int dstIP) {
	int idx = -1;

	for ( int i=0; i<getSize(); i++ ) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].destIP == dstIP ) {
			idx = i;
			break;
		}
	}
	return idx;
}


Record& RREQTable::getEntry(int idx)
{
	if (idx >= getSize() || idx < 0)
	{
		//error
	}

	return rreqCache.at(idx);
}

Record& RREQTable::operator[](int idx)
{
	return getEntry(idx);
}

bool RREQTable::remRReq(int srcNode, int rreq) {

	for ( int i=0; i<getSize(); i++ ) {
		if ( rreqCache[i].srcId == srcNode && rreqCache[i].rreqId == rreq ) {
			rreqCache.erase(rreqCache.begin()+i);
			return true;
		}
	}
	return false;

}

bool RREQTable::remRReq(int srcNode, int dstIP, int rreq) {

	for ( int i=0; i<getSize(); i++ ) {
		if (    rreqCache[i].srcId == srcNode &&
				rreqCache[i].destIP == dstIP&&
				rreqCache[i].rreqId == rreq ) {

			rreqCache.erase(rreqCache.begin()+i);
			return true;

		}
	}
	return false;

}

std::string RREQTable::print() {
	std::stringstream out;

	out << "\t-----------------------------------\n";
	out << "\tRREQ TABLE:\n";
	if ( rreqCache.empty() ) {
		out << "\t **empty**\n";
		out << "\t-----------------------------------\n";
		return out.str();
	}

	for ( int i=0; i<getSize(); i++ ) {
		out << "\t|" << rreqCache[i].srcId << "| ";
		out << "" << ((rreqCache[i].destIP & 0xff000000)>>24) << ".";
		out << "" << ((rreqCache[i].destIP & 0x00ff0000)>>16) << ".";
		out << "" << ((rreqCache[i].destIP & 0x0000ff00)>>8) << ".";
		out << "" << ((rreqCache[i].destIP & 0x000000ff)) << "| ";
		out << "" << rreqCache[i].rreqId << "| ";
		out << "" << rreqCache[i].reBroadcast << "| ";
		out << "" << rreqCache[i].decisionTaken << "| ";
		out << "" << rreqCache[i].time << "| ";
//		out << "numRoutes=" << rreqCache[i].routes.size() << "\n";
		for (int j=0; j<rreqCache[i].routes.size(); j++) {
			out << "R" << j << ":"<< rreqCache[i].routes[j]->printShort() << " ";
		}
		out << "\n";
	}
	out << "\t-----------------------------------\n";

	return out.str();
}


bool RREQTable::clearTable() {
	rreqCache.clear();
	return true;
}

int RREQTable::getNumRoutes(int srcNode, int rreq) {
	int i = findRReq(srcNode, rreq);
	return rreqCache[i].routes.size();
}

std::vector<Route*>* RREQTable::getRoutes(int srcNode, int rreq) {
	int i = findRReq(srcNode, rreq);
	return &rreqCache[i].routes;
}


bool RREQTable::addRoute(int srcNode, int rreq, Route* r){
//	printf("###\n");
//	printf(" %d| %d | %s\n",srcNode,rreq,r.print().c_str()  );

	int i = findRReq(srcNode, rreq);
	rreqCache[i].routes.push_back(r);
	return true;
}

/** Removes old entryes */
bool RREQTable::Refresh(double validity, double now) {
//	printf("#######################################\n");
//	printf("NOW=%f\n",now);
//	printf("%s\n",print().c_str());
//	printf("---------------------------------------\n");
	bool deleted = false;

	for ( int i=0; i<rreqCache.size(); i++ ) {

		if ( now - rreqCache.at(i).time > validity ) {

			rreqCache.erase(rreqCache.begin()+i);
			i--;
			deleted = true;
		}
	}
//	printf("%s\n",print().c_str());
//	printf("#######################################\n");
	return deleted;

}


